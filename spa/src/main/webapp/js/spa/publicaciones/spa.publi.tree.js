/**
 * Definicion del modelo del store de las publicaciones
 * 
 * @extends: 'Ext.data.Model'
 * @fields: [
 * 		title, string que define el nombre de la primera column
 * 		fechaPublicacion, hora en la que se va a publicar el anuncio
 * ]
 * 
 * @author �lvaro
 * @version 1.0
 */

Ext.define('Publicaciones',{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'title', 			type: 'string'},
		{name: 'fechaPublicacion',	type: 'date'}
	]
});


/**
 * @class spa.publi.tree
 * @extends: 'Ext.tree.Panel'
 * 
 * Tabla que muestra las publicaciones de la ultima semana, tanto las ya publicadas, como
 * las que aun no se han publicado
 * 
 * @author �lvaro
 * @version 1.0
 * 
 * @use: Ext.create('spa.publi.tree')
 */
Ext.define('spa.publi.tree',{
	extend: 'Ext.tree.Panel',
	
	store: Ext.create('Ext.data.TreeStore', {
        model: 'Publicaciones',
        proxy: {
            type: 'ajax',
            //the store will get the content from the .json file
            url: 'publicacionesProgramadas.do'
        },
        folderSort: false,
        sorters: ['fechaPublicacion']
    }),
    
    //config treepanel
	region:'center',
	id:'tree',
    title: 'Agenda de publicaciones',	
    rootVisible: false,
    multiSelect: false,
    userArrows: true,
	//forceFit: true,
    //singleExpand: false,
	simpleSelect: false,
	//autoScroll: true,
	containerScroll: true,
	//events treepanel
	viewConfig: {
		trackOver: false,
		stripeRows: false,
		componentCls: 'scroll'
	},
	listeners:{
		beforeselect: function(rm, rec, index, opt){
			if (!rec.data.leaf)
				return false;
		}
	},
	tbar: [
 	  '->',
 	  {
 		  text: 'Cerrar Sesi&oacute;n',
 		  iconCls: 'logout',
 		  handler: function(){
 			  window.location='logout';
 		  }
 	  }
     ],	
    //the 'columns' property is now 'headers'
    columns: [
		{
			xtype: 'treecolumn', //this is so we know which column will show the tree
			dataIndex: 'title',
			text: 'Titulo',
			//width: 300,
			//flex: 1,
			width: 500,
			sortable: true,
			expanded: true,
			hideable: false
		},
		{
			//xtype: 'treecolumn',
			text: 'Hora a la que se publicar�',
			dataIndex: 'fechaPublicacion',
			sortable: true,
			//width: '30%',
			flex: 2,
			renderer: Ext.util.Format.dateRenderer('H:i:s')
		}
	]
	
});
