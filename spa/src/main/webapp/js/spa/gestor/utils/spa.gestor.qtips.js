var tooltips = [
    {
    	title: Ext.getCmp('file_1').getRawValue(),
    	id: 'tooltip1',
    	target: 'v1',
    	autoHide: false,
    	closable: true,
    	listeners:{
    		beforeshow: function(tip){
    			tip.update('<img src="/spa/viewImage?"' + Ext.getCmp('idImagen1').getValue() + '/>');
    		}
    	}
    }
]
