var validaForm = function(){
	var msg = "";
	var valueReturn = true;
	var diasSemana = ["Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"];
	Ext.each(diasSemana, function(value){
		var desde = new Date(Ext.getCmp('d'+value).getValue());
		var hasta = new Date(Ext.getCmp('h'+value).getValue());
		var check = Ext.getCmp('c'+value);
		if (check.checked){
			if (desde.getTime() > hasta.getTime()){
				msg += "- La hora desde <b>" + value + "</b> no puede ser mayor a la hora hasta<br/>";
			}			
		}
	});
	return msg;	
}

var getLengthDescripcion = function(html){
	var long = 0;
	if (html.length>0){
		Ext.each(html, function(nodo){
			long += nodo.textContent.length;
		});	
	}
	return long;
}

var cardNav = function(incr, alta, padre){
    var l = Ext.getCmp('panel_anuncio').getLayout();
    var i = l.activeItem.id.split('card-')[1];
    var next = parseInt(i, 10) + incr;
    if (next == '1' && alta!='-1'){
    	Ext.getCmp('card-1').getForm().load({
    		url: 'loadFormMa.do',
    		waitMsg: 'Cargando...',
    		waitTitle: 'Espere',
    		params:{
    			id: alta
    		},
    		success: function(form, action){
    			datosForm = Ext.Object.getKeys(action.result.data);    			
    			if (Ext.Array.contains(datosForm,"imagen1")){
    				Ext.getCmp('x1').setVisible(true);
    				Ext.getCmp('v1').setVisible(true);
    				Ext.getCmp('file_1').setDisabled(true);
    				Ext.getCmp('idImagen1').setValue(action.result.data.idImagen1);
    				Ext.getCmp('file_1').setRawValue(action.result.data.imagen1);
    				Ext.getCmp('link_1').setDisabled(true);
    			}
    			if (Ext.Array.contains(datosForm,"imagen2")){
    				Ext.getCmp('x2').setVisible(true);
    				Ext.getCmp('v2').setVisible(true);
    				Ext.getCmp('file_2').setDisabled(true);
    				Ext.getCmp('idImagen2').setValue(action.result.data.idImagen2);
    				Ext.getCmp('file_2').setRawValue(action.result.data.imagen2);
    				Ext.getCmp('link_2').setDisabled(true);
    			}
    			if (Ext.Array.contains(datosForm,"imagen3")){
    				Ext.getCmp('x3').setVisible(true);
    				Ext.getCmp('v3').setVisible(true);
    				Ext.getCmp('file_3').setDisabled(true);
    				Ext.getCmp('idImagen3').setValue(action.result.data.idImagen3);
    				Ext.getCmp('file_3').setRawValue(action.result.data.imagen3);
    				Ext.getCmp('link_3').setDisabled(true);
    			}
    			if (Ext.Array.contains(datosForm,"imagen4")){
    				Ext.getCmp('x4').setVisible(true);
    				Ext.getCmp('v4').setVisible(true);
    				Ext.getCmp('file_4').setDisabled(true);
    				Ext.getCmp('idImagen4').setValue(action.result.data.idImagen4);
    				Ext.getCmp('file_4').setRawValue(action.result.data.imagen4);
    				Ext.getCmp('link_4').setDisabled(true);
    			}
    			if (Ext.Array.contains(datosForm,"imagen5")){
    				Ext.getCmp('x5').setVisible(true);
    				Ext.getCmp('v5').setVisible(true);
    				Ext.getCmp('file_5').setDisabled(true);
    				Ext.getCmp('idImagen5').setValue(action.result.data.idImagen5);
    				Ext.getCmp('file_5').setRawValue(action.result.data.imagen5);
    				Ext.getCmp('link_5').setDisabled(true);
    			}
    			if (Ext.Array.contains(datosForm,"imagen6")){
    				Ext.getCmp('x6').setVisible(true);
    				Ext.getCmp('v6').setVisible(true);
    				Ext.getCmp('file_6').setDisabled(true);
    				Ext.getCmp('idImagen6').setValue(action.result.data.idImagen6);
    				Ext.getCmp('file_6').setRawValue(action.result.data.imagen6);
    				Ext.getCmp('link_6').setDisabled(true);
    			}
    		},
    		failure: function(){
    			Ext.Msg.show({
    				title: "Error",
    				msg:"Error al recuperar los datos del anuncio",
    				icon: Ext.Msg.ERROR,
    				buttons: Ext.Msg.OK
    			});
    		}
    	});
    }
    if (next<2){
    	if (Ext.getCmp('card-0').getForm().isValid()){
    		var msg = validaForm();
    		if (msg == "" )
    			l.setActiveItem(next);
    		else{
        		Ext.Msg.show({
        			title: 'Datos Incorrectos',
        			msg: msg,
        			icon: Ext.Msg.ERROR,
        			buttons: Ext.Msg.OK
        		}); 
        		next--;
    		}
    	}else{
    		Ext.Msg.show({
    			title: 'Datos Incompletos',
    			msg: 'Revise el formulario, existe campos obligatorios que est&aacute;n vac&iacute;os',
    			icon: Ext.Msg.ERROR,
    			buttons: Ext.Msg.OK
    		});
    		next--;
    	}
    }else{
    	if (alta=='-1'){
    		Ext.getCmp('idPadre').setValue(padre);
    		Ext.getCmp('id').setValue(-1);
    		var url = "nuevoAnuncio.do";
    	}else{
    		Ext.getCmp('id').setValue(alta);
    		var url = "updateAnuncio.do";
    	}
    		
    		//Ext.getCmp('card-0').getForm().setValues({id: -1});
    		//var url = "/spa/nuevoAnuncio?id="+alta;
    	//console.log ("Longitud HTML -> " + getLengthDescripcion(Ext.getCmp('descripcion').getEditorBody().childNodes));
    	if (getLengthDescripcion(Ext.getCmp('descripcion').getEditorBody().childNodes)<31){
    		Ext.Msg.show({
    			title: 'Datos Incorrectos',
    			msg: 'El campo descripción debe tener al menos 30 caracteres.',
    			icon: Ext.Msg.ERROR,
    			buttons: Ext.Msg.OK
    		});
    	}else{
        	if (Ext.getCmp('card-1').getForm().isValid()){
        		try{
            		Ext.getCmp('card-1').getForm().submit({
            			url: url,
            			params:	Ext.getCmp('card-0').getForm().getValues(),
            			waitTitle: 'Guardando',
            			waitMsg: 'Espere por favor. Dando de alta el anuncio...',
            			success: function(from, action){
            				Ext.getCmp('anuncio').close();
            				Ext.getCmp('treeGrid').getStore().load();
            				Ext.getCmp('tree').getStore().load();
            				Ext.Msg.show({
            					title: 'Informaci&oacute;n',
            					msg: action.result.msg,
            					icon: Ext.Msg.INFO,
            					buttons: Ext.Msg.OK
            				});
            			},
            			scope: this,
            			failure: function(form, action){
            				Ext.Msg.show({
            					title: 'Error',
            					msg: action.result.msg,
            					icon: Ext.Msg.ERROR,
            					buttons: Ext.Msg.OK
            				});
            				next--;
            			}
            		});	
        		}catch (err){
        			console.log(err);
        		}
        	}else{
        		Ext.Msg.show({
        			title: 'Datos Incompletos',
        			msg: 'Revise el formulario, existe campos obligatorios que est&aacute;n vac&iacute;os',
        			icon: Ext.Msg.ERROR,
        			buttons: Ext.Msg.OK
        		});
        		next--;
        	}   
    	}
    }
    Ext.getCmp('card-prev').setDisabled(next===0);
    //Ext.getCmp('card-next').setDisabled(next===2);
    if (next===1){
    	if (alta=='-1'){
    		Ext.getCmp('card-next').setText("Crear Anuncio");
    		Ext.getCmp('card-next').setIconCls("new_doc");
    	}else{
    		Ext.getCmp('card-next').setText("Guardar");
    		Ext.getCmp('card-next').setIconCls("edit_doc");
    	}
    }else if (next < 1){
    	Ext.getCmp('card-next').setText("Siguiente");
    	Ext.getCmp('card-next').setIconCls("next");
    }
};	