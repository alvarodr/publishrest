var storeProvincias = Ext.define('spa.store.combo',{
	extend: 'Ext.data.Store',
	alias: 'widget.combostore'
});

Ext.define('spa.gestor.formMA',{
	extend: 'Ext.form.Panel',
	
	alias: 'widget.spawindforma',
	autoScroll: true,
	items:[
        {
    	   	xtype: 'hidden',
    	   	id: 'idPadre',
    	   	name: 'idPadre'
        },
        {
        	xtype: 'hidden',
        	id: 'id',
        	name: 'id'
        },
        {
        	columnWidth: 1,
        	border: false,
        	html: '<div><b>Formulario para MundoAnuncio.com</b><hr /></div>'
        },
		{
		    columnWidth: .50,
			id: 'titleMunAnun',
			name: 'titleMunAnun',
			allowBlank: false,
			minLength: 10,
			fieldLabel: '<span style="color: red">Titulo</span>',
			margin: '5%',
			xtype: 'textfield'
		},
		{
			columnWidth: .50,
			id: 'categoria',
			name: 'categoria',
			xtype: 'combo',
			margin: '5%',
			fieldLabel: '<span style="color: red">Categoria</span>',
			typeAhead: false,
			hideTrigger: false,
			allowBlank: false,
			queryDelay: 100,
			minChar: 2,
			valueField: 'valoId',
			displayField: 'valoNombre',
			triggerAction: 'all',
			mode: 'remote',
			loadingText: 'Cargando...',
			store: {
				xtype: 'combostore',
				model: 'Combo',
			    proxy: {
					type: 'ajax',
					url: 'getCategorias.do',
					reader: {
						type: 'json',
						root: 'items',
						totalProperty: 'totalCount'
					}
			    },
				autoLoad: true
			},
			/*tpl: Ext.create('Ext.XTemplate',{
		         '<tpl for="."><div>',
         			'<h3>{valoNombre}</h3>',		         	
     			   '</div>',
     			  '</tpl>'
			}),*/
			forceSelection  : true
		},
		{
		    columnWidth		: .50,
			id				: 'pais',
			name			: 'pais',
			margin			: '5%',
			xtype			: 'combo',
			fieldLabel		: 'Pa&iacute;s',
			typeAhead		: false,
			hideTrigger		: false,
			valueField		: 'valoId',
			displayField	: 'valoNombre',
			triggerAction 	: 'all',
			mode			: 'local',
			store			: Ext.create('Ext.data.Store', {
			    fields: ['valoId', 'valoNombre'],
			    data : [
			        {"valoId":"2", "valoNombre":"ESPA�A"}
			    ]
			}),
			value			: "2",
			readOnly		: true
			
		},
		{
		    columnWidth		: .50,
			id				: 'provincia',
			name			: 'provincia',
			margin			: '5%',
			xtype			: 'combo',
			fieldLabel		: '<span style="color: red">Provincia</span>',
			typeAhead		: false,
			hideTrigger		: false,
			allowBlank		: false,
			queryDelay		: 100,
			minChar			: 1,
			valueField		: 'valoId',
			displayField	: 'valoNombre',
			triggerAction 	: 'all',
			mode			: 'remote',
			store			: {
				xtype: 'combostore',
				model: 'Combo',
			    proxy: {
					type: 'ajax',
					url: 'getProv.do',
					reader: {
						type: 'json',
						root: 'items',
						totalProperty: 'totalCount'
					}
			    },
				autoLoad: true
			},
			forceSelection  : true
		},
		{
			columnWidth: 1,
			id: 'emailContacto',
			name: 'emailContacto',
			fieldLabel: 'Email Contacto',
			margin: '5%',
			xtype: 'textfield'
		},
		/** IMAGEN 1**/
		{
			xtype: 'textfield',
			columnWidth: .50,
			id :'link_1',
			name: 'link1',
			fieldLabel: 'Link Asociado',
			margin: '5%'
		},
		{
			xtype: 'filefield',
			columnWidth: .41,
			id: 'file_1',
			fieldLabel: 'Imagen 1',
			name: 'imagen1',
			buttonText: '',
			buttonConfig:{
			   iconCls: 'upload'
			},
			margin: '5%',
			listeners: {
				change: function(){
					Ext.getCmp('flag1').setValue('1');
				}
			}
		},
		{
			xtype: 'hidden',
			id: 'idImagen1',
			name: 'idImagen1',
			value: '-1'
		},
		{
			xtype: 'hidden',
			id: 'flag1',
			name: 'flag1',
			value: '0'
		},
		{
			xtype: 'button',
			id: 'x1',
			margin: '5%',
			columnWidth: .045,
			iconCls: 'remove-picture',
			//text: 'X',
			hidden: true,
			listeners:{
				click: function(){
					Ext.MessageBox.confirm("Confirmaci&oacute;n","�Esta seguro de que desea eliminar la imagen <b>"+ Ext.getCmp("file_1").getRawValue() +"</b>?<br />La operaci&oacute;n no se podr&aacute; deshacer.", function(btn){
						if (btn=='yes'){
							Ext.getCmp('file_1').setDisabled(false);
							Ext.getCmp('flag1').setValue('1');
							Ext.getCmp('link_1').setDisabled(false);
							Ext.getCmp('link_1').setValue('');
							Ext.getCmp('file_1').setRawValue('');
							Ext.getCmp('v1').setVisible(false);
							Ext.getCmp('x1').setVisible(false);
						}
					})
				}
			}
		},
		{
			xtype: 'button',
			id: 'v1',
			columnWidth: .045,
			margin: '5%',
			iconCls: 'view-picture',
			//text: 'V',
			hidden: true,
			listeners:{
				click: function(b, e){
					Ext.create('Ext.window.Window',{
						id: 'ventana1',
						title: Ext.getCmp('file_1').getRawValue(),
						width: 450,
						height: 450,
						maximizable: false,
						minimizable: false,
						html: '<iframe frameborder="0"  width="100%" height="100%" src="viewImage.do?idImagen='+Ext.getCmp('idImagen1').getValue()+'"/>'
					}).show();
				}
			}
		},
		/** IMAGEN 2 **/
		{
			xtype: 'textfield',
			columnWidth: .50,
			id :'link_2',
			name: 'link2',
			fieldLabel: 'Link Asociado',
			margin: '5%'	
		},
		{
			xtype: 'filefield',
			columnWidth: .41,
			id: 'file_2',
			fieldLabel: 'Imagen 2',
			name: 'imagen2',
			buttonText: '',
			buttonConfig:{
				iconCls: 'upload'
			},
			margin: '5%',
			listeners: {
				change: function(){
					Ext.getCmp('flag2').setValue('1');
				}
			}
		},
		{
			xtype: 'hidden',
			id: 'idImagen2',
			name: 'idImagen2',
			value: '-1'
		},
		{
			xtype: 'hidden',
			id: 'flag2',
			name: 'flag2',
			value: '0'
		},
		{
			xtype: 'button',
			id: 'x2',
			margin: '5%',
			columnWidth: .045,
			iconCls: 'remove-picture',
			//text: 'X',
			hidden: true,
			listeners:{
				click: function(){
					Ext.MessageBox.confirm("Confirmaci&oacute;n","�Esta seguro de que desea eliminar la imagen <b>"+ Ext.getCmp("file_2").getRawValue() +"</b>?<br />La operaci&oacute;n no se podr&aacute; deshacer.", function(btn){
						if (btn=='yes'){					
							Ext.getCmp('file_2').setDisabled(false);
							Ext.getCmp('flag2').setValue('1');
							Ext.getCmp('link_2').setDisabled(false);
							Ext.getCmp('link_2').setValue('');
							Ext.getCmp('file_2').setRawValue('');
							Ext.getCmp('v2').setVisible(false);
							Ext.getCmp('x2').setVisible(false);
						}
					});
				}
			}
		},
		{
			xtype: 'button',
			id: 'v2',
			columnWidth: .045,
			margin: '5%',
			iconCls: 'view-picture',
			//text: 'V',
			hidden: true,
			listeners:{
				click: function(b, e){
					Ext.create('Ext.window.Window',{
						id: 'ventana2',
						title: Ext.getCmp('file_2').getRawValue(),
						width: 450,
						height: 450,
						maximizable: false,
						minimizable: false,
						html: '<iframe frameborder="0"  width="100%" height="100%" src="viewImage.do?idImagen='+Ext.getCmp('idImagen2').getValue()+'"/>'
					}).show();
				}
			}
		},
		/** IMAGEN 3**/
		{			
			xtype: 'textfield',
			columnWidth: .50,
			id :'link_3',
			name: 'link3',
			fieldLabel: 'Link Asociado',
			margin: '5%'
		},
		{
			xtype: 'filefield',
			columnWidth: .41,
			id: 'file_3',
			fieldLabel: 'Imagen 3',
			name: 'imagen3',
			buttonText: '',
			buttonConfig:{
			   iconCls: 'upload'
			},
			margin: '5%',
			listeners: {
				change: function(){
					Ext.getCmp('flag3').setValue('1');
				}
			}
		},
		{
			xtype: 'hidden',
			id: 'idImagen3',
			name: 'idImagen3',
			value: '-1'
		},
		{
			xtype: 'hidden',
			id: 'flag3',
			name: 'flag3',
			value: '0'
		},
		{
			xtype: 'button',
			id: 'x3',
			margin: '5%',
			columnWidth: .045,
			iconCls: 'remove-picture',
			//text: 'X',
			hidden: true,
			listeners:{
				click: function(){
					Ext.MessageBox.confirm("Confirmaci&oacute;n","�Esta seguro de que desea eliminar la imagen <b>"+ Ext.getCmp("file_3").getRawValue() +"</b>?<br />La operaci&oacute;n no se podr&aacute; deshacer.", function(btn){
						if (btn=='yes'){					
							Ext.getCmp('file_3').setDisabled(false);
							Ext.getCmp('flag3').setValue('1');
							Ext.getCmp('link_3').setDisabled(false);
							Ext.getCmp('link_3').setValue('');
							Ext.getCmp('file_3').setRawValue('');
							Ext.getCmp('v3').setVisible(false);
							Ext.getCmp('x3').setVisible(false);
						}
					});
				}
			}
		},
		{
			xtype: 'button',
			id: 'v3',
			columnWidth: .045,
			margin: '5%',
			iconCls: 'view-picture',
			//text: 'V',
			hidden: true,
			listeners:{
				click: function(b, e){
					Ext.create('Ext.window.Window',{
						id: 'ventana3',
						title: Ext.getCmp('file_3').getRawValue(),
						width: 450,
						height: 450,
						maximizable: false,
						minimizable: false,
						html: '<iframe frameborder="0"  width="100%" height="100%" src="viewImage.do?idImagen='+Ext.getCmp('idImagen3').getValue()+'"/>'
					}).show();
				}
			}
		},
		/** IMAGEN 4 **/
		{
			xtype: 'textfield',
			columnWidth: .50,
			id :'link_4',
			name: 'link4',
			fieldLabel: 'Link Asociado',
			margin: '5%'	
		},
		{
			xtype: 'filefield',
			columnWidth: .41,
			id: 'file_4',
			fieldLabel: 'Imagen 4',
			name: 'imagen4',
			buttonText: '',
			buttonConfig:{
				iconCls: 'upload'
			},
			margin: '5%',
			listeners: {
				change: function(){
					Ext.getCmp('flag4').setValue('1');
				}
			}
		},
		{
			xtype: 'hidden',
			id: 'idImagen4',
			name: 'idImagen4',
			value: '-1'
		},
		{
			xtype: 'hidden',
			id: 'flag4',
			name: 'flag4',
			value: '0'
		},
		{
			xtype: 'button',
			id: 'x4',
			margin: '5%',
			columnWidth: .045,
			iconCls: 'remove-picture',
			//text: 'X',
			hidden: true,
			listeners:{
				click: function(){
					Ext.MessageBox.confirm("Confirmaci&oacute;n","�Esta seguro de que desea eliminar la imagen <b>"+ Ext.getCmp("file_4").getRawValue() +"</b>?<br />La operaci&oacute;n no se podr&aacute; deshacer.", function(btn){
						if (btn=='yes'){					
							Ext.getCmp('file_4').setDisabled(false);
							Ext.getCmp('flag4').setValue('1');
							Ext.getCmp('link_4').setDisabled(false);
							Ext.getCmp('link_4').setValue('');
							Ext.getCmp('file_4').setRawValue('');
							Ext.getCmp('v4').setVisible(false);
							Ext.getCmp('x4').setVisible(false);
						}
					});
				}
			}
		},
		{
			xtype: 'button',
			id: 'v4',
			columnWidth: .045,
			margin: '5%',
			iconCls: 'view-picture',
			//text: 'V',
			hidden: true,
			listeners:{
				click: function(b, e){
					Ext.create('Ext.window.Window',{
						id: 'ventana4',
						title: Ext.getCmp('file_4').getRawValue(),
						width: 450,
						height: 450,
						maximizable: false,
						minimizable: false,
						html: '<iframe frameborder="0"  width="100%" height="100%" src="viewImage.do?idImagen='+Ext.getCmp('idImagen4').getValue()+'"/>'
					}).show();
				}
			}
		},
		/** IMAGEN 5 **/
		{
			xtype: 'textfield',
			columnWidth: .50,
			id :'link_5',
			name: 'link5',
			fieldLabel: 'Link Asociado',
			margin: '5%'	
		},
		{
			xtype: 'filefield',
			columnWidth: .41,
			id: 'file_5',
			fieldLabel: 'Imagen 5',
			name: 'imagen5',
			buttonText: '',
			buttonConfig:{
				iconCls: 'upload'
			},
			margin: '5%',
			listeners: {
				change: function(){
					Ext.getCmp('flag5').setValue('1');
				}
			}
		},
		{
			xtype: 'hidden',
			id: 'idImagen5',
			name: 'idImagen5',
			value: '-1'
		},
		{
			xtype: 'hidden',
			id: 'flag5',
			name: 'flag5',
			value: '0'
		},
		{
			xtype: 'button',
			id: 'x5',
			margin: '5%',
			columnWidth: .045,
			iconCls: 'remove-picture',
			//text: 'X',
			hidden: true,
			listeners:{
				click: function(){
					Ext.MessageBox.confirm("Confirmaci&oacute;n","�Esta seguro de que desea eliminar la imagen <b>"+ Ext.getCmp("file_5").getRawValue() +"</b>?<br />La operaci&oacute;n no se podr&aacute; deshacer.", function(btn){
						if (btn=='yes'){					
							Ext.getCmp('file_5').setDisabled(false);
							Ext.getCmp('flag5').setValue('1');
							Ext.getCmp('link_5').setDisabled(false);
							Ext.getCmp('link_5').setValue('');
							Ext.getCmp('file_5').setRawValue('');
							Ext.getCmp('v5').setVisible(false);
							Ext.getCmp('x5').setVisible(false);
						}
					});
				}
			}
		},
		{
			xtype: 'button',
			id: 'v5',
			columnWidth: .045,
			margin: '5%',
			iconCls: 'view-picture',
			//text: 'V',
			hidden: true,
			listeners:{
				click: function(b, e){
					Ext.create('Ext.window.Window',{
						id: 'ventana5',
						title: Ext.getCmp('file_5').getRawValue(),
						width: 450,
						height: 450,
						maximizable: false,
						minimizable: false,
						html: '<iframe frameborder="0"  width="100%" height="100%" src="viewImage.do?idImagen='+Ext.getCmp('idImagen5').getValue()+'"/>'
					}).show();
				}
			}
		},
		/** IMAGEN 6 **/
		{
			xtype: 'textfield',
			columnWidth: .50,
			id :'link_6',
			name: 'link6',
			fieldLabel: 'Link Asociado',
			margin: '5%'	
		},
		{
			xtype: 'filefield',
			columnWidth: .41,
			id: 'file_6',
			fieldLabel: 'Imagen 6',
			name: 'imagen6',
			buttonText: '',
			buttonConfig:{
				iconCls: 'upload'
			},
			margin: '5%',
			listeners: {
				change: function(){
					Ext.getCmp('flag6').setValue('1');
				}
			}
		},
		{
			xtype: 'hidden',
			id: 'idImagen6',
			name: 'idImagen6',
			value: '-1'
		},
		{
			xtype: 'hidden',
			id: 'flag6',
			name: 'flag6',
			value: '0'
		},
		{
			xtype: 'button',
			id: 'x6',
			margin: '5%',
			columnWidth: .045,
			iconCls: 'remove-picture',
			//text: 'X',
			hidden: true,
			listeners:{
				click: function(){
					Ext.MessageBox.confirm("Confirmaci&oacute;n","�Esta seguro de que desea eliminar la imagen <b>"+ Ext.getCmp("file_6").getRawValue() +"</b>?<br />La operaci&oacute;n no se podr&aacute; deshacer.", function(btn){
						if (btn=='yes'){					
							Ext.getCmp('file_6').setDisabled(false);
							Ext.getCmp('flag6').setValue('1');
							Ext.getCmp('link_6').setDisabled(false);
							Ext.getCmp('link_6').setValue('');
							Ext.getCmp('file_6').setRawValue('');
							Ext.getCmp('v6').setVisible(false);
							Ext.getCmp('x6').setVisible(false);
						}
					});
				}
			}
		},
		{
			xtype: 'button',
			id: 'v6',
			columnWidth: .045,
			margin: '5%',
			iconCls: 'view-picture',
			//text: 'V',
			hidden: true,
			listeners:{
				click: function(b, e){
					Ext.create('Ext.window.Window',{
						id: 'ventana6',
						title: Ext.getCmp('file_6').getRawValue(),
						width: 450,
						height: 450,
						maximizable: false,
						minimizable: false,
						html: '<iframe frameborder="0"  width="100%" height="100%" src="viewImage.do?idImagen='+Ext.getCmp('idImagen6').getValue()+'"/>'
					}).show();
				}
			}
		},
		{	
			columnWidth: 1,
			margin: '5%',
			xtype: 'htmleditor',
			allowBlank: false,
			//height: 350,
			id: 'descripcion',
			name: 'descripcion',
			validateOnChange: true,
//			listeners:{
//				sync: function(field, html){
//					console.log(Ext.htmlDecode(html));
//					//Obtengo el texto simple
//					field.getEditorBody().childNodes[0].textContent;
//					field.addCls('.x-form-invalid-field, textarea.x-form-invalid-field');
//				}
//			},
			fieldLabel: '<span style="color: red">Descripci&oacute;n</span>'
		},
		{
			columnWidth: 1,
			margin: '5%',
			border: false,
			html: '<div style="color: red"><b>Los campos en rojo son obligatorios</b></div>'
		}
	]	
});