/**
 * Definici�n del Modelo para la grid del Gestor de plubicaciones
 * @extends: 'Ext.data.Model'
 * @fields: [
 * 		title,	Mapea un string, que es el titulo de la columna principal
 * 		fechaPublicacione, String que se formatea a un tipo fecha
 * 		intervalo, String que indica cada cuanto tiempo se va a publicar el anuncio
 * 		franjaHoraria, String que indica en la franja horaria que se va a publicar
 * 		activo, buleano que indica si el anuncio esta activo
 * ]
 * 
 * @author �lvaro
 */

Ext.define('Anuncios',{
	extend: 'Ext.data.Model',
	fields: [
		{name: 'titulo', 			type: 'string'},
		{name: 'fechaPublicacion',	type: 'string',	dateFormat: "d/m/Y"},
		{name: 'intervalo',			type: 'string'},
		{name: 'franjaHoraria',		type: 'string'},
		{name: 'activo', 			type: 'bool'}
	]
});


/**
 * @class spa.gestor.tree
 * @extends 'Ext.tree.Panel
 * 
 * TreeGrid que gestiona las publicaciones
 * 
 * @author �lvaro
 * @version 1.0
 * @use: Ext.create('spa.gestor.tree')
 */
Ext.define('spa.gestor.tree',{
	extend: 'Ext.tree.Panel',
	
	region: 'center',
	id: 'treeGrid',
	title: 'Gesti&oacute;n de Anuncios',
	rootVisible: false,
    multiSelect: false,
	forceFit: true,
    singleExpand: false,
	simpleSelect: false,
	autoScroll: true,
	bodyCls: 'scroll',
	cls: 'scroll',
	tbar: [
	  '->',
	  {
		  text: 'Cerrar Sesi&oacute;n',
		  iconCls: 'logout',
		  handler: function(){
			  window.location='logout';
		  }
	  }
    ],
	
	store: Ext.create('Ext.data.TreeStore', {
        model: 'Anuncios',
        proxy: {
            type: 'ajax',
            //the store will get the content from the .json file
            url: 'getArbol.do'
        },
        folderSort: true
    }),
	
	listeners:{
		itemcontextmenu: function(view, rec, node, index, e){
			e.stopEvent();
			if (!rec.data.leaf)
				menuFolder.showAt(e.getXY());
			else menuFile.showAt(e.getXY());
			return false;
			
		}
	},			
	columns:[
		{
			xtype: 'treecolumn',
			text: 'Titulo',
			dataIndex: 'titulo',
			hideable: false,
			sortable: true,
			flex: 1
		}/*,
		{
			text: 'Intervalo (min.)',
			sortable: true,
			dataIndex: 'intervalo',
			flex: .10
		},
		{
			text: 'Franja Horaria',
			sortable: true,
			dataIndex: 'franjaHoraria',
			flex: .15
		}*/
	]
	
});