Ext.define('Combo',{
	extend: 'Ext.data.Model',
	fields: [
	    {name: 'valoId',			type: 'int'},
	    {name: 'valoNombre',		type: 'string'}
	]
});