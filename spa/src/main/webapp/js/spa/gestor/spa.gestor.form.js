Ext.define('spa.gestor.form',{
	extend: 'Ext.form.Panel',
	
	alias: 'widget.spawindform',
	autoScroll: true,
	items: [
       /** Identificador del nodo  Padre*/
      /* {
    	   xtype: 'hidden',
    	   id: 'idPadre',
    	   name: 'idPadre',
    	   value: this.idFolder
       },*/
       {
    	   columnWidth: 1,
    	   html: '<div><b>Formulario Datos Interno</b><hr /></div>',
    	   border: false
       },
       {
    	    columnWidth: 1,
    	    margin: '5%',
    	    allowBlank: false,
			id: 'titulo',
			name: 'titulo',
			fieldLabel: 'Titulo',
			xtype: 'textfield'
       },
       /**LUNES**/
       {
    	   xtype: 'checkboxfield',
    	   margin: '5%',
		   id: 'cLunes',
		   name: 'cLunes',
		   boxLabel: 'Lunes',
		   columnWidth: .25,
		   listeners:{
			   change: function(check, newValue, oldValue){
				   if (check.checked){
					   Ext.getCmp('dLunes').setDisabled(false);
					   Ext.getCmp('hLunes').setDisabled(false);
					   Ext.getCmp('iLunes').setDisabled(false);
				   }else{
					   Ext.getCmp('dLunes').setDisabled(true);
					   Ext.getCmp('hLunes').setDisabled(true);
					   Ext.getCmp('iLunes').setDisabled(true);
				   }
			   }
		   }
       },
       {
    	   xtype: 'timefield',
    	   disabled: true,
    	   allowBlank: false,
    	   format: 'H:i',
    	   margin: '5%',
    	   name: 'dLunes',
		   id: 'dLunes',
		   fieldLabel: 'Desde las',
		   columnWidth: .25, 
		   editable:false
       },
       {
    	   xtype: 'timefield',
    	   format: 'H:i',
    	   disabled: true,
    	   allowBlank: false,
    	   margin: '5%',
    	   name: 'hLunes',
		   id: 'hLunes',
		   fieldLabel: 'Hasta las',
		   columnWidth: .25, 
		   editable:false
       },
       {
    	   xtype: 'customspinner',
    	   margin: '5%',
    	   disabled: true,
		   id: 'iLunes',
		   name: 'iLunes',
		   fieldLabel: 'Repetir cada (min.)',
		   labelWidth: 120,
		   columnWidth: .25,
		   value: 3,
		   step: 1, 
		   editable:false
       },										       
       /**MARTES**/
       {
    	   xtype: 'checkboxfield',
		   id: 'cMartes',
		   name: 'cMartes',
		   margin: '5%',
		   boxLabel: 'Martes',
		   columnWidth: .25,
		   listeners:{
			   change: function(check, newValue, oldValue){
				   if (check.checked){
					   Ext.getCmp('dMartes').setDisabled(false);
					   Ext.getCmp('hMartes').setDisabled(false);
					   Ext.getCmp('iMartes').setDisabled(false);
				   }else{
					   Ext.getCmp('dMartes').setDisabled(true);
					   Ext.getCmp('hMartes').setDisabled(true);
					   Ext.getCmp('iMartes').setDisabled(true);
				   }
			   }
		   }
       },
       {
    	   xtype: 'timefield',
    	   format: 'H:i',
    	   disabled: true,
    	   allowBlank: false,
		   id: 'dMartes',
		   margin: '5%',
		   name: 'dMartes',
		   fieldLabel: 'Desde las',
		   columnWidth: .25, 
		   editable:false
       },
       {
    	   xtype: 'timefield',
    	   format: 'H:i',
    	   disabled: true,
    	   allowBlank: false,
    	   margin: '5%',
		   id: 'hMartes',
		   name: 'hMartes',
		   fieldLabel: 'Hasta las',
		   columnWidth: .25, 
		   editable:false
       },
       {
    	   xtype: 'customspinner',
    	   margin: '5%',
    	   disabled: true,
		   id: 'iMartes',
		   name: 'iMartes',
		   fieldLabel: 'Repetir cada (min.)',
		   labelWidth: 120,
		   step: 1,
		   value: 3,
		   columnWidth: .25, 
		   editable:false
       },	
       /**MIERCOLES**/
       {
    	   xtype: 'checkboxfield',
		   id: 'cMiercoles',
		   margin: '5%',
		   name: 'cMiercoles',
		   boxLabel: 'Miercoles',
		   columnWidth: .25,
		   listeners:{
			   change: function(check, newValue, oldValue){
				   if (check.checked){
					   Ext.getCmp('dMiercoles').setDisabled(false);
					   Ext.getCmp('hMiercoles').setDisabled(false);
					   Ext.getCmp('iMiercoles').setDisabled(false);
				   }else{
					   Ext.getCmp('dMiercoles').setDisabled(true);
					   Ext.getCmp('hMiercoles').setDisabled(true);
					   Ext.getCmp('iMiercoles').setDisabled(true);
				   }
			   }
		   }
       },
       {
    	   xtype: 'timefield',
    	   format: 'H:i',
    	   disabled: true,
    	   allowBlank: false,
    	   margin: '5%',
    	   name:'dMiercoles',
		   id: 'dMiercoles',
		   fieldLabel: 'Desde las',
		   columnWidth: .25, 
		   editable:false
       },
       {
    	   xtype: 'timefield',
    	   format: 'H:i',
    	   disabled: true,
    	   allowBlank: false,
    	   name: 'hMiercoles',
    	   margin: '5%',
		   id: 'hMiercoles',
		   fieldLabel: 'Hasta las',
		   columnWidth: .25, 
		   editable:false
       },
       {
    	   xtype: 'customspinner',
    	   margin: '5%',
    	   disabled: true,
		   id: 'iMiercoles',
		   name: 'iMiercoles',
		   fieldLabel: 'Repetir cada (min.)',
		   labelWidth: 120,
		   step: 1,
		   value: 3,
		   columnWidth: .25, 
		   editable:false
       },	
       /**JUEVES**/
       {
    	   xtype: 'checkboxfield',
		   id: 'cJueves',
		   margin: '5%',
		   name: 'cJueves',
		   boxLabel: 'Jueves',
		   columnWidth: .25,
		   listeners:{
			   change: function(check, newValue, oldValue){
				   if (check.checked){
					   Ext.getCmp('dJueves').setDisabled(false);
					   Ext.getCmp('hJueves').setDisabled(false);
					   Ext.getCmp('iJueves').setDisabled(false);
				   }else{
					   Ext.getCmp('dJueves').setDisabled(true);
					   Ext.getCmp('hJueves').setDisabled(true);
					   Ext.getCmp('iJueves').setDisabled(true);
				   }
			   }
		   }
       },
       {
    	   xtype: 'timefield',
    	   format: 'H:i',
    	   disabled: true,
    	   allowBlank: false,
    	   margin: '5%',
		   id: 'dJueves',
		   name: 'dJueves',
		   fieldLabel: 'Desde las',
		   columnWidth: .25, 
		   editable:false
       },
       {
    	   xtype: 'timefield',
    	   format: 'H:i',
    	   disabled: true,
    	   allowBlank: false,
		   id: 'hJueves',
		   margin: '5%',
		   name: 'hJueves',
		   fieldLabel: 'Hasta las',
		   columnWidth: .25, 
		   editable:false
       },
       {
    	   xtype: 'customspinner',
    	   margin: '5%',
    	   disabled: true,
		   id: 'iJueves',
		   name: 'iJueves',
		   fieldLabel: 'Repetir cada (min.)',
		   labelWidth: 120,
		   step: 1,
		   value: 3,
		   columnWidth: .25, 
		   editable:false
       },	
       /**VIERNES**/
       {
    	   xtype: 'checkboxfield',
		   id: 'cViernes',
		   margin: '5%',
		   name: 'cViernes',
		   boxLabel: 'Viernes',
		   columnWidth: .25,
		   listeners:{
			   change: function(check, newValue, oldValue){
				   if (check.checked){
					   Ext.getCmp('dViernes').setDisabled(false);
					   Ext.getCmp('hViernes').setDisabled(false);
					   Ext.getCmp('iViernes').setDisabled(false);
				   }else{
					   Ext.getCmp('dViernes').setDisabled(true);
					   Ext.getCmp('hViernes').setDisabled(true);
					   Ext.getCmp('iViernes').setDisabled(true);
				   }
			   }
		   }
       },
       {
    	   xtype: 'timefield',
    	   format: 'H:i',
    	   disabled: true,
    	   allowBlank: false,
		   id: 'dViernes',
		   name: 'dViernes',
		   margin: '5%',
		   fieldLabel: 'Desde las',
		   columnWidth: .25, 
		   editable:false
       },
       {
    	   xtype: 'timefield',
    	   format: 'H:i',
    	   disabled: true,
    	   allowBlank: false,
		   id: 'hViernes',
		   margin: '5%',
		   name: 'hViernes',
		   fieldLabel: 'Hasta las',
		   columnWidth: .25, 
		   editable:false
       },
       {
    	   xtype: 'customspinner',
    	   margin: '5%',
    	   disabled: true,
		   id: 'iViernes',
		   name: 'iViernes',
		   fieldLabel: 'Repetir cada (min.)',
		   labelWidth: 120,
		   step: 1,
		   value: 3,
		   columnWidth: .25, 
		   editable:false
       },	
       /**SABADO**/
       {
    	   xtype: 'checkboxfield',
		   id: 'cSabado',
		   name:'cSabado',
		   margin: '5%',
		   boxLabel: 'Sabado',
		   columnWidth: .25,
		   listeners:{
			   change: function(check, newValue, oldValue){
				   if (check.checked){
					   Ext.getCmp('dSabado').setDisabled(false);
					   Ext.getCmp('hSabado').setDisabled(false);
					   Ext.getCmp('iSabado').setDisabled(false);
				   }else{
					   Ext.getCmp('dSabado').setDisabled(true);
					   Ext.getCmp('hSabado').setDisabled(true);
					   Ext.getCmp('iSabado').setDisabled(true);
				   }
			   }
		   }
       },
       {
    	   xtype: 'timefield',
    	   format: 'H:i',
    	   disabled: true,
    	   allowBlank: false,
    	   margin: '5%',
		   id: 'dSabado',
		   name: 'dSabado',
		   fieldLabel: 'Desde las',
		   columnWidth: .25, 
		   editable:false
       },
       {
    	   xtype: 'timefield',
    	   format: 'H:i',
    	   disabled: true,
    	   allowBlank: false,
    	   margin: '5%',
		   id: 'hSabado',
		   name: 'hSabado',
		   fieldLabel: 'Hasta las',
		   columnWidth: .25, 
		   editable:false
       },
       {
    	   xtype: 'customspinner',
    	   margin: '5%',
    	   disabled: true,
		   id: 'iSabado',
		   name: 'iSabado',
		   fieldLabel: 'Repetir cada (min.)',
		   labelWidth: 120,
		   step: 1,
		   value: 3,
		   columnWidth: .25, 
		   editable:false
       },	
       /**DOMINGO**/
       {
    	   xtype: 'checkboxfield',
		   id: 'cDomingo',
		   margin: '5%',
		   name: 'cDomingo',
		   boxLabel: 'Domingo',
		   columnWidth: .25,
		   listeners:{
			   change: function(check, newValue, oldValue){
				   if (check.checked){
					   Ext.getCmp('dDomingo').setDisabled(false);
					   Ext.getCmp('hDomingo').setDisabled(false);
					   Ext.getCmp('iDomingo').setDisabled(false);
				   }else{
					   Ext.getCmp('dDomingo').setDisabled(true);
					   Ext.getCmp('hDomingo').setDisabled(true);
					   Ext.getCmp('iDomingo').setDisabled(true);
				   }
			   }
		   }
       },
       {
    	   xtype: 'timefield',
    	   format: 'H:i',
    	   disabled: true,
    	   allowBlank: false,
		   id: 'dDomingo',
		   margin: '5%',
		   name: 'dDomingo',
		   fieldLabel: 'Desde las',
		   columnWidth: .25, 
		   editable:false
       },
       {
    	   xtype: 'timefield',
    	   format: 'H:i',
    	   disabled: true,
    	   allowBlank: false,
		   id: 'hDomingo',
		   name: 'hDomingo',
		   margin: '5%',
		   fieldLabel: 'Hasta las',
		   columnWidth: .25, 
		   editable:false
       },
       {
    	   xtype: 'customspinner',
    	   margin: '5%',
    	   disabled: true,
		   id: 'iDomingo',
		   name: 'iDomingo',
		   fieldLabel: 'Repetir cada (min.)',
		   labelWidth: 120,
		   step: 1,
		   value: 3,
		   columnWidth: .25, 
		   editable:false
       }
	]
	
});