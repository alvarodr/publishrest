

Ext.define('spa.gestor.windowForm',{
	extend: 'Ext.window.Window',
	
	title: 'Escribe el anuncio',
	id: 'anuncio',
	height: screen.height <= 800? 410 : 520,
	width: 900,
	layout: 'fit',
	items: [
        {
		    xtype: 'panel',
		    id: 'panel_anuncio',
		    layout: 'card',
		    idNodo: this.idNodo,
			bbar: [
		       {
		    	   id: 'card-prev',
		    	   text: 'Anterior',
		    	   iconCls: 'back',
		    	   handler: Ext.Function.bind(cardNav, this, [-1]),
		    	   disabled: true
		       },
		       '->',
		       {
		    	   id: 'card-next',
		    	   text:'Siguiente',
		    	   iconCls: 'next',
		    	   handler: function(){
		    		   var node = Ext.getCmp('treeGrid').getView().getSelectionModel().getSelection()[0];
		    		   if (node.data.leaf)
		    			   cardNav(1, node.data.id);
	    			   else
	    				   cardNav(1, '-1', node.data.id);
		    	   }
		       },
		       {
		    	   text: 'Cancelar',
		    	   iconCls: 'cancel',
		    	   handler: function(){
		    		   Ext.getCmp('anuncio').close();
		    	   }
		       }
			 ],
			 items: [
		         {
		        	 xtype: 'spawindform',
		        	 id: 'card-0',
		        	 layout: 'column',
		        	 bodyStyle: 'padding: 50px',
		        	 idFolder: this.idPadre,
		        	 defaults: {
		        		 labelWidth: 75
		        	 }
		         },
		         {
		        	 xtype: 'spawindforma',
		        	 id: 'card-1',
		        	 layout: 'column',
		        	 bodyStyle: 'padding: 50px'
		         }
			]
        }
	]
});