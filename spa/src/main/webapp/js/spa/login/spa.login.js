Ext.define('spa.login', {
	extend: 'Ext.form.Panel',
	id:'formLogin',
	items: [
	    {
	    	xtype: 'textfield',
	    	id: 'user',
	    	name: 'j_username',
	    	fieldLabel: 'Usuario',
	    	allowBlank: false
	    },{
	    	xtype: 'textfield',
	    	inputType: 'password',
	    	id: 'password',
	    	fieldLabel: 'Contrase&ntilde;a',
	    	name: 'j_password',
	    	allowBlank: false,
	    	listeners: {"keypress": {fn: function(textfield,event){
				if(event.getKey()==13){
					Ext.getCmp('formLogin').getForm().submit();
				}							
			}}}
	    }
	],
	
	buttons: [
	    {
	    	text: 'Acceder',
	    	handler: function(){
	    		if (Ext.getCmp('formLogin').getForm().isValid()){
	    			Ext.getCmp('formLogin').getForm().submit();
	    		}
	    	}
	    }
	]
});