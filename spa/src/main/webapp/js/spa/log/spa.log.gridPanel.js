/**
 * Definici�n del Modelo para la grid de log
 * 
 * @extends: 'Ext.data.Model'
 * @fields: [
 * 		anuncio, String que describe el titulo del anuncio
 * 		fecha, Fecha de publicacion
 * ]
 * 
 * @author �lvaro
 */

Ext.define('Log',{
	extend: 'Ext.data.Model',
	fields: [
	    {name: 'anuncio',		type:'string'},
	    {name: 'fecha',			type:'date'},
	    {name: 'estado',		type:'bool'}
	]
});

var groupingFeature = Ext.create('Ext.grid.feature.Grouping',{
    groupHeaderTpl: 'Anuncio: {name} ({rows.length} Item{[values.rows.length > 1 ? "s" : ""]})'
});

var store = Ext.create('Ext.data.Store',{
	model: 'Log',
    sorters: [{
    	property: 'anuncio'
    },
    {
    	property: 'fecha',
    	direction: 'DESC'
    }],
    groupField: 'anuncio',
    pageSize: 20,
	proxy: {
		type: 'ajax',
		url: 'log.do',
		reader: {
			type: 'json',
			root: 'items',
			totalProperty: 'totalCount'
		}
	}
});

/**
 * @class spa.log.gridPanel
 * @extend: 'Ext.grid.Panel'
 * 
 * Clase que muestra los anuncios que se han mostrado
 * 
 * @author �lvaro
 * @version 1.0
 * 
 * @use: Ext.create('spa.log.girdPanel,{})
 */
Ext.define('spa.log.gridPanel',{
	extend: 'Ext.grid.Panel',
	id: 'logPanel',
	title: 'Publicaciones Realizadas',
	region:'center',
	store: store,

	features:[groupingFeature],
	//autoScroll: true,
	viewConfig: {
		forceFit: true,
		enableRowBody	: true,
		autoScroll: true
		//componentCls: 'scroll'
	},
	tbar: [
 	  '->',
 	  {
 		  text: 'Cerrar Sesi&oacute;n',
 		  iconCls: 'logout',
 		  handler: function(){
 			  window.location='logout';
 		  }
 	  }
     ],
     dockedItems:[
          {
        	  xtype: 'pagingtoolbar',
        	  store: store,
        	  dock: 'bottom',
        	  displayInfo: true
          }
     ],
	columns: [
      {
    	  xtype: 'gridcolumn',
    	  header: 'Anuncio',
    	  dataIndex: 'anuncio',
    	  width: 450
      },
      {
    	  xtype: 'gridcolumn',
    	  header: 'Fecha',
    	  dataIndex: 'fecha',
    	  width: 450,
    	  //width: 600,
    	  renderer: Ext.util.Format.dateRenderer('l d \\d\\e F \\d\\e Y H:i')
      },
      {
    	  xtype: 'gridcolumn',
    	  header: 'Estado',
    	  dataIndex: 'estado',
    	  width: 100,
    	  renderer: function(value, cell){
    		  return value?'<img src="ico/accept.png">':'<img src="ico/cancel.png">'
    	  }
      }
	]
});