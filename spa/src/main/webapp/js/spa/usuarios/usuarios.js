Ext.require(['*','Ext.selection.CellModel','Ext.ux.CheckColumn']);

Ext.define('Usuarios',{
	extend: 'Ext.data.Model',
	fields:[
        {name: 'usuario',		type: 'string'},
        {name: 'activo',	type: 'bool'}
    ]
});

var dsUsers = Ext.create('Ext.data.Store',{
	model: 'Usuarios',
	proxy: {
		type: 'ajax',
		url: 'admin/getUsuarios.do',
		reader: {
			type: 'json',
			root: 'items',
			totalProperty: 'totalCount'
		}
	},
	autoLoad: true
});

var grid_users = Ext.create('Ext.grid.Panel',{
	title: 'Gesti&oacute;n Usuarios',
	store: dsUsers,
	//selModel: Ext.create('Ext.selection.CheckboxModel'),
	tbar: [
	       {
	    	   text: 'Nuevo Usuario',
	    	   iconCls: 'add-user',
	    	   handler: function (){
	    		   /*var r = Ext.create('Usuarios',{});
	    		   dsUsers.insert(0,r);
	    		   cellEditing.startEditByPosition({row: 0, column: 0});*/
	    		   Ext.create('Ext.window.Window',{
	    				title: 'Nuevo Usuario',
	    				id: 'win_user',
	    				layout: 'fit',
	    				width: 300,
	    				items:[
	    				       {
	    				    	   xtype: 'form',
	    				    	   id: 'form_new_user',
	    				    	   layout: 'column',
	    				    	   bodyStyle: 'padding: 10px',
	    				    	   buttonAlign: 'center',
	    				    	   defaults:{
	    				    		   margin: 5,
	    				    		   labelWidth: 120
	    				    	   },
	    				    	   items:[
	    				    	          {
	    				    	        	  columnWidth: 1,
	    				    	        	  xtype: 'textfield',
	    				    	        	  fieldLabel: 'Usuario',
	    				    	        	  allowBlank: false,
	    				    	        	  name: 'user',
	    				    	        	  id: 'user'
	    				    	          },
	    				    	          {
	    				    	        	  columnWidth: 1,
	    				    	        	  xtype: 'textfield',
	    				    	        	  fieldLabel: 'Contrase&ntilde;a',
	    				    	        	  allowBlank: false,
	    				    	        	  inputType: 'password',
	    				    	        	  name: 'pass',
	    				    	        	  id: 'pass'
	    				    	          },
	    				    	          {
	    				    	        	  columnWidth: 1,
	    				    	        	  xtype: 'textfield',
	    				    	        	  allowBlank:false,
	    				    	        	  fieldLabel: 'Repita Contrase&ntilde;a',
	    				    	        	  inputType: 'password',
	    				    	        	  name: 'pass-repeat',
	    				    	        	  id: 'pass-repeat',
	    				    	        	  vtype: 'password',
	    				    	        	  initialPassField: 'pass'
	    				    	          }
	    				    	          ],
	    				    	   buttons: [
	    				    	             {
	    				    	            	 text: 'Alta Usuario',
	    				    	            	 iconCls: 'add',
	    				    	            	 handler: function(){
	    				    	            		 if (Ext.getCmp('pass-repeat').isValid() && Ext.getCmp('form_new_user').getForm().isValid()){
	    					    	            		 /*var r = Ext.create('Usuarios',{usuario: Ext.getCmp('user').getValue()});
	    					    	            		 dsUsers.insert(0,r);*/
	    				    	            			 
	    				    	            			 Ext.Ajax.request({
	    				    									url: 'admin/nuevoUsuario.do',
	    				    									params:{
	    				    										usuario: Ext.getCmp('user').getValue(),
	    				    										password: Ext.getCmp('pass').getValue()
	    				    									},
	    				    									success: function(){
	    				    										dsUsers.load();
	    				    										Ext.getCmp('win_user').close();
	    				    									},
	    				    									failure: function(){
	    				    										Ext.Msg.show({
	    				    											title: 'Error',
	    				    											msg: 'No se ha podido crear el usuario',
	    				    											icon: Ext.Msg.ERROR,
	    				    											buttons: Ext.Msg.OK
	    				    										})
	    				    									}
	    				    								});
	    				    	            			 
	    				    	            		 }else
	    				    	            			 Ext.Msg.show({
	    				    	            				 title: 'Error',
	    				    	            				 msg: 'Las contraseņas no coinciden o existe algun campo vacio',
	    				    	            				 buttons: Ext.Msg.OK,
	    				    	            				 icon: Ext.Msg.ERROR
	    				    	            			 });
	    				    	            	 }
	    				    	             },
	    				    	             '->',
	    				    	             {
	    				    	            	 text: 'Cancelar',
	    				    	            	 iconCls: 'cancel',
	    				    	            	 handler: function(){
	    				    	            		 Ext.getCmp('win_user').close();
	    				    	            	 }
	    				    	             }
	    				    	             ]
	    				       }
	    				       ]
	    			}).show();
	    	   }
	       },
	       {
	    	   text: 'Eliminar Usuario',
	    	   iconCls: 'del-user',
	    	   handler: function(){
	    		   if (grid_users.getSelectionModel().getCount()>0){
	    			   
	    			   Ext.Ajax.request({
							url: 'admin/eliminarUsuario.do',
							params:{
								email: grid_users.getSelectionModel().getSelection()[0].data.usuario
							},
							success: function(){
								dsUsers.load();
							},
							failure: function(){
								Ext.Msg.show({
									title: 'Error',
									msg: 'No se ha podido eliminar el usuario',
									icon: Ext.Msg.ERROR,
									buttons: Ext.Msg.OK
								})
							}
						});
	    		   		
	    		   }
	    		   else
	    			   Ext.Msg.show({
	    				  title: 'Aviso',
	    				  msg: 'Debes seleccionar un usuario',
	    				  buttons: Ext.Msg.OK,
	    				  icon: Ext.Msg.WARNING
	    			   });
	    	   }
	       },
	       {
	    	   text: 'Cambiar Password',
	    	   iconCls: 'password',
	    	   handler: function(){
	    		   if (grid_users.getSelectionModel().getCount()==1){
	    			   Ext.MessageBox.textField.inputType='password';
	    			   //console.dir(Ext.MessageBox);
	    			   Ext.MessageBox.prompt(
    					   'Cambiar Contraseņa', 
    					   'Introduzca la nueva contraseņa:',
    					   function(btn, text){
		    				   if (text!='' && btn=='ok'){
		    					   
		    					   Ext.Ajax.request({
			   							url: 'admin/cambiarPassword.do',
			   							params:{
			   								email: grid_users.getSelectionModel().getSelection()[0].data.usuario,
			   								password: text
			   							},
			   							success: function(){
			   								Ext.Msg.show({
					    						   title: 'Informacion',
					    						   msg: 'La contraseņa ha sido cambiada',
					    						   buttons: Ext.Msg.OK,
					    						   icon: Ext.Msg.INFO
					    					});
			   							},
			   							failure: function(){
			   								Ext.Msg.show({
			   									title: 'Error',
			   									msg: 'No se ha podido cambiar la contraseņa',
			   									icon: Ext.Msg.ERROR,
			   									buttons: Ext.Msg.OK
			   								})
			   							}
		   							});
		    				   }
	    				   }
	    			   );	
		    	   }else if (grid_users.getSelectionModel().getCount()>1)
		    		   	Ext.Msg.show({
		    				  title: 'Aviso',
		    				  msg: 'Solo puedes seleccionar un usuario',
		    				  buttons: Ext.Msg.OK,
		    				  icon: Ext.Msg.WARNING	    				   
	    			   });
	    		   else if (grid_users.getSelectionModel().getCount()==0)
	    			   Ext.Msg.show({
		    				  title: 'Aviso',
		    				  msg: 'Debes seleccionar un usuario',
		    				  buttons: Ext.Msg.OK,
		    				  icon: Ext.Msg.WARNING
		    			   });
	    	   }
	       },
	       '->',
	       {
	    	   text: 'Cerrar Sesi&oacute;n',
	    	   iconCls: 'logout',
	    	   handler: function(){
	    		   window.location='logout';
	    	   }
	       }
	       ],
	columns: [
	          {
	        	  header: 'Usuario', 
	        	  dataIndex: 'usuario',
	        	  flex: 1
	          },{
	        	  xtype: 'checkcolumn',
	        	  header: 'Activo',
	        	  dataIndex: 'activo',
	        	  listeners:{
	        		  checkchange: function(column,rowIndex,checked) {
	        			  grid_users.getSelectionModel().select(rowIndex);
	        			  Ext.Ajax.request({
	   							url: 'admin/cambiarActivo.do',
	   							params:{
	   								email: grid_users.getSelectionModel().getSelection()[0].data.usuario,
	   								activo: checked
	   							},
	   							success: function(){
	   								dsUsers.load();
	   							},
	   							failure: function(){
	   								Ext.Msg.show({
	   									title: 'Error',
	   									msg: 'No se ha podido cambiar el estado del usuario',
	   									icon: Ext.Msg.ERROR,
	   									buttons: Ext.Msg.OK
	   								})
	   							}
 							});
	        		  	}
	        	     }
	        	  }
	          ]
});

Ext.onReady(function(){
	
    // Add the additional 'advanced' VTypes
    Ext.apply(Ext.form.field.VTypes, {
        password: function(val, field) {
            if (field.initialPassField) {
                var pwd = field.up('form').down('#' + field.initialPassField);
                return (val == pwd.getValue());
            }
            return true;
        },

        passwordText: 'Las contraseņas no coinciden'
    });
	
	Ext.create('Ext.container.Viewport',{
		margin: 10,
		baseCls: 'x-style-html-body',
		layout: 'fit',
		items:[grid_users]
	});
});