Ext.require(['*','Ext.ux.CheckColumn','Ext.selection.CellModel']);

var gridLog = Ext.create('spa.log.gridPanel');

var treeGrid = Ext.create('spa.gestor.tree');

var tree = Ext.create('spa.publi.tree');

//var ventana = Ext.create('spa.gestor.windowForm');
	
eliminarNodo = function(nodo){
	Ext.Ajax.request({
		url: 'delFolder.do',
		params: {
			id: nodo.data.id
		},
		success: function(){
			//nodo.remove();
			Ext.getCmp('treeGrid').getStore().load();
			Ext.getCmp('tree').getStore().load();
		},
		failure: function(){
			Ext.Msg.show({
				title: 'Error',
				msg: 'Se produjo error interno',
				icon: Ext.Msg.ERROR,
				buttons: Ext.Msg.OK
			});
		}
	});					
};

var menuFile = new Ext.menu.Menu({
	items: [
		{
			text: 'Editar Anuncio',
			iconCls: 'edit_doc',
			handler: function(){
				var node = Ext.getCmp('treeGrid').getView().getSelectionModel().getSelection()[0];
				var ventana = Ext.create('spa.gestor.windowForm',{
					id: 'anuncio'
				}).show();
				
				Ext.getCmp('card-0').getForm().load({
					url: 'loadFormAg.do',
					waitTitle: 'Espere',
					waitMsg: 'Cargando...',
					params: {
						id: node.data.id
					},
					failure: function(){
		    			Ext.Msg.show({
		    				title: "Error",
		    				msg:"Error al recuperar los datos del anuncio",
		    				icon: Ext.Msg.ERROR,
		    				buttons: Ext.Msg.OK
		    			});						
					}
				});
			}
		},
		{
			text: 'Eliminar Anuncio',
			iconCls: 'del_doc',
			handler: function(){
				var nodo = Ext.getCmp('treeGrid').getView().getSelectionModel().getSelection()[0];
				eliminarNodo(nodo);	
			}
		}
	]
});
	
var menuFolder = new Ext.menu.Menu({
	items: [
		{
			text: 'Crear Carpeta',
			iconCls: 'new_dir',
			handler: function(){
				var folder = Ext.getCmp('treeGrid').getView().getSelectionModel().getSelection()[0];
				Ext.Msg.prompt(
					'Nueva Carpeta',
					'Nombre',
					function(btn, txt){
						if (btn=='ok'){
							Ext.Ajax.request({
								url: 'newFolder.do',
								params:{
									titulo: txt,
									leaf: false,
									padre: folder.data.id 
								},
								success: function(){
									Ext.getCmp('treeGrid').getStore().load();
								},
								failure: function(){
									Ext.Msg.show({
										title: 'Error',
										msg: 'No se ha podido crear el directorio',
										icon: Ext.Msg.ERROR,
										buttons: Ext.Msg.OK
									})
								}
							})
						}
					}
				);
			}
		},
		{
			text: 'Eliminar Carpeta',
			iconCls: 'del_dir',
			handler: function(){
				var nodo =Ext.getCmp('treeGrid').getView().getSelectionModel().getSelection()[0];
				if (nodo.data.id=='-1'){
					Ext.Msg.show({
						title: 'Error',
						msg: 'No se puede eliminar el directorio <b>"Root"</b>',
						icon: Ext.Msg.ERROR,
						buttons: Ext.Msg.OK
					});
				}else{
					eliminarNodo(nodo);				
				}
			}
		},
		{
			text: 'Nuevo Anuncio',
			iconCls: 'new_doc',
			handler: function(){
				var folder = Ext.getCmp('treeGrid').getView().getSelectionModel().getSelection()[0];
				var ventana = Ext.create('spa.gestor.windowForm',{
					id: 'anuncio'
				}).show();
			}
		}
	]
});
   
    	

Ext.onReady(function(){
	
    Ext.getBody().on("contextmenu", Ext.emptyFn, null, {preventDefault: true});
	
	Ext.create('Ext.container.Viewport', {
		layout: 'border',
		//plain: false,
		baseCls: 'x-style-html-body',
		padding: '20 20 20 20',
		//margin: '10px 10px 10px 10px',
		items: [
			{ 			
				xtype: 'verticaltabpanel',
				activeTab: 0,
				plain: true,
				region: 'center',
				defaults:{
					style: {position: !Ext.isIE?'absolute':'relative'},
					hideMode  :!Ext.isIE?'visibility':'display'
				},				
				tabsConfig: {
					width: 50,
					height: 40
				},
				items: [
					{
						border: false,
						layout: 'border',
						//title: 'Anuncios',
						icon: 'ico/manageIcon.png',
						items:[treeGrid]
					},
					{
						layout: 'fit',
						//title: 'Publicaciones',
						border: false,
						icon: 'ico/agendaIcon.png',
						items: [tree]
					},
					{
						layout: 'border',
						//title: 'Publicaciones',
						border: false,
						icon: 'ico/logIcon.png',
						items: [gridLog]
					}
				]
			}
		]
	});
});
