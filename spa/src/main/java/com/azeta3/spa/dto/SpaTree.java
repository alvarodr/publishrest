package com.azeta3.spa.dto;

import java.io.Serializable;
import java.util.List;

public class SpaTree implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2461447025217966719L;

	/** Identificador del nodo **/
	private Long id;
	
	/** Columna titulo **/
	private String titulo;
	
	/** Columna Intervalo **/
	private Integer intervalo;
	
	/** Columna Franja Horaria **/
	private String franjaHoraria;
	
	/** Identifica si es nodo hijo (true) o no (false) **/
	private Boolean leaf;
	
	/** Clase CSS donde se define el icono **/
	private String iconCls;
	
	/** Cataloga si el anuncio esta activo o no **/
	private Boolean checked;
	
	/** Hijos del nodo **/
	private List<SpaTree> children;
	
	/** Si el nodo es expandible **/
	private Boolean expanded;

	public Boolean getExpanded() {
		return expanded;
	}

	public void setExpanded(Boolean expanded) {
		this.expanded = expanded;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Integer getIntervalo() {
		return intervalo;
	}

	public void setIntervalo(Integer intervalo) {
		this.intervalo = intervalo;
	}

	public String getFranjaHoraria() {
		return franjaHoraria;
	}

	public void setFranjaHoraria(String franjaHoraria) {
		this.franjaHoraria = franjaHoraria;
	}

	public Boolean getLeaf() {
		return leaf;
	}

	public void setLeaf(Boolean leaf) {
		this.leaf = leaf;
	}

	public String getIconCls() {
		return iconCls;
	}

	public void setIconCls(String iconCls) {
		this.iconCls = iconCls;
	}

	public Boolean getChecked() {
		return checked;
	}

	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	public List<SpaTree> getChildren() {
		return children;
	}

	public void setChildren(List<SpaTree> children) {
		this.children = children;
	}	
}
