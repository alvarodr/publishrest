package com.azeta3.spa.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class SpaImagenDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5302264086564555382L;
	private MultipartFile imagen1;
	private MultipartFile imagen2;
	private MultipartFile imagen3;
	private MultipartFile imagen4;
	private MultipartFile imagen5;
	private MultipartFile imagen6;
	private Long idImagen1;
	private Long idImagen2;
	private Long idImagen3;
	private Long idImagen4;
	private Long idImagen5;
	private Long idImagen6;
	private Integer flag1;
	private Integer flag2;
	private Integer flag3;
	private Integer flag4;
	private Integer flag5;
	private Integer flag6;
	private String link1;
	private String link2;
	private String link3;
	private String link4;
	private String link5;
	private String link6;
	
	public List<Long> getIdsImagenes(){
		List<Long> idsImagenes = new ArrayList<Long>();
		idsImagenes.add(idImagen1);
		idsImagenes.add(idImagen2);
		idsImagenes.add(idImagen3);
		idsImagenes.add(idImagen4);
		idsImagenes.add(idImagen5);
		idsImagenes.add(idImagen6);
		return idsImagenes;
	}
	
	public List<MultipartFile> getImagenes(){
		List<MultipartFile> imagenes = new ArrayList<MultipartFile>();
		if(imagen1 != null && imagen1.getSize() > 0) imagenes.add(imagen1);
		if(imagen2 != null && imagen2.getSize() > 0) imagenes.add(imagen2);
		if(imagen3 != null && imagen3.getSize() > 0) imagenes.add(imagen3);
		if(imagen4 != null && imagen4.getSize() > 0) imagenes.add(imagen4);
		if(imagen5 != null && imagen5.getSize() > 0) imagenes.add(imagen5);
		if(imagen6 != null && imagen6.getSize() > 0) imagenes.add(imagen6);		
		return imagenes;
	}
	public MultipartFile getImagen1() {
		return imagen1;
	}
	public void setImagen1(MultipartFile imagen1) {
		this.imagen1 = imagen1;
	}
	public MultipartFile getImagen2() {
		return imagen2;
	}
	public void setImagen2(MultipartFile imagen2) {
		this.imagen2 = imagen2;
	}
	public MultipartFile getImagen3() {
		return imagen3;
	}
	public void setImagen3(MultipartFile imagen3) {
		this.imagen3 = imagen3;
	}
	public String getLink1() {
		return link1;
	}
	public void setLink1(String link1) {
		this.link1 = link1;
	}
	public String getLink2() {
		return link2;
	}
	public void setLink2(String link2) {
		this.link2 = link2;
	}
	public String getLink3() {
		return link3;
	}
	public void setLink3(String link3) {
		this.link3 = link3;
	}
	public Long getIdImagen1() {
		return idImagen1;
	}
	public void setIdImagen1(Long idImagen1) {
		this.idImagen1 = idImagen1;
	}
	public Long getIdImagen2() {
		return idImagen2;
	}
	public void setIdImagen2(Long idImagen2) {
		this.idImagen2 = idImagen2;
	}
	public Long getIdImagen3() {
		return idImagen3;
	}
	public void setIdImagen3(Long idImagen3) {
		this.idImagen3 = idImagen3;
	}
	public Integer getFlag1() {
		return flag1;
	}
	public void setFlag1(Integer flag1) {
		this.flag1 = flag1;
	}
	public Integer getFlag2() {
		return flag2;
	}
	public void setFlag2(Integer flag2) {
		this.flag2 = flag2;
	}
	public Integer getFlag3() {
		return flag3;
	}
	public void setFlag3(Integer flag3) {
		this.flag3 = flag3;
	}

	public MultipartFile getImagen4() {
		return imagen4;
	}

	public void setImagen4(MultipartFile imagen4) {
		this.imagen4 = imagen4;
	}

	public MultipartFile getImagen5() {
		return imagen5;
	}

	public void setImagen5(MultipartFile imagen5) {
		this.imagen5 = imagen5;
	}

	public MultipartFile getImagen6() {
		return imagen6;
	}

	public void setImagen6(MultipartFile imagen6) {
		this.imagen6 = imagen6;
	}

	public Long getIdImagen4() {
		return idImagen4;
	}

	public void setIdImagen4(Long idImagen4) {
		this.idImagen4 = idImagen4;
	}

	public Long getIdImagen5() {
		return idImagen5;
	}

	public void setIdImagen5(Long idImagen5) {
		this.idImagen5 = idImagen5;
	}

	public Long getIdImagen6() {
		return idImagen6;
	}

	public void setIdImagen6(Long idImagen6) {
		this.idImagen6 = idImagen6;
	}

	public Integer getFlag4() {
		return flag4;
	}

	public void setFlag4(Integer flag4) {
		this.flag4 = flag4;
	}

	public Integer getFlag5() {
		return flag5;
	}

	public void setFlag5(Integer flag5) {
		this.flag5 = flag5;
	}

	public Integer getFlag6() {
		return flag6;
	}

	public void setFlag6(Integer flag6) {
		this.flag6 = flag6;
	}

	public String getLink4() {
		return link4;
	}

	public void setLink4(String link4) {
		this.link4 = link4;
	}

	public String getLink5() {
		return link5;
	}

	public void setLink5(String link5) {
		this.link5 = link5;
	}

	public String getLink6() {
		return link6;
	}

	public void setLink6(String link6) {
		this.link6 = link6;
	}
	
}
