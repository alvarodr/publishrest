package com.azeta3.spa.dto;

public class SpaCombo {
	private Long valoId;
	private String valoNombre;
	
	public Long getValoId() {
		return valoId;
	}
	
	public void setValoId(Long valoId) {
		this.valoId = valoId;
	}
	
	public String getValoNombre() {
		return valoNombre;
	}
	
	public void setValoNombre(String valoNombre) {
		this.valoNombre = valoNombre;
	}
}
