package com.azeta3.spa.constantes;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public enum DiasSemanaEnum implements Serializable{
	@Persistent
	LUNES ("LUNES"),
	@Persistent
    MARTES ("MARTES"),
    @Persistent
    MIERCOLES ("MIERCOLES"),
    @Persistent
	JUEVES ("JUEVES"),
	@Persistent
	VIERNES ("VIERNES"),
	@Persistent
	SABADO ("SABADO"),
	@Persistent
	DOMINGO ("DOMINGO");
	
	private String dia;
	
	DiasSemanaEnum(String dia){
		this.dia = dia;
	}
	
	public String getDia(){
		return this.dia;
	}
	
	public Integer getPositionDia(){
		switch (DiasSemanaEnum.valueOf(dia)) {
			case LUNES: return 0;
			case MARTES: return 1;
			case MIERCOLES: return 2;
			case JUEVES: return 3;
			case VIERNES: return 4;
			case SABADO: return 5;
			case DOMINGO: return 6;
			default:
				return -1;
		}
	}
	
	public static Calendar getDiaSemanaEnCalendar(DiasSemanaEnum d){
		Calendar calendar = new GregorianCalendar();
		
		switch (d) {
			case LUNES: 
				calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
				return calendar;
			case MARTES: 
				calendar.set(Calendar.DAY_OF_WEEK, Calendar.TUESDAY);
				return calendar;
			case MIERCOLES: 
				calendar.set(Calendar.DAY_OF_WEEK, Calendar.WEDNESDAY);
				return calendar;
			case JUEVES: 
				calendar.set(Calendar.DAY_OF_WEEK, Calendar.THURSDAY);
				return calendar;
			case VIERNES: 
				calendar.set(Calendar.DAY_OF_WEEK, Calendar.FRIDAY);
				return calendar;
			case SABADO: 
				calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
				return calendar;
			case DOMINGO: 
				calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
				return calendar;
			default:
				return null;
		}
	}
	
	public static DiasSemanaEnum getDiaSemanaDeCalendar(Calendar dia){
		
		switch (dia.get(Calendar.DAY_OF_WEEK)) {
			case Calendar.MONDAY: return LUNES;
			case Calendar.TUESDAY: return MARTES;
			case Calendar.WEDNESDAY: return MIERCOLES;
			case Calendar.THURSDAY: return JUEVES;
			case Calendar.FRIDAY: return VIERNES;
			case Calendar.SATURDAY: return SABADO;
			case Calendar.SUNDAY: return DOMINGO;
			default:
				return null;
		}
	}
}
