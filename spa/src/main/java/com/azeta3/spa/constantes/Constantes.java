package com.azeta3.spa.constantes;

public class Constantes {

		public static final String USUARIOENSESION = "USUARIO";
		
		public static final Integer LUNES = 0;
		public static final Integer MARTES = 1;
		public static final Integer MIERCOLES = 2;
		public static final Integer JUEVES = 3;
		public static final Integer VIERNES = 4;
		public static final Integer SABADO = 5;
		public static final Integer DOMINGO = 6;
		
		public static final String APPLICATION_NAME = "publicaciondeanuncios-v2";
}
