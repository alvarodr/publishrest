package com.azeta3.spa.dao.impl;

import java.util.List;

import javax.jdo.FetchPlan;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jdo.support.JdoDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.azeta3.spa.bom.SpaProxy;
import com.azeta3.spa.dao.ProxyDAO;

@Repository
public class ProxyDaoImpl extends JdoDaoSupport implements ProxyDAO {

	@Autowired
    public ProxyDaoImpl(PersistenceManagerFactory pmf)
    {
		setPersistenceManagerFactory(pmf);
    }

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<SpaProxy> findAll() {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(SpaProxy.class);
			List<SpaProxy> results = (List<SpaProxy>) query.execute();
			return results;
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public SpaProxy create(SpaProxy proxy) {
		return getPersistenceManager().makePersistent(proxy);
	}

	@Override
	public void delete(Long id) {
		try{
			getPersistenceManager().deletePersistent(
				getPersistenceManager().getObjectById(SpaProxy.class,id));
		}catch(Exception e){}
	}

	@Override
	public void update(SpaProxy proxy) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			SpaProxy proxyFound = (SpaProxy) getPersistenceManager().getObjectById(proxy.getId());
			
			proxyFound.setConexionesFallidas(proxy.getConexionesFallidas());
			proxyFound.setHost(proxy.getHost());
			proxyFound.setPort(proxy.getPort());
			proxyFound.setSocks(proxy.getSocks());
			
		}catch(Exception e){
		}
		
	}

	@Override
	public SpaProxy findById(Long id) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			return (SpaProxy) getPersistenceManager().getObjectById(id);
		}catch(Exception e){
			return null;
		}
	}
	
	

}