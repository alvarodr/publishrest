package com.azeta3.spa.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.azeta3.spa.bom.SpaPublicacionEfectiva;

/**
 * 
 * @author ADORU3N
 *
 */
@Repository
public interface PublicacionEfectivaDAO {
	
	/**
	 * 
	 * @param publicacion
	 * @return
	 */
	SpaPublicacionEfectiva create(SpaPublicacionEfectiva publicacion);

	/**
	 * 
	 * @param publicacion
	 * @return
	 */
	SpaPublicacionEfectiva update(SpaPublicacionEfectiva publicacion);

	/**
	 * 
	 * @param idSpaPublicacionEfectiva
	 */
	void remove(Long idSpaPublicacionEfectiva);

	/**
	 * 
	 * @param id
	 * @return
	 */
	SpaPublicacionEfectiva findById(Long id);

	/**
	 * 
	 * @param idAnuncio
	 * @return
	 */
	List<SpaPublicacionEfectiva> findByAnuncio(Long idAnuncio);

	/**
	 * 
	 * @param usuario
	 * @param start
	 * @param limit
	 * @return
	 */
	List<SpaPublicacionEfectiva> findByAnuncio(String usuario, Integer start, Integer limit);

	/**
	 * 
	 * @param usuario
	 * @param start
	 * @param limit
	 * @return
	 */
	Integer getTotalByUsuario(String usuario, Integer start, Integer limit);

	/**
	 * 
	 * @param idAnuncio
	 * @return
	 */
	SpaPublicacionEfectiva findMasAntigua(Long idAnuncio);

	/**
	 * 
	 * @param idPublicacionProgramada
	 * @return
	 */
	List<SpaPublicacionEfectiva> findByPublicacionProgramada(Long idPublicacionProgramada);
}
