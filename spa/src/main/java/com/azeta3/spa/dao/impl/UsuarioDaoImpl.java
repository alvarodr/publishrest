package com.azeta3.spa.dao.impl;

import java.util.List;

import javax.jdo.FetchPlan;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jdo.support.JdoDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.azeta3.spa.bom.SpaUsuario;
import com.azeta3.spa.dao.UsuarioDAO;
import com.azeta3.spa.security.Roles;

@Repository
public class UsuarioDaoImpl extends JdoDaoSupport implements UsuarioDAO {
	
//	private Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
    public UsuarioDaoImpl(PersistenceManagerFactory pmf)
    {
		setPersistenceManagerFactory(pmf);
    }
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public SpaUsuario findById(String email) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(SpaUsuario.class);
			query.setFilter("email == emailParam");
			query.declareParameters("String emailParam");
			List<SpaUsuario> results = (List<SpaUsuario>) query.executeWithArray(email);
			if(results.size() >0 && results.get(0) != null) {
				return results.get(0);
			}
			else return null;
		}catch(Exception e){
			return null;
		}
	}

	@Override
	@Transactional
	public void create(SpaUsuario usuario) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		getPersistenceManager().makePersistent(usuario);
	}

	@Override
	@Transactional
	public void remove(SpaUsuario usuario) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		getPersistenceManager().deletePersistent(getPersistenceManager().getObjectById(usuario.getClass(),usuario.getEmail()));
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<SpaUsuario> findUsersByRole(Roles rol) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(SpaUsuario.class);
			query.setFilter("authorities == authoritiesParam");
			query.declareParameters("String authoritiesParam");
			List<SpaUsuario> results = (List<SpaUsuario>) query.executeWithArray(rol.getAuthority());
			return results;
		}catch(Exception e){
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<SpaUsuario> findAll() {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(SpaUsuario.class);
			List<SpaUsuario> results = (List<SpaUsuario>) query.execute();
			return results;
		}catch(Exception e){
			return null;
		}
	}
	
	@Override
	@Transactional
	public void update(SpaUsuario newUsuario) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		SpaUsuario usuario = getPersistenceManager().getObjectById(com.azeta3.spa.bom.SpaUsuario.class,newUsuario.getEmail());
		
		usuario.setApellidos(newUsuario.getApellidos());
		usuario.setAuthorities(newUsuario.getAuthorities());
		usuario.setEmail(newUsuario.getEmail());
		usuario.setIdAnuncios(newUsuario.getIdAnuncios());
		usuario.setNombre(newUsuario.getNombre());
		usuario.setEnabled(newUsuario.isEnabled());
		usuario.setPassword(newUsuario.getPassword());
	}
	
}
