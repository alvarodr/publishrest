package com.azeta3.spa.dao.impl;

import java.util.List;

import javax.jdo.FetchPlan;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jdo.support.JdoDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.azeta3.spa.bom.SpaLocalidad;
import com.azeta3.spa.dao.LocalidadDAO;

@Repository
public class LocalidadDaoImpl extends JdoDaoSupport implements LocalidadDAO {

	@Autowired
    public LocalidadDaoImpl(PersistenceManagerFactory pmf)
    {
		setPersistenceManagerFactory(pmf);
    }
	
	@Override
	@Transactional(readOnly = true)
	public SpaLocalidad findLocalidadById(Long id) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			return (SpaLocalidad) getPersistenceManager().getObjectById(id);
		}catch(Exception e){
			return null;
		}
	}

	@Override
	@Transactional
	public void createLocalidad(SpaLocalidad localidad) {
		getPersistenceManager().makePersistent(localidad);	
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public void deleteAllLocalidades() {
		Query query = getPersistenceManager().newQuery(SpaLocalidad.class);
		List<SpaLocalidad> localidades = (List<SpaLocalidad>) query.execute();
		query.closeAll();
		getPersistenceManager().deletePersistentAll(localidades);	
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public boolean isEmpty() {
		Query query = null;
		List<SpaLocalidad> localidades = null;
		query = getPersistenceManager().newQuery(SpaLocalidad.class);
		localidades = (List<SpaLocalidad>) query.execute();
		return localidades.isEmpty();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<SpaLocalidad> getAllLocalidades() {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(SpaLocalidad.class);
			return (List<SpaLocalidad>) query.execute();
		}catch(Exception e){
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<SpaLocalidad> getLocalidadByProvincia(Long idProvincia) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(SpaLocalidad.class);
			query.setFilter("idProvincia == idProvinciaParam");
			query.declareParameters("Long idProvinciaParam");
			
			List<SpaLocalidad> localidades = (List<SpaLocalidad>) query.executeWithArray(idProvincia);
			
			return localidades;
		}catch(Exception e){
			return null;
		}
	}

}