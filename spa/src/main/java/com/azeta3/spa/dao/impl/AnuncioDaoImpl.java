package com.azeta3.spa.dao.impl;

import java.util.List;

import javax.jdo.FetchPlan;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import org.apache.log4j.Logger;
import org.datanucleus.exceptions.NucleusObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jdo.support.JdoDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.azeta3.spa.bom.SpaAnuncio;
import com.azeta3.spa.dao.AnuncioDAO;

/**
 * 
 * @author ADORU3N
 *
 */
@Repository
public class AnuncioDaoImpl extends JdoDaoSupport implements AnuncioDAO {

	private Logger log = Logger.getLogger(AnuncioDaoImpl.class);

	@Autowired
    public AnuncioDaoImpl(PersistenceManagerFactory pmf)
    {
		setPersistenceManagerFactory(pmf);
    }
	
	/*
	 * (non-Javadoc)
	 * @see com.azeta3.spa.dao.AnuncioDAO#creaAnuncio(com.azeta3.spa.bom.SpaAnuncio)
	 */
	@Override
	@Transactional
	public SpaAnuncio creaAnuncio(SpaAnuncio anuncio) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			return (SpaAnuncio) getPersistenceManager().makePersistent(anuncio);
		}catch(Exception e){
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.azeta3.spa.dao.AnuncioDAO#eliminaAnuncio(com.azeta3.spa.bom.SpaAnuncio)
	 */
	@Override
	@Transactional
	public void eliminaAnuncio(SpaAnuncio anuncio) {
		Long idAnuncio = anuncio.getAnunId();
		anuncio = null;
		getPersistenceManager().deletePersistent(getPersistenceManager().getObjectById(SpaAnuncio.class,idAnuncio));
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.azeta3.spa.dao.AnuncioDAO#findAnuncioById(java.lang.Long)
	 */
	@Override
	@Transactional(readOnly = true)
	public SpaAnuncio findAnuncioById(Long id) {
		getPersistenceManager().getFetchPlan().setGroup(FetchPlan.ALL);
		try{
			SpaAnuncio anuncio = 
					getPersistenceManager().detachCopy(getPersistenceManager().getObjectById(com.azeta3.spa.bom.SpaAnuncio.class, id));
			return anuncio;
		}catch(Exception e){
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.azeta3.spa.dao.AnuncioDAO#findAllAnuncios()
	 */
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<SpaAnuncio> findAllAnuncios() {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(com.azeta3.spa.bom.SpaAnuncio.class);
			List<SpaAnuncio> anuncios = (List<SpaAnuncio>) query.execute();
			getPersistenceManager().detachCopyAll(anuncios);
			
			return anuncios;
		}catch(NucleusObjectNotFoundException e){
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.azeta3.spa.dao.AnuncioDAO#findByPadre(java.lang.String, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<SpaAnuncio> findByPadre(String id, String usuarioEmail) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(com.azeta3.spa.bom.SpaAnuncio.class);
			query.setFilter("nodoPadre == idPadre && idUsuarioEmail == idUsuarioEmailParam");
			query.declareParameters("java.lang.Long idPadre, java.lang.String idUsuarioEmailParam");
			List<SpaAnuncio> anuncios = (List<SpaAnuncio>) query.executeWithArray(Long.parseLong(id), usuarioEmail);
			getPersistenceManager().detachCopyAll(anuncios);
			return anuncios;
		}catch(Exception e){
			return null;
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.azeta3.spa.dao.AnuncioDAO#findByUserEmail(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<SpaAnuncio> findByUserEmail(String email) {
		log.debug("### se consultan los anuncios de " + email);
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(SpaAnuncio.class);
			query.setFilter("idUsuarioEmail == idUsuarioEmailParam");
			query.declareParameters("String idUsuarioEmailParam");
			List<SpaAnuncio> anuncios =  (List<SpaAnuncio>) query.executeWithArray(email);
			getPersistenceManager().detachCopyAll(anuncios);
			return anuncios;
		}catch(Exception e){
			log.error(e.getStackTrace());
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see com.azeta3.spa.dao.AnuncioDAO#updateAnuncio(com.azeta3.spa.bom.SpaAnuncio)
	 */
	@Override
	@Transactional
	public void updateAnuncio(SpaAnuncio anuncio) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		SpaAnuncio a = (SpaAnuncio)getPersistenceManager().getObjectById(anuncio.getClass(),anuncio.getAnunId());
		a.setBarrio(anuncio.getBarrio());
		a.setDescripcionHTML(anuncio.getDescripcionHTML());
		a.setEmail(anuncio.getEmail());
		a.setIdPublicacionesProgramadas(anuncio.getIdPublicacionesProgramadas());
		a.setIdUsuarioEmail(anuncio.getIdUsuarioEmail());
		a.setIntervalo(anuncio.getIntervalo());
		a.setPoblacion(anuncio.getPoblacion());
		a.setTitulo(anuncio.getTitulo());
		a.setIdsImagenes(anuncio.getIdsImagenes());
		a.setIdCategoria(anuncio.getIdCategoria());
		a.setIdProvincia(anuncio.getIdProvincia());
		a.setTituloMundoanuncio(anuncio.getTituloMundoanuncio());
	}

}
