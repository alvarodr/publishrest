package com.azeta3.spa.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.azeta3.spa.bom.SpaUsuario;
import com.azeta3.spa.security.Roles;

/**
 * 
 * @author ADORU3N
 *
 */
@Repository
public interface UsuarioDAO {
	
	/**
	 * 
	 * @param email
	 * @return
	 */
	SpaUsuario findById(String email);

	/**
	 * 
	 * @param usuario
	 */
	void create(SpaUsuario usuario);

	/**
	 * 
	 * @param usuario
	 */
	void remove(SpaUsuario usuario);

	/**
	 * 
	 * @param rol
	 * @return
	 */
	List<SpaUsuario> findUsersByRole(Roles rol);

	/**
	 * 
	 * @return
	 */
	List<SpaUsuario> findAll();

	/**
	 * 
	 * @param newUsuario
	 */
	void update(SpaUsuario newUsuario);
}
