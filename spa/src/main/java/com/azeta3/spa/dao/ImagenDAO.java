package com.azeta3.spa.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.azeta3.spa.bom.SpaImagen;

/**
 * 
 * @author ADORU3N
 *
 */
@Repository
public interface ImagenDAO {
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	SpaImagen findById(Long id);

	/**
	 * 
	 * @param imagen
	 * @return
	 */
	SpaImagen create(SpaImagen imagen);

	/**
	 * 
	 * @param idImagen
	 */
	void delete(Long idImagen);

	/**
	 * 
	 * @param imagen
	 */
	void update(SpaImagen imagen);

	/**
	 * 
	 * @param idAnuncio
	 * @return
	 */
	List<SpaImagen> findByIdAnuncio(Long idAnuncio);
}
