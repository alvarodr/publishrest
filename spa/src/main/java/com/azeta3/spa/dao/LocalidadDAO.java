package com.azeta3.spa.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.azeta3.spa.bom.SpaLocalidad;

/**
 * 
 * @author ADORU3N
 *
 */
@Repository
public interface LocalidadDAO {
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	SpaLocalidad findLocalidadById(Long id);

	/**
	 * 
	 * @param localidad
	 */
	void createLocalidad(SpaLocalidad localidad);

	/**
	 * Elimina todas las localidades.
	 */
	void deleteAllLocalidades();

	/**
	 * 
	 * @return
	 */
	boolean isEmpty();

	/**
	 * 
	 * @return
	 */
	List<SpaLocalidad> getAllLocalidades();

	/**
	 * 
	 * @param idProvincia
	 * @return
	 */
	List<SpaLocalidad> getLocalidadByProvincia(Long idProvincia);
}
