package com.azeta3.spa.dao.impl;

import java.util.List;

import javax.jdo.FetchPlan;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jdo.support.JdoDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.azeta3.spa.bom.SpaImagen;
import com.azeta3.spa.bom.SpaUsuario;
import com.azeta3.spa.dao.ImagenDAO;

@Repository
public class ImagenDaoImpl extends JdoDaoSupport implements ImagenDAO {

	@Autowired
    public ImagenDaoImpl(PersistenceManagerFactory pmf)
    {
		setPersistenceManagerFactory(pmf);
    }
	
	@Override
	@Transactional(readOnly = true)
	public SpaImagen findById(Long id) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			return (SpaImagen) getPersistenceManager().getObjectById(SpaImagen.class,id);
		}catch(Exception e){
			return null;
		}
	}

	@Override
	@Transactional
	public SpaImagen create(SpaImagen imagen) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			return (SpaImagen) getPersistenceManager().makePersistent(imagen);
		}catch(Exception e){
			return null;
		}
	}

	@Override
	@Transactional
	public void delete(Long idImagen) {
		getPersistenceManager().deletePersistent(getPersistenceManager().getObjectById(SpaImagen.class,idImagen));
	}

	@Override
	@Transactional
	public void update(SpaImagen imagen) {
		SpaImagen imagenFound = getPersistenceManager().getObjectById(SpaImagen.class,imagen.getId());
		imagenFound.setIdAnuncio(imagen.getIdAnuncio());
		imagenFound.setImagen(imagen.getImagen());
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<SpaImagen> findByIdAnuncio(Long idAnuncio) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(SpaUsuario.class);
			query.setFilter("idAnuncio == idAnuncioParam");
			query.declareParameters("java.lang.Long idAnuncioParam");
			return (List<SpaImagen>) query.execute(idAnuncio);
		}catch(Exception e){
			return null;
		}
	}
}