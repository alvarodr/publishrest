package com.azeta3.spa.dao.impl;

import java.util.Calendar;
import java.util.List;

import javax.jdo.FetchPlan;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jdo.support.JdoDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.azeta3.spa.bom.SpaPublicacionProgramada;
import com.azeta3.spa.constantes.DiasSemanaEnum;
import com.azeta3.spa.dao.PublicacionProgramadaDAO;

@Repository
public class PublicacionProgramadaDaoImpl extends JdoDaoSupport implements PublicacionProgramadaDAO {
	
//	private Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
    public PublicacionProgramadaDaoImpl(PersistenceManagerFactory pmf)
    {
		setPersistenceManagerFactory(pmf);
    }

	@Override
	@Transactional(readOnly = true)
	public SpaPublicacionProgramada findById(Long id) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		/*try{*/
			SpaPublicacionProgramada p = getPersistenceManager().getObjectById(SpaPublicacionProgramada.class,id);
			return p;
		/*}catch(Exception e){
			return null;
		}*/
	}

	@Override
	@Transactional(readOnly = true)
	public List<SpaPublicacionProgramada> findByDayOfWeek(DiasSemanaEnum DayOfWeek) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(SpaPublicacionProgramada.class);
			query.setFilter("diaSemana == diaSemanaParam");
			query.declareParameters("String diaSemanaParam");
				
			@SuppressWarnings("unchecked")
			List<SpaPublicacionProgramada> results = (List<SpaPublicacionProgramada>) query.executeWithArray(DayOfWeek.getDia());
			results = (List<SpaPublicacionProgramada>) getPersistenceManager().detachCopyAll(results);
			return results;
		}catch(Exception e){
			return null;
		}
	}

	@Override
	@Transactional
	public SpaPublicacionProgramada createPublicacionProgramada(
			SpaPublicacionProgramada publicacion) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			SpaPublicacionProgramada result =  getPersistenceManager().makePersistent(publicacion);
			result = getPersistenceManager().detachCopy(result);
			return result;
		}catch(Exception e){
			return null;
		}
	}

	@Override
	@Transactional
	public void remove(Long idPublicacion) {
		getPersistenceManager().deletePersistent(getPersistenceManager().getObjectById(SpaPublicacionProgramada.class,idPublicacion));	
	}
	
	@Override
	@Transactional
	public List<SpaPublicacionProgramada> findPublicacionesExecuteNow(
			Calendar now, String idUsuario) {
		
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(SpaPublicacionProgramada.class);
			
			query.setFilter("horaMindesde <= nowParam <= horaMinHasta" +
					"&& idUsuario == idUsuarioParam " +
					"&& diaSemana == diaSemanaParam");
			query.declareParameters(
					"java.lang.String diaSemanaParam, " +
					"java.util.Date nowParam, " +
					"java.lang.String idUsuarioParam");
			
			@SuppressWarnings("unchecked")
			List<SpaPublicacionProgramada> results = 
				(List<SpaPublicacionProgramada>) query.executeWithArray(
						DiasSemanaEnum.getDiaSemanaDeCalendar(now).getDia(),
						now.getTime(),
						idUsuario);
			return results;
		}catch(Exception e){
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<SpaPublicacionProgramada> findAll() {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(SpaPublicacionProgramada.class);
			return (List<SpaPublicacionProgramada>)query.execute();
		}catch(Exception e){
			return null;
		}
	}

}
