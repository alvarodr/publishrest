package com.azeta3.spa.dao;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.azeta3.spa.bom.SpaPublicacionProgramada;
import com.azeta3.spa.constantes.DiasSemanaEnum;

/**
 * 
 * @author ADORU3N
 *
 */
@Repository
public interface PublicacionProgramadaDAO {
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	SpaPublicacionProgramada findById(Long id);

	/**
	 * 
	 * @param DayOfWeek
	 * @return
	 */
	List<SpaPublicacionProgramada> findByDayOfWeek(DiasSemanaEnum DayOfWeek);

	/**
	 * 
	 * @param publicacion
	 * @return
	 */
	SpaPublicacionProgramada createPublicacionProgramada(SpaPublicacionProgramada publicacion);

	/**
	 * 
	 * @param idPublicacion
	 */
	void remove(Long idPublicacion);

	/**
	 * 
	 * @param now
	 * @param idUsuario
	 * @return
	 */
	List<SpaPublicacionProgramada> findPublicacionesExecuteNow(Calendar now, String idUsuario);

	/**
	 * 
	 * @return
	 */
	List<SpaPublicacionProgramada> findAll();
}
