package com.azeta3.spa.dao.impl;

import java.util.List;

import javax.jdo.FetchPlan;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jdo.support.JdoDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.azeta3.spa.bom.SpaProvincia;
import com.azeta3.spa.dao.ProvinciaDAO;

@Repository
public class ProvinciaDaoImpl extends JdoDaoSupport implements ProvinciaDAO {
	
	@Autowired
    public ProvinciaDaoImpl(PersistenceManagerFactory pmf)
    {
		setPersistenceManagerFactory(pmf);
    }
	
	@Override
	@Transactional(readOnly = true)
	public SpaProvincia findProvById(Long id) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			return (SpaProvincia) getPersistenceManager().getObjectById(id);
		}catch(Exception e){
			return null;
		}
	}

	@Override
	@Transactional
	public void createProvincia(SpaProvincia prov) {
		getPersistenceManager().makePersistent(prov);		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public void deleteAllProvincias() {
		Query query = getPersistenceManager().newQuery(SpaProvincia.class);
		List<SpaProvincia> provincias = (List<SpaProvincia>) query.execute();
		getPersistenceManager().deletePersistentAll(provincias);		
	}

	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<SpaProvincia> getAllProvincias(){
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(SpaProvincia.class);
			return (List<SpaProvincia>) query.execute();
		}catch(Exception e){
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public boolean isEmpty() {
		Query query = getPersistenceManager().newQuery(SpaProvincia.class);
		List<SpaProvincia> provincias = (List<SpaProvincia>) query.execute("");

		return provincias.isEmpty();
	}
}