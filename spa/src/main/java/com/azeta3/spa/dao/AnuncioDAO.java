package com.azeta3.spa.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.azeta3.spa.bom.SpaAnuncio;

/**
 * 
 * @author ADORU3N
 *
 */
@Repository
public interface AnuncioDAO {

	/**
	 * 
	 * @param anuncio
	 * @return
	 */
	SpaAnuncio creaAnuncio(SpaAnuncio anuncio);

	/**
	 * 
	 * @param anuncios
	 */
	void eliminaAnuncio(SpaAnuncio anuncios);

	/**
	 * 
	 * @param id
	 * @return
	 */
	SpaAnuncio findAnuncioById(Long id);

	/**
	 * 
	 * @return
	 */
	List<SpaAnuncio> findAllAnuncios();

	/**
	 * 
	 * @param id
	 * @param usuarioEmail
	 * @return
	 */
	List<SpaAnuncio> findByPadre(String id, String usuarioEmail);

	/**
	 * 
	 * @param email
	 * @return
	 */
	List<SpaAnuncio> findByUserEmail(String email);

	/**
	 * 
	 * @param anuncio
	 */
	void updateAnuncio(SpaAnuncio anuncio);
}
