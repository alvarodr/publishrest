package com.azeta3.spa.dao.impl;

import java.util.List;

import javax.jdo.FetchPlan;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jdo.support.JdoDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.azeta3.spa.bom.SpaPublicacionEfectiva;
import com.azeta3.spa.dao.PublicacionEfectivaDAO;

public class PublicacionEfectivaDaoImpl extends JdoDaoSupport implements PublicacionEfectivaDAO{

	private Logger log = Logger.getLogger(this.getClass());
	@Autowired
    public PublicacionEfectivaDaoImpl(PersistenceManagerFactory pmf)
    {
		setPersistenceManagerFactory(pmf);
    }
	
	@Override
	@Transactional
	public SpaPublicacionEfectiva create(SpaPublicacionEfectiva publicacion) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			return getPersistenceManager().makePersistent(publicacion);
		}catch(Exception e){
			return null;
		}
	}

	@Override
	@Transactional
	public SpaPublicacionEfectiva update(SpaPublicacionEfectiva publicacion) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			SpaPublicacionEfectiva publicacionFound = getPersistenceManager().getObjectById(
					com.azeta3.spa.bom.SpaPublicacionEfectiva.class,
					publicacion.getId());
			publicacionFound.setFechaPublicacion(publicacion.getFechaPublicacion());
			publicacionFound.setIdAnuncio(publicacion.getIdAnuncio());
			publicacionFound.setIdPublicacionProgramada(publicacion.getIdPublicacionProgramada());
			publicacionFound.setEstado(publicacion.getEstado());
			return publicacionFound;
		}catch(Exception e){
			return null;
		}
	}

	@Override
	@Transactional
	public void remove(Long idSpaPublicacionEfectiva) {
		getPersistenceManager().deletePersistent(getPersistenceManager().getObjectById(SpaPublicacionEfectiva.class,idSpaPublicacionEfectiva));
		
	}

	@Override
	@Transactional(readOnly = true)
	public SpaPublicacionEfectiva findById(Long id) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			return getPersistenceManager().getObjectById(com.azeta3.spa.bom.SpaPublicacionEfectiva.class, id);
		}catch(Exception e){
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<SpaPublicacionEfectiva> findByAnuncio(Long idAnuncio) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(com.azeta3.spa.bom.SpaPublicacionEfectiva.class);
			query.setFilter("idAnuncio == idAnuncioParam");
			query.setRange(0, 10);
			query.declareParameters("java.lang.Long idAnuncioParam");
			return (List<SpaPublicacionEfectiva>) query.executeWithArray(idAnuncio);
		}catch(Exception e){
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<SpaPublicacionEfectiva> findByAnuncio(String usuario,
			Integer start, Integer limit) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(com.azeta3.spa.bom.SpaPublicacionEfectiva.class);
			query.setFilter("idUsuario == idUsuarioParam");
			query.setRange(start, start + limit);
			query.declareParameters("java.lang.String idUsuarioParam");
			return (List<SpaPublicacionEfectiva>) query.executeWithArray(usuario);
		}catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public Integer getTotalByUsuario(String usuario,
			Integer start, Integer limit) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(com.azeta3.spa.bom.SpaPublicacionEfectiva.class);
			query.setFilter("idUsuario == idUsuarioParam");
			//query.setRange(start, limit);
			query.declareParameters("java.lang.String idUsuarioParam");
			return ((List<SpaPublicacionEfectiva>) query.executeWithArray(usuario)).size();
		}catch (Exception e) {
			// TODO: handle exception
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public SpaPublicacionEfectiva findMasAntigua(Long idAnuncio) {
		try{
			Query query = getPersistenceManager().newQuery(
					"SELECT FROM " + SpaPublicacionEfectiva.class.getName()  +
					" WHERE idAnuncio == " + idAnuncio.toString() + 
					" && fechaPublicacion == (SELECT MIN(fechaPublicacion) FROM " + 
					SpaPublicacionEfectiva.class.getName() + ")");
			//query.setRange(1, 1);
			return ((List<SpaPublicacionEfectiva>) query.execute()).get(0);
		}catch(Exception e){
			log.error(e.getMessage());
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<SpaPublicacionEfectiva> findByPublicacionProgramada(
			Long idPublicacionProgramada) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		/*try{
			Query query = getPersistenceManager().newQuery(com.azeta3.spa.bom.SpaPublicacionEfectiva.class);
			query.setFilter("idPublicacionProgramada == idPublicacionProgramadaParam");
			query.declareParameters("java.lang.Long idPublicacionProgramadaParam");
			return (List<SpaPublicacionEfectiva>) query.executeWithArray(idPublicacionProgramada);
		}catch(Exception e){
			log.error(e.getMessage());
			return null;
		}*/
		try{
			Query query = getPersistenceManager().newQuery(
					"SELECT FROM " + SpaPublicacionEfectiva.class.getName()  +
					" WHERE idPublicacionProgramada == " + idPublicacionProgramada.toString());
			return ((List<SpaPublicacionEfectiva>) query.execute());
			
		}catch(Exception e){
			log.error(e.getMessage());
			return null;
		}
	}
}
