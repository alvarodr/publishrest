package com.azeta3.spa.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.azeta3.spa.bom.SpaProxy;

/**
 * 
 * @author ADORU3N
 *
 */
@Repository
public interface ProxyDAO {

	/**
	 * 
	 * @return
	 */
	List<SpaProxy> findAll();

	/**
	 * 
	 * @param proxy
	 * @return
	 */
	SpaProxy create(SpaProxy proxy);

	/**
	 * 
	 * @param id
	 */
	void delete(Long id);

	/**
	 * 
	 * @param proxy
	 */
	void update(SpaProxy proxy);

	/**
	 * 
	 * @param id
	 * @return
	 */
	SpaProxy findById(Long id);
}
