package com.azeta3.spa.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.azeta3.spa.bom.SpaCategoria;

/**
 * 
 * @author ADORU3N
 *
 */
@Repository
public interface CategoriaDAO {
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	SpaCategoria findCategoriaById(Long id);

	/**
	 * 
	 * @param categoria
	 * @return
	 */
	SpaCategoria createCategoria(SpaCategoria categoria);

	/**
	 * Elimina todas las categorias
	 */
	void deleteAllCategorias();

	/**
	 * 
	 * @return
	 */
	boolean isEmpty();

	/**
	 * 
	 * @return
	 */
	List<SpaCategoria> getAllCategorias();
}
