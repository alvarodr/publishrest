package com.azeta3.spa.dao.impl;

import java.util.List;

import javax.jdo.FetchPlan;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.jdo.support.JdoDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.azeta3.spa.bom.SpaCategoria;
import com.azeta3.spa.dao.CategoriaDAO;

@Repository
public class CategoriaDaoImpl extends JdoDaoSupport implements CategoriaDAO {
	
	@Autowired
    public CategoriaDaoImpl(PersistenceManagerFactory pmf)
    {
		setPersistenceManagerFactory(pmf);
    }
	
	@Override
	@Transactional(readOnly = true)
	public SpaCategoria findCategoriaById(Long id) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			return getPersistenceManager().getObjectById(com.azeta3.spa.bom.SpaCategoria.class,id);
		}catch(Exception e){
			return null;
		}
	}

	@Override
	@Transactional
	public SpaCategoria createCategoria(SpaCategoria categoria) {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			return (SpaCategoria)getPersistenceManager().makePersistent(categoria);				
		}catch(Exception e){
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public void deleteAllCategorias() {
		Query query = getPersistenceManager().newQuery(com.azeta3.spa.bom.SpaCategoria.class);
		List<SpaCategoria> categorias = (List<SpaCategoria>) query.execute();
		getPersistenceManager().deletePersistentAll(categorias);		
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public boolean isEmpty() {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);

		Query query = getPersistenceManager().newQuery(com.azeta3.spa.bom.SpaCategoria.class);
		List<SpaCategoria> categorias = (List<SpaCategoria>) query.execute();
		
		return categorias.isEmpty();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly = true)
	public List<SpaCategoria> getAllCategorias() {
		getPersistenceManager().getFetchPlan().addGroup(FetchPlan.ALL);
		try{
			Query query = getPersistenceManager().newQuery(com.azeta3.spa.bom.SpaCategoria.class);
			return (List<SpaCategoria>) query.execute();
		}catch(Exception e){
			return null;
		}
	}
	

}