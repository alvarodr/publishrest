package com.azeta3.spa.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.azeta3.spa.bom.SpaProvincia;

/**
 * 
 * @author ADORU3N
 *
 */
@Repository
public interface ProvinciaDAO {
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	SpaProvincia findProvById(Long id);

	/**
	 * 
	 * @param prov
	 */
	void createProvincia(SpaProvincia prov);

	/**
	 * 
	 */
	void deleteAllProvincias();

	/**
	 * 
	 * @return
	 */
	boolean isEmpty();

	/**
	 * 
	 * @return
	 */
	List<SpaProvincia> getAllProvincias();
}
