package com.azeta3.spa.service.impl;

import java.util.StringTokenizer;

import org.fluttercode.datafactory.impl.DataFactory;

import com.azeta3.spa.service.TextService;

public class TextServiceImpl implements TextService{

	@Override
	public String Randomize(String texto) {
		DataFactory df = new DataFactory();
		String result = new String();
		Integer numModificaciones = 0;
		
		while(numModificaciones < 3){
			result = "";
			StringTokenizer tokenizer = new StringTokenizer(texto, " ");
			while(tokenizer.hasMoreTokens()){
				
				result += " " + tokenizer.nextToken();
				
				Integer probabilidad = df.getNumberBetween(0, 17);
				
				if(probabilidad == 1){
					numModificaciones++;
					Integer contenidoAleatorio = df.getNumberBetween(0, 5);
					result += " [";
					
					if(contenidoAleatorio == 0){
						result += df.getFirstName();
					}
					if(contenidoAleatorio == 1){
						result += df.getLastName();
					}
					if(contenidoAleatorio == 2){
						result += df.getCity();
					}
					if(contenidoAleatorio == 3){
						result += df.getBusinessName();
					}
					if(contenidoAleatorio == 4){
						result += df.getRandomWord();
					}
					if(contenidoAleatorio == 5){
						result += df.getStreetName();
					}
					
					result += "]";
				}
			}
		}
		return result;
	}


}
