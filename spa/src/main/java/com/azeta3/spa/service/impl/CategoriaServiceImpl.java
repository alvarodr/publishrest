package com.azeta3.spa.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.azeta3.spa.bom.SpaCategoria;
import com.azeta3.spa.dao.CategoriaDAO;
import com.azeta3.spa.dto.SpaCombo;
import com.azeta3.spa.service.CategoriaService;

public class CategoriaServiceImpl implements CategoriaService {

	@Autowired
	private CategoriaDAO categoriaDao;
	
	@Override
	public List<SpaCombo> getCategorias(String query) {
		// TODO Auto-generated method stub
		List<SpaCategoria> categorias = categoriaDao.getAllCategorias();
		List<SpaCombo> listCombo = new ArrayList<SpaCombo>();
		for (SpaCategoria categoria : categorias){
			SpaCombo combo = new SpaCombo();
			if (query!="" && query!=null){
				if (categoria.getNombreCategoria().toLowerCase().startsWith(query.toLowerCase())){
					combo.setValoId(categoria.getId());
					combo.setValoNombre(categoria.getNombreCategoria());
					listCombo.add(combo);
				}				
			}else{
				combo.setValoId(categoria.getId());
				combo.setValoNombre(categoria.getNombreCategoria());
				listCombo.add(combo);	
			}
		}
		return listCombo;
	}

	public CategoriaDAO getCategoriaDao() {
		return categoriaDao;
	}

	public void setCategoriaDao(CategoriaDAO categoriaDao) {
		this.categoriaDao = categoriaDao;
	}
}
