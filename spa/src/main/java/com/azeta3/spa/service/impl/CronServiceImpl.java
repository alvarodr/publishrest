package com.azeta3.spa.service.impl;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.Scheduled;

import com.azeta3.spa.bom.SpaAnuncio;
import com.azeta3.spa.bom.SpaPublicacionEfectiva;
import com.azeta3.spa.service.AgendaEfectivaService;
import com.azeta3.spa.service.AnuncioService;
import com.azeta3.spa.service.CronService;
import com.azeta3.spa.service.PublicaService;
import com.azeta3.spa.service.PublicacionEfectivaService;
import com.azeta3.spa.tasks.CrearPublicacionTask;

public class CronServiceImpl implements CronService{

	@Autowired
	private AgendaEfectivaService agendaEfectivaService;
	
	@Autowired
	private PublicacionEfectivaService publicacionEfectivaService;
	
	@Autowired
	private TaskExecutor spaExecutor;
	
	@Autowired
	private AnuncioService anuncioService;
	
	@Autowired
	private PublicaService publicaService;
	
	Logger log = Logger.getLogger(this.getClass());
	
	@Override
	@Scheduled(cron="0 0/1 * ? * *")
	public void publicacion() throws Exception {
		Calendar now = Calendar.getInstance();
		now.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
		
		Calendar expireDate = new GregorianCalendar();
		expireDate.set(Calendar.DATE, 17);
		expireDate.set(Calendar.MONTH, 2);
		expireDate.set(Calendar.YEAR,2012);
		
		if(now.compareTo(expireDate) < 0){
		
			/**
			 * Recupero los anuncios que se deben publicar en este minuto
			 */
			
			List<SpaPublicacionEfectiva> publicaciones = agendaEfectivaService.generarAgenda();
			
			for(SpaPublicacionEfectiva publicacion : publicaciones){
				Calendar horaPublicacion = dateToCalendar(publicacion.getFechaPublicacion());
				horaPublicacion.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
				
				if(horaPublicacion.get(Calendar.DAY_OF_WEEK) == now.get(Calendar.DAY_OF_WEEK)
				&& horaPublicacion.get(Calendar.HOUR_OF_DAY) == now.get(Calendar.HOUR_OF_DAY)
				&& horaPublicacion.get(Calendar.MINUTE) == now.get(Calendar.MINUTE)){
	
					/**
					 * A�ado una tarea de publicacion por anuncio
					 */
					
					SpaAnuncio anuncio = anuncioService.findAnuncioById(publicacion.getIdAnuncio());
					
					CrearPublicacionTask task = new CrearPublicacionTask(publicaService, publicacionEfectivaService);
					task.setAnuncioPublicar(anuncio);
					task.setPublicacionEfectiva(publicacion);
					
					spaExecutor.execute(task);
				}  
			}
			
		}//Cierre if licencia
	}
	
	private Calendar dateToCalendar(Date date){
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		return gc;
	}

	@Override
	@Scheduled(cron="0 0/1 * ? * *")
	public void borrarImagenes() throws Exception {
		ClassPathResource r = new ClassPathResource("images/parent/file");
		File folder = new File(r.getFile().getParent());
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmm");
		Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
		
		String now = sdf.format(c.getTime());
		
		String[] children = new File(folder.getParent()).list();
		for(String file : children){
			
			StringTokenizer tkn = new StringTokenizer(file,"_");
			String fechaCreacionFichero = tkn.nextToken();
			
			if(fechaCreacionFichero.length() > 6 
					&& (Long.parseLong(now) - Long.parseLong(fechaCreacionFichero.substring(6))) > 5 ){
				
				File borrar = new File(r.getFile().getParent() + file.substring(6));
				borrar.delete();
				
			}
		}
		
	}

}
