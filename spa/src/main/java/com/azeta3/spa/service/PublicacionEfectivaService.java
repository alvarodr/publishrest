package com.azeta3.spa.service;

import java.util.List;

import com.azeta3.spa.bom.SpaPublicacionEfectiva;

public interface PublicacionEfectivaService {
	
	public SpaPublicacionEfectiva create(SpaPublicacionEfectiva publicacion);
	public SpaPublicacionEfectiva findById(Long id);
	public List<SpaPublicacionEfectiva> findByAnuncio(Long idAnuncio);
	public List<SpaPublicacionEfectiva> findByAnuncio(String usuario, Integer start, Integer limit);
	public Integer getTotalLogByUsua(String usuario, Integer start, Integer limit);
	public SpaPublicacionEfectiva findMasAntigua(Long idAnuncio);
	public void remove(Long idSpaPublicacionEfectiva);
	public Integer getTotalByUsuario(String usuario, Integer start, Integer limit);
}
