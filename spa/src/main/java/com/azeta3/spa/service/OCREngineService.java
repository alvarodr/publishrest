package com.azeta3.spa.service;

import java.io.IOException;

public interface OCREngineService {

	/**
	 * 
	 * @param imagen
	 * @return
	 * @throws IOException
	 */
	String ocr(byte[] imagen) throws IOException;
}
