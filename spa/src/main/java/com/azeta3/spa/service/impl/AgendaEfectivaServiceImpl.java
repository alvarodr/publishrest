package com.azeta3.spa.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.azeta3.spa.bom.SpaAnuncio;
import com.azeta3.spa.bom.SpaPublicacionEfectiva;
import com.azeta3.spa.bom.SpaPublicacionProgramada;
import com.azeta3.spa.constantes.DiasSemanaEnum;
import com.azeta3.spa.dao.AnuncioDAO;
import com.azeta3.spa.dao.PublicacionProgramadaDAO;
import com.azeta3.spa.service.AgendaEfectivaService;

@Service
public class AgendaEfectivaServiceImpl implements AgendaEfectivaService{

	@Autowired
	private PublicacionProgramadaDAO publicacionProgramadaDao;
	@Autowired
	private AnuncioDAO anuncioDao;
	
	public AgendaEfectivaServiceImpl(PublicacionProgramadaDAO publicacionProgramadaDao,
			AnuncioDAO anuncioDao){
		this.publicacionProgramadaDao = publicacionProgramadaDao;
		this.anuncioDao = anuncioDao;
	}
	
	public AgendaEfectivaServiceImpl(){}
	
	private Logger log = Logger.getLogger(this.getClass());
	
	//Mapa que guarda el minuto final de cada hora en que se realiz� una publicacion efectiva
	private Map<SpaAnuncio, Integer> minutoFinalUltimaHora = new HashMap<SpaAnuncio, Integer>();
		
	public List<SpaPublicacionEfectiva> generarAgenda(){
		
		//Obtengo las publicaciones programadas por anuncio
		Map<Long,List<SpaPublicacionProgramada>> publicacionesProgramadasPorAnuncio = new HashMap<Long,List<SpaPublicacionProgramada>>();
		List<SpaAnuncio> anuncios = anuncioDao.findAllAnuncios();
		
		for(SpaAnuncio anuncio : anuncios){
			List<SpaPublicacionProgramada> pubsPorAnuncio = new ArrayList<SpaPublicacionProgramada>();
			if(anuncio.getIdPublicacionesProgramadas() != null){
				for(Long idPublicacionProgramada : anuncio.getIdPublicacionesProgramadas()){
					SpaPublicacionProgramada p = publicacionProgramadaDao.findById(idPublicacionProgramada);
					pubsPorAnuncio.add(p);
				}
				publicacionesProgramadasPorAnuncio.put(anuncio.getAnunId(), pubsPorAnuncio);
			}
		}
		
		return generarDias(publicacionesProgramadasPorAnuncio, anuncios);
	}
	
	private List<SpaPublicacionEfectiva> generarDias(
			Map<Long,List<SpaPublicacionProgramada>> publicacionesProgramadasPorAnuncio,
			List<SpaAnuncio> anuncios){
		
		List<SpaPublicacionEfectiva> result = new ArrayList<SpaPublicacionEfectiva>();
		
		for(DiasSemanaEnum diaSemana : DiasSemanaEnum.values()){
			result.addAll(generarHorasPorDia(diaSemana, publicacionesProgramadasPorAnuncio, anuncios));
		}
		
		return result;
	}
	
	private List<SpaPublicacionEfectiva> generarHorasPorDia(DiasSemanaEnum diaSemana,
			Map<Long,List<SpaPublicacionProgramada>> publicacionesProgramadasPorAnuncio,
			List<SpaAnuncio> anuncios){
		
		List<SpaPublicacionEfectiva> result = new ArrayList<SpaPublicacionEfectiva>();
				
		Calendar hcalendar = new GregorianCalendar();
		hcalendar.set(Calendar.HOUR_OF_DAY, 00);
		hcalendar.set(Calendar.MINUTE, 00);
		
		for(Calendar hora = dateToCalendar(hcalendar.getTime());
		hora.get(Calendar.HOUR_OF_DAY)<=22;
		hora.add(Calendar.HOUR_OF_DAY, 1)){
			result.addAll(generarPublicacionesPorHora(diaSemana,
					hora.getTime(),
					publicacionesProgramadasPorAnuncio,
					anuncios));
		}
		
		//Las 23:00 se hace fuera del bucle, de otro modo no entra al ser ciclico...
		//Si se pone la condicion de mientras hora < 23:00 entramos en un bucle infinito
		Calendar hora = new GregorianCalendar();
		hora.set(Calendar.HOUR_OF_DAY, 23);
		hora.set(Calendar.MINUTE, 00);
		//A�ado un nuevo mapa con la hora a la lista de resultados
		result.addAll(generarPublicacionesPorHora(diaSemana,
				hora.getTime(), 
				publicacionesProgramadasPorAnuncio,
				anuncios));
		
		return result;
	}
	
	/**
	 * Este metodo genera las publicaciones efectivas que deber�an realizarse
	 * en la hora que se le pasa como parametro
	 * @param usuario
	 * @param diaSemana
	 * @param hora
	 * @return
	 */
	private List<SpaPublicacionEfectiva> generarPublicacionesPorHora(
			DiasSemanaEnum diaSemana,
			Date hora, 
			Map<Long,List<SpaPublicacionProgramada>> publicacionesPorAnuncio,
			List<SpaAnuncio> anuncios){

		//Lista de mapas
		//Cada mapa es una publicacion efectiva
		List<SpaPublicacionEfectiva> result = new ArrayList<SpaPublicacionEfectiva>();
				
		//Para cada anuncio...
		for(SpaAnuncio anuncio : anuncios){
			
			//Recupero las publicaciones programadas de cada anuncio
			List<SpaPublicacionProgramada> publicaciones = 
				publicacionesPorAnuncio.get(anuncio.getAnunId());
			
			if( publicaciones != null){
				for(SpaPublicacionProgramada publiProg : publicaciones){
					//this.minutoFinalUltimaHora = 0;
					//Si la publicaci�n programada deber�a generar alguna publicacion efectiva en �sta hora y �ste dia...
					if(publiProg.getDiaSemana() == diaSemana){
						
						Boolean DentroDelIntervaloHorario = false;
						Boolean MismaHoraPeroDentroIntervaloMinutos = false;
						
						if(dateToCalendar(publiProg.getHoraMindesde()).get(Calendar.HOUR_OF_DAY) <= 
								dateToCalendar(hora).get(Calendar.HOUR_OF_DAY)
								&& 
								dateToCalendar(publiProg.getHoraMinHasta()).get(Calendar.HOUR_OF_DAY) >= 
								dateToCalendar(hora).get(Calendar.HOUR_OF_DAY)
							){
							DentroDelIntervaloHorario = true;
						}
						if(dateToCalendar(publiProg.getHoraMindesde()).get(Calendar.HOUR_OF_DAY) == 
							dateToCalendar(hora).get(Calendar.HOUR_OF_DAY)
							&& 
							dateToCalendar(publiProg.getHoraMinHasta()).get(Calendar.HOUR_OF_DAY) == 
							dateToCalendar(hora).get(Calendar.HOUR_OF_DAY)
							&&
							dateToCalendar(publiProg.getHoraMindesde()).get(Calendar.MINUTE) <= 
							dateToCalendar(hora).get(Calendar.MINUTE)
							&&
							dateToCalendar(publiProg.getHoraMinHasta()).get(Calendar.MINUTE) > 
							dateToCalendar(hora).get(Calendar.MINUTE)
						){
							MismaHoraPeroDentroIntervaloMinutos = true;
						}
						if(DentroDelIntervaloHorario || MismaHoraPeroDentroIntervaloMinutos){
							//Seteo los minutos iniciales de la hora a los minutos iniciales de la hora desde de la publicacion
							//Solo si es la primera hora del intervalo
							if(dateToCalendar(hora).get(Calendar.HOUR_OF_DAY) 
							== dateToCalendar(publiProg.getHoraMindesde()).get(Calendar.HOUR_OF_DAY)){
							
								Integer minutes = dateToCalendar(publiProg.getHoraMindesde()).get(Calendar.MINUTE);
								Calendar horaCalendar = dateToCalendar(hora);
								horaCalendar.set(Calendar.MINUTE, minutes);
								hora = horaCalendar.getTime();
							}
							else{
								if (this.minutoFinalUltimaHora.containsKey(anuncio)){
									Calendar horaCalendar = dateToCalendar(hora);
									horaCalendar.set(Calendar.MINUTE, this.minutoFinalUltimaHora.get(anuncio));
									hora = horaCalendar.getTime();								
								}
							}
							//Genero las publicaciones que deber�an hacerse en �sta hora
							for(Calendar h = dateToCalendar(hora);
							h.get(Calendar.HOUR_OF_DAY) == dateToCalendar(hora).get(Calendar.HOUR_OF_DAY);
							h.add(Calendar.MINUTE, publiProg.getIntervaloMin())){
								if(
									!(
									((h.get(Calendar.HOUR_OF_DAY) == 
										dateToCalendar(publiProg.getHoraMinHasta()).get(Calendar.HOUR_OF_DAY)
										&&
										h.get(Calendar.MINUTE) >
										dateToCalendar(publiProg.getHoraMinHasta()).get(Calendar.MINUTE))
									)
									
									||
									
									((h.get(Calendar.HOUR_OF_DAY) == 
									dateToCalendar(publiProg.getHoraMindesde()).get(Calendar.HOUR_OF_DAY)
									&&
									h.get(Calendar.MINUTE) <
									dateToCalendar(publiProg.getHoraMindesde()).get(Calendar.MINUTE)))
									)
								){
									

									if((60 - h.get(Calendar.MINUTE)) == publiProg.getIntervaloMin()){
										this.minutoFinalUltimaHora.put(anuncio, 0);
									}
									else{
										this.minutoFinalUltimaHora.put(anuncio, publiProg.getIntervaloMin() - (60 - h.get(Calendar.MINUTE)));	
									}	
									
									Calendar diaSemanaCalendar = diaSemana.getDiaSemanaEnCalendar(publiProg.getDiaSemana());
									
									Date fecha = h.getTime();
									Calendar fechaCalendar = dateToCalendar(fecha);
									fechaCalendar.set(Calendar.DAY_OF_WEEK, diaSemanaCalendar.get(Calendar.DAY_OF_WEEK));
									
									SpaPublicacionEfectiva publicacionEfectiva = new SpaPublicacionEfectiva();
									publicacionEfectiva.setFechaPublicacion(fechaCalendar.getTime());
									publicacionEfectiva.setIdAnuncio(anuncio.getAnunId());
									publicacionEfectiva.setIdPublicacionProgramada(publiProg.getId());
									
									result.add(publicacionEfectiva);
								}
							}
						}
					}
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Dada una fecha en Date devuelve un Calendar inicializado 
	 * para esa fecha
	 * @param date
	 * @return
	 */
	private Calendar dateToCalendar(Date date){
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		return gc;
	}
	
}
