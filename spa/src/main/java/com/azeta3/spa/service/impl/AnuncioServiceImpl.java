package com.azeta3.spa.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.azeta3.spa.bom.SpaAnuncio;
import com.azeta3.spa.bom.SpaPublicacionEfectiva;
import com.azeta3.spa.dao.AnuncioDAO;
import com.azeta3.spa.dao.ImagenDAO;
import com.azeta3.spa.dao.PublicacionEfectivaDAO;
import com.azeta3.spa.dao.PublicacionProgramadaDAO;
import com.azeta3.spa.service.AnuncioService;

public class AnuncioServiceImpl implements AnuncioService {
	
	private Logger log = Logger.getLogger(AnuncioServiceImpl.class);
	
	@Autowired
	private AnuncioDAO anuncioDao;
	
	@Autowired
	private PublicacionProgramadaDAO publicacionProgramadaDao;
	
	@Autowired
	private ImagenDAO imagenDao;
	
	@Autowired
	private PublicacionEfectivaDAO publicacionEfectivaDao;

	@Override
	public SpaAnuncio nuevoAnuncio(SpaAnuncio anuncio) {
		return (SpaAnuncio) anuncioDao.creaAnuncio(anuncio);
	}

	@Override
	public void eliminaAnuncio(SpaAnuncio anuncio, String idUsuarioEmail) {
		
		List<SpaAnuncio> anunciosHijos = new ArrayList<SpaAnuncio>();
		anunciosHijos = buscarTodosHijos(anuncio, idUsuarioEmail, anunciosHijos);
		
		anunciosHijos.add(anuncio);
		
		//Borrar publicaciones programadas y las imagenes del anuncio
		for(SpaAnuncio anunHijo : anunciosHijos){
				
				//Eliminamos las publicaciones programadas
				for(Long idPublicacion : anunHijo.getIdPublicacionesProgramadas()){
					publicacionProgramadaDao.remove(idPublicacion);
				}
				
				List<SpaPublicacionEfectiva> publicacionesEfectivas = 
						publicacionEfectivaDao.findByAnuncio(anunHijo.getAnunId());
				for(SpaPublicacionEfectiva publicacionEfectiva : publicacionesEfectivas){
					publicacionEfectivaDao.remove(publicacionEfectiva.getId());
				}
			
			try{
				for(Long idImagen : anunHijo.getIdsImagenes()){
					imagenDao.delete(idImagen);
				}				
			}catch (Exception e) {
				// TODO: handle exception
				log.debug("No se ha encontrado la imagen en el anuncio");
			}
			
			anuncioDao.eliminaAnuncio(anunHijo);
		}
	}
	
	/**
	 * 
	 * @param anuncio
	 * @param idUsuarioEmail
	 * @param anunciosFound Array inicializado!
	 * @return
	 */
	public List<SpaAnuncio> buscarTodosHijos(SpaAnuncio anuncio, 
			String idUsuarioEmail, List<SpaAnuncio> anunciosFound){		
		
		//Si es padre
		if(!anuncio.getLeaf()){
			//Recuperamos todos sus hijos
			List<SpaAnuncio> anuncios = 
				anuncioDao.findByPadre(anuncio.getAnunId().toString(), idUsuarioEmail);
			//agregamos el nodo a la lista de nodos recuperados	
			anunciosFound.add(anuncio);
			for(SpaAnuncio anuncioFound : anuncios){
				//buscamos los hijos del nodo
				buscarTodosHijos(anuncioFound, idUsuarioEmail, anunciosFound);
			}
		}
		else{//si no es padre lo a�adimos a la lista
			anunciosFound.add(anuncio);
		}
		return anunciosFound;
	}

	@Override
	public SpaAnuncio findAnuncioById(Long id) {
		// TODO Auto-generated method stub
		return anuncioDao.findAnuncioById(id);
	}

	@Override
	public List<SpaAnuncio> findAllAnuncios() {
		// TODO Auto-generated method stub
		return anuncioDao.findAllAnuncios();
	}

	@Override
	public List<SpaAnuncio> findByPadre(String id, String usuarioEmail) {
		// TODO Auto-generated method stub
		return anuncioDao.findByPadre(id, usuarioEmail);
	}

	@Override
	public void update(SpaAnuncio anuncio) {
		anuncioDao.updateAnuncio(anuncio);
	}

	@Override
	public List<SpaAnuncio> findByUsuario(String email) {
		// TODO Auto-generated method stub
		return anuncioDao.findByUserEmail(email);
	}
}
