package com.azeta3.spa.service;

import org.springframework.stereotype.Service;

import com.azeta3.spa.bom.SpaUsuario;

@Service
public interface AgendaService {
	
	public Object generarAgendaJSON(SpaUsuario usuario);

}
