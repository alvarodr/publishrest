package com.azeta3.spa.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.azeta3.spa.bom.SpaAnuncio;
import com.azeta3.spa.bom.SpaPublicacionProgramada;
import com.azeta3.spa.bom.SpaUsuario;
import com.azeta3.spa.constantes.DiasSemanaEnum;
import com.azeta3.spa.dao.AnuncioDAO;
import com.azeta3.spa.dao.PublicacionProgramadaDAO;
import com.azeta3.spa.service.AgendaService;

@Service
public class AgendaServiceImpl implements AgendaService{

	@Autowired
	private PublicacionProgramadaDAO publicacionProgramadaDao;
	
	@Autowired
	private AnuncioDAO anuncioDao;
	
	private Logger log = Logger.getLogger(this.getClass());
	
	private Map<SpaAnuncio, Integer> minutoFinalUltimaHora = new HashMap<SpaAnuncio, Integer>();
	
	public Object generarAgendaJSON(SpaUsuario usuario){
		
		//Obtengo los anuncios del usuario
		List<SpaAnuncio> anuncios = anuncioDao.findByUserEmail(usuario.getEmail());
		
		//Obtengo las publicaciones programadas por anuncio
		Map<Long,List<SpaPublicacionProgramada>> publicacionesPorAnuncio = 
			new HashMap<Long, List<SpaPublicacionProgramada>>();
		
		for(SpaAnuncio anuncio : anuncios){
			List<SpaPublicacionProgramada> publicaciones = new ArrayList<SpaPublicacionProgramada>();
			for(Long idPublicacion : anuncio.getIdPublicacionesProgramadas()){
				SpaPublicacionProgramada p = publicacionProgramadaDao.findById(idPublicacion);
				publicaciones.add(p);
			}
			publicacionesPorAnuncio.put(anuncio.getAnunId(), publicaciones);
		}
		
		return generarDias(usuario, anuncios, publicacionesPorAnuncio);
		
	}
	
	@SuppressWarnings("rawtypes")
	private Object generarDias(SpaUsuario usuario, 
			List<SpaAnuncio> anuncios, Map<Long,List<SpaPublicacionProgramada>> publicacionesPorAnuncio){
		
		//Lista de mapas
		//Cada mapa es un dia
		List<Map<Object,Object>> result = new ArrayList<Map<Object,Object>>();
		
		for(DiasSemanaEnum diaSemana : DiasSemanaEnum.values()){
			//A�ado un nuevo mapa con el dia a la lista de resultados
			Map<Object,Object> diaResult = new HashMap<Object,Object>();
			diaResult.put("title", "<b>" + diaSemana.getDia() + "</b>");
			diaResult.put("expanded", "true");
			diaResult.put("iconCls", "calendar");
			diaResult.put("children", generarHorasPorDia(usuario, diaSemana, anuncios, publicacionesPorAnuncio));
			
			if( ((List)diaResult.get("children")).size()>0  ){
				diaResult.put("leaf", "false");
			}
			else{
				diaResult.put("leaf", "true");
			}
			
			result.add(diaResult);
		}
		
		return result;
	}
	
	@SuppressWarnings("rawtypes")
	private Object generarHorasPorDia(SpaUsuario usuario, DiasSemanaEnum diaSemana,
			List<SpaAnuncio> anuncios, Map<Long,List<SpaPublicacionProgramada>> publicacionesPorAnuncio){
		
		//Lista de mapas
		//Cada mapa es una hora
		List<Map<Object,Object>> result = new ArrayList<Map<Object,Object>>();
				
		Calendar hcalendar = new GregorianCalendar();
		hcalendar.set(Calendar.HOUR_OF_DAY, 00);
		hcalendar.set(Calendar.MINUTE, 00);
		
		for(Calendar hora = dateToCalendar(hcalendar.getTime());
		hora.get(Calendar.HOUR_OF_DAY)<=22;
		hora.add(Calendar.HOUR_OF_DAY, 1)){
			//this.minutoFinalUltimaHora = 0;
			//A�ado un nuevo mapa con la hora a la lista de resultados
			Map<Object,Object> horaResult = new HashMap<Object,Object>();
			horaResult.put("title", "<b>[ " + 
					hora.get(Calendar.HOUR_OF_DAY) +
					":00 - " + 
					(hora.get(Calendar.HOUR_OF_DAY) + 1) + 
					":00 ]</b>");
			horaResult.put("iconCls", "time");
			//hora.set(Calendar.MINUTE, this.minutoFinalUltimaHora);
			horaResult.put("children", generarPublicacionesPorHora(usuario, diaSemana, hora.getTime(),
					anuncios,publicacionesPorAnuncio));
			
			if( ((List)horaResult.get("children")).size()>0  ){
				horaResult.put("leaf", "false");
				result.add(horaResult);
			}
			else{
				horaResult.put("leaf", "true");
			}
		}
		
		//Las 23:00 se hace fuera del bucle, de otro modo no entra al ser ciclico...
		//Si se pone la condicion de mientras hora < 23:00 entramos en un bucle infinito
		Calendar hora = new GregorianCalendar();
		hora.set(Calendar.HOUR_OF_DAY, 23);
		hora.set(Calendar.MINUTE, 00);
		//A�ado un nuevo mapa con la hora a la lista de resultados
		Map<Object,Object> horaResult = new HashMap<Object,Object>();
		horaResult.put("title", "<b>[ " + 
				hora.get(Calendar.HOUR_OF_DAY) +
				":00 - 00:00 ]</b>");
		horaResult.put("iconCls", "time");
		//hora.set(Calendar.MINUTE, this.minutoFinalUltimaHora);
		horaResult.put("children", generarPublicacionesPorHora(usuario, diaSemana, hora.getTime(),anuncios,publicacionesPorAnuncio));
		
		if( ((List)horaResult.get("children")).size()>0  ){
			horaResult.put("leaf", "false");
			result.add(horaResult);
		}
		else{
			horaResult.put("leaf", "true");
		}
		
		return result;
	}
	
	/**
	 * Este metodo genera las publicaciones efectivas que deber�an realizarse
	 * en la hora que se le pasa como parametro
	 * @param usuario
	 * @param diaSemana
	 * @param hora
	 * @return
	 */
	private Object generarPublicacionesPorHora(SpaUsuario usuario, DiasSemanaEnum diaSemana,
			Date hora, List<SpaAnuncio> anuncios, Map<Long,List<SpaPublicacionProgramada>> publicacionesPorAnuncio){

		//Lista de mapas
		//Cada mapa es una publicacion efectiva
		List<Map<String,String>> result = new ArrayList<Map<String,String>>();
				
		//Para cada anuncio...
		for(SpaAnuncio anuncio : anuncios){
			
			//Recupero las publicaciones programadas de cada anuncio
			List<SpaPublicacionProgramada> publicaciones = 
				publicacionesPorAnuncio.get(anuncio.getAnunId());
			
			for(SpaPublicacionProgramada publiProg : publicaciones){
				//this.minutoFinalUltimaHora = 0;
				//Si la publicaci�n programada deber�a generar alguna publicacion efectiva en �sta hora y �ste dia...
				if(publiProg.getDiaSemana() == diaSemana){
					
					Boolean DentroDelIntervaloHorario = false;
					Boolean MismaHoraPeroDentroIntervaloMinutos = false;
					
					if(dateToCalendar(publiProg.getHoraMindesde()).get(Calendar.HOUR_OF_DAY) <= 
							dateToCalendar(hora).get(Calendar.HOUR_OF_DAY)
							&& 
							dateToCalendar(publiProg.getHoraMinHasta()).get(Calendar.HOUR_OF_DAY) >= 
							dateToCalendar(hora).get(Calendar.HOUR_OF_DAY)
						){
						DentroDelIntervaloHorario = true;
					}
					if(dateToCalendar(publiProg.getHoraMindesde()).get(Calendar.HOUR_OF_DAY) == 
						dateToCalendar(hora).get(Calendar.HOUR_OF_DAY)
						&& 
						dateToCalendar(publiProg.getHoraMinHasta()).get(Calendar.HOUR_OF_DAY) == 
						dateToCalendar(hora).get(Calendar.HOUR_OF_DAY)
						&&
						dateToCalendar(publiProg.getHoraMindesde()).get(Calendar.MINUTE) <= 
						dateToCalendar(hora).get(Calendar.MINUTE)
						&&
						dateToCalendar(publiProg.getHoraMinHasta()).get(Calendar.MINUTE) > 
						dateToCalendar(hora).get(Calendar.MINUTE)
					){
						MismaHoraPeroDentroIntervaloMinutos = true;
					}
					if(DentroDelIntervaloHorario || MismaHoraPeroDentroIntervaloMinutos){
						//Seteo los minutos iniciales de la hora a los minutos iniciales de la hora desde de la publicacion
						//Solo si es la primera hora del intervalo
						if(dateToCalendar(hora).get(Calendar.HOUR_OF_DAY) 
						== dateToCalendar(publiProg.getHoraMindesde()).get(Calendar.HOUR_OF_DAY)){
						
							Integer minutes = dateToCalendar(publiProg.getHoraMindesde()).get(Calendar.MINUTE);
							Calendar horaCalendar = dateToCalendar(hora);
							horaCalendar.set(Calendar.MINUTE, minutes);
							hora = horaCalendar.getTime();
						}
						else{
							if (this.minutoFinalUltimaHora.containsKey(anuncio)){
								Calendar horaCalendar = dateToCalendar(hora);
								horaCalendar.set(Calendar.MINUTE, this.minutoFinalUltimaHora.get(anuncio));
								hora = horaCalendar.getTime();								
							}
						}
						//Genero las publicaciones que deber�an hacerse en �sta hora
						for(Calendar h = dateToCalendar(hora);
						h.get(Calendar.HOUR_OF_DAY) == dateToCalendar(hora).get(Calendar.HOUR_OF_DAY);
						h.add(Calendar.MINUTE, publiProg.getIntervaloMin())){
							if(
								!(
								((h.get(Calendar.HOUR_OF_DAY) == 
									dateToCalendar(publiProg.getHoraMinHasta()).get(Calendar.HOUR_OF_DAY)
									&&
									h.get(Calendar.MINUTE) >
									dateToCalendar(publiProg.getHoraMinHasta()).get(Calendar.MINUTE))
								)
								
								||
								
								((h.get(Calendar.HOUR_OF_DAY) == 
								dateToCalendar(publiProg.getHoraMindesde()).get(Calendar.HOUR_OF_DAY)
								&&
								h.get(Calendar.MINUTE) <
								dateToCalendar(publiProg.getHoraMindesde()).get(Calendar.MINUTE)))
								)
							){
								
								//A�ado un nuevo mapa de publicacion efectiva a la lista de resultados
								Map<String,String> publiEfect = new HashMap<String, String>();
								publiEfect.put("title", anuncio.getTitulo());
								
								//Salia 0 en vez de 00, por eso creo este String
								String minutos = h.get(Calendar.MINUTE)== 0?"00":Integer.toString(h.get(Calendar.MINUTE)); 
								publiEfect.put("fechaPublicacion","10/10/2011 " + //una fecha cualquiera
										h.get(Calendar.HOUR_OF_DAY) + 
										":" + 
										minutos +
										":00");
								publiEfect.put("iconCls", "file");
								publiEfect.put("enlace", "");
								publiEfect.put("leaf", "true");
								if((60 - h.get(Calendar.MINUTE)) == publiProg.getIntervaloMin()){
									this.minutoFinalUltimaHora.put(anuncio, 0);
								}
								else{
									this.minutoFinalUltimaHora.put(anuncio, publiProg.getIntervaloMin() - (60 - h.get(Calendar.MINUTE)));	
								}
								
								result.add(publiEfect);
							}
						}
					}
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Dada una fecha en Date devuelve un Calendar inicializado 
	 * para esa fecha
	 * @param date
	 * @return
	 */
	private Calendar dateToCalendar(Date date){
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(date);
		return gc;
	}
	
}
