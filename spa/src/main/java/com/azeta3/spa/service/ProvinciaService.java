package com.azeta3.spa.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.azeta3.spa.dto.SpaCombo;

@Service
public interface ProvinciaService {

	public List<SpaCombo> getProvsByQuery(String query);
}
