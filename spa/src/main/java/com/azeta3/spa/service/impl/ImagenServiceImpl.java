package com.azeta3.spa.service.impl;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.azeta3.spa.bom.SpaImagen;
import com.azeta3.spa.dao.ImagenDAO;
import com.azeta3.spa.service.ImagenService;

@Service
public class ImagenServiceImpl implements ImagenService{

	private Logger log = Logger.getLogger(ImagenService.class);
	
	@Autowired
	private ImagenDAO imagenDao;

	@Override
	public SpaImagen create(SpaImagen imagen) {
		return imagenDao.create(imagen);
	}

	@Override
	public SpaImagen findById(Long idImagen) {
		return imagenDao.findById(idImagen);
	}

	@Override
	public void update(SpaImagen imagen) {
		imagenDao.update(imagen);
	}

	@Override
	public void delete(Long idImagen) {
		imagenDao.delete(idImagen);
	}

	@Override
	public List<SpaImagen> findByIdAnuncio(Long idAnuncio) {
		return imagenDao.findByIdAnuncio(idAnuncio);
	}

	@Override
	public byte[] ModificarImagen(byte[] imagen) {
		// TODO Auto-generated method stub
		ByteArrayOutputStream byteArrayOutputStream = null;
		try{
			ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(imagen);
			BufferedImage img = ImageIO.read(byteArrayInputStream);
			
			//Obtenemos una coordenada X aleatoria de la imagen
			int x = (int) Math.floor(Math.random() * img.getWidth() + 1);
			
			//Obtenemos una coordenada Y aleatoria de la imagen
			int y = (int) Math.floor(Math.random() * img.getHeight() + 1);
			
			//Cambiamos el RGB del pixel seleccionado
			img.setRGB(x, y, img.getRGB(x, y) + 1);
			
			byteArrayOutputStream = new ByteArrayOutputStream();
			ImageIO.write(img, "JPG", byteArrayOutputStream);
		}catch (Exception e) {
			// TODO: handle exception
			log.error("Se ha producido un error en el tratamiento de la imagen", e);
		}
		return byteArrayOutputStream.toByteArray();
	}
	
	
}
