package com.azeta3.spa.service;

import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Service;

import com.azeta3.spa.bom.SpaPublicacionProgramada;

@Service
public interface PublicacionesProgramadasService {
	public Long guardaPublicacion(SpaPublicacionProgramada publi);
	public SpaPublicacionProgramada findById(Long id);
	public void delete(Long id);
	public List<SpaPublicacionProgramada> findPublicacionesExecuteNow(Calendar now, String idUsuario);
}
