package com.azeta3.spa.service;

import java.io.IOException;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

@Service
public interface BulkDataService {
	
	/**
	 * 
	 * @param ficheroCSV
	 * @throws IOException
	 */
	void cargarProvincias(Resource ficheroCSV) throws IOException;

	/**
	 * 
	 * @param ficheroCSV
	 * @throws IOException
	 */
	void cargarUsuarios(Resource ficheroCSV) throws IOException;

	/**
	 * 
	 * @param ficheroCSV
	 * @throws IOException
	 */
	void cargarCategorias(Resource ficheroCSV) throws IOException;

	/**
	 * 
	 * @param ficheroCSV
	 * @throws IOException
	 */
	void cargarLocalidades(Resource ficheroCSV) throws IOException;

	/**
	 * 
	 * @param ficheroCSV
	 * @throws IOException
	 */
	void cargarDirectorioRaiz(Resource ficheroCSV) throws IOException;

	/**
	 * 
	 * @param ficheroCSV
	 * @throws IOException
	 */
	void cargarProxys(Resource ficheroCSV) throws IOException;
}
