package com.azeta3.spa.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.azeta3.spa.bom.SpaAnuncio;
import com.azeta3.spa.bom.SpaUsuario;
import com.azeta3.spa.dao.UsuarioDAO;
import com.azeta3.spa.security.Roles;
import com.azeta3.spa.service.AnuncioService;
import com.azeta3.spa.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService{

	@Autowired
	private AnuncioService anuncioService;
	
	@Autowired
	private UsuarioDAO usuarioDao;
	
	@Override
	public List<SpaUsuario> findAll() {
		return usuarioDao.findAll();
	}

	@Override
	public List<SpaUsuario> findUsersByRole(Roles rol) {
		return usuarioDao.findUsersByRole(rol);
	}
	
	@Override
	public SpaUsuario findByEmail(String email){
		
		return usuarioDao.findById(email.toUpperCase());
	}
	
	@Override
	public void removeUser(String email){
		
		SpaUsuario usuario = new SpaUsuario();
		usuario.setEmail(email);
		usuarioDao.remove(usuario);
		
		//Elimino todos sus anuncios
		SpaAnuncio raiz = anuncioService.findAnuncioById(-1L);
		List<SpaAnuncio> anunciosBajoRaiz = anuncioService.findByPadre(raiz.getAnunId().toString(), email);

		for(SpaAnuncio anuncio : anunciosBajoRaiz){
			anuncioService.eliminaAnuncio(anuncio, email); //Elimina los anuncios y sus hijos
		}
	}
	
	@Override
	public void createUser(SpaUsuario usuario){
		usuario.setEmail(usuario.getEmail().toUpperCase());
		usuarioDao.create(usuario);
	}

	@Override
	public void update(SpaUsuario usuario) {
		usuarioDao.update(usuario);
	}
	
}
