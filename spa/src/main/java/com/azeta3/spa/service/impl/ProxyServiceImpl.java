package com.azeta3.spa.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.azeta3.spa.bom.SpaProxy;
import com.azeta3.spa.dao.ProxyDAO;
import com.azeta3.spa.service.ProxyService;

@Service
public class ProxyServiceImpl implements ProxyService{

	@Autowired
	private ProxyDAO proxyDao;
	
	@Override
	public List<SpaProxy> findAll() {
		return proxyDao.findAll();
	}

	@Override
	public SpaProxy create(SpaProxy proxy) {
		return proxyDao.create(proxy);
	}

	@Override
	public void delete(Long id) {
		proxyDao.delete(id);
		
	}

	@Override
	public void update(SpaProxy proxy) {
		proxyDao.update(proxy);
		
	}

	@Override
	public SpaProxy findById(Long id) {
		return proxyDao.findById(id);
	}

	
}
