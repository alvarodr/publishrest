package com.azeta3.spa.service;

import java.util.List;

import com.azeta3.spa.bom.SpaProxy;

public interface ProxyService {

	public List<SpaProxy> findAll();
	public SpaProxy create(SpaProxy proxy);
	public void delete(Long id);
	public void update(SpaProxy proxy);
	public SpaProxy findById(Long id);
	
}
