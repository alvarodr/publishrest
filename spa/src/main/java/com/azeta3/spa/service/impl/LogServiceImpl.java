package com.azeta3.spa.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.azeta3.spa.bom.SpaAnuncio;
import com.azeta3.spa.bom.SpaPublicacionEfectiva;
import com.azeta3.spa.service.AnuncioService;
import com.azeta3.spa.service.LogService;
import com.azeta3.spa.service.PublicacionEfectivaService;

@Service
public class LogServiceImpl implements LogService{

	private Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private PublicacionEfectivaService publicacionEfectivaService;
	
	@Autowired
	private AnuncioService anuncioService;
	
	@Override
	public List<Map<String,Object>> getLogJSON(String idEmailUsuario, Integer start, Integer limit) {
		
		List<Map<String,Object>> listaJSON = new ArrayList<Map<String,Object>>();
		
		List<SpaPublicacionEfectiva> pubEfectivas = publicacionEfectivaService.findByAnuncio(idEmailUsuario, start, limit);

		for(SpaPublicacionEfectiva publicacion : pubEfectivas){
			try{
				Map<String,Object> pub = new HashMap<String, Object>();
				SpaAnuncio anuncio = anuncioService.findAnuncioById(publicacion.getIdAnuncio());
				pub.put("anuncio", anuncio.getTitulo() +
						" ID " + anuncio.getAnunId());
				
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
				sdf.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
				String fecha = sdf.format(publicacion.getFechaPublicacion());
				
				pub.put("fecha", fecha);
				
				pub.put("estado", publicacion.getEstado());
				
				listaJSON.add(pub);
			}
			catch(Exception e){
				log.error(e.getMessage());
			}
			
		}
		
		return listaJSON;
	}

	@Override
	public Integer getTotalLog(String usuario, Integer start, Integer limit){
		return publicacionEfectivaService.getTotalLogByUsua(usuario, start, limit);
	}
}
