package com.azeta3.spa.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;

import com.azeta3.spa.bom.SpaUsuario;
import com.azeta3.spa.dao.UsuarioDAO;
import com.azeta3.spa.security.Roles;
import com.azeta3.spa.service.AdminPanelService;

public class AdminPanelServiceImpl implements AdminPanelService{

	@Autowired
	private UsuarioDAO usuarioDao;
	
	public AdminPanelServiceImpl(){}
	
	public AdminPanelServiceImpl(UsuarioDAO usuarioDao){
		this.usuarioDao = usuarioDao;
	}
	
	@Override
	public Object getUsuariosJSON() {
		
		List<Object> result = new ArrayList<Object>();
		
		List<SpaUsuario> usuarios = usuarioDao.findAll();
		for(SpaUsuario usuario : usuarios){
			
			if(usuario.getAuthorities().size() == 1){
				Set<GrantedAuthority> authorities = usuario.getAuthorities();
				for(GrantedAuthority authority : authorities){
					
					if(authority.getAuthority().equals(Roles.ROLE_USER.getAuthority())){
						Map<String,String> usuarioMap = new HashMap<String, String>();
						usuarioMap.put("usuario", usuario.getEmail());
						usuarioMap.put("activo",Boolean.toString(usuario.isEnabled()));
						
						result.add(usuarioMap);
						break;
					}
					
				}
			}
		}
		
		return result;
	}

}
