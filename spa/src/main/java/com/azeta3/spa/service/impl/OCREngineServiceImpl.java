package com.azeta3.spa.service.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import prophecy.common.image.BWImage;

import com.azeta3.spa.service.OCREngineService;

import drjava.util.Tree;
import eye.eye03.EyeData;
import eye.eye03.RecognizerOnDisk;
import eyedev._01.ImageReader;
import eyedev._01.InputImage;
import eyedev._01.OCRUtil;
import eyedev._01.RecognizedText;
import eyedev._18.WithFratboySegmenter;

public class OCREngineServiceImpl implements OCREngineService{

	Logger log = Logger.getLogger(this.getClass());
	
	@Override
	public String ocr(byte[] imagen) throws IOException {
		
		EyeData anticaptcha = new EyeData();
		
		/**
		 * OBTENGO UN RECOGNIZER, OBJETO QUE CONTIENE EL JUEGO DE CARACTERES
		 * Y SABE QUE CARACTER CORRESPONDE A CADA LETRA
		 */
		
		//Establezco el Recognizer
		RecognizerOnDisk recognizer = new RecognizerOnDisk(
				new File("memory//recognizers//recognizer-Arial-Times-Courier-with-hungarian.eye"),
				"Arial//Times//Courier with hungarian");
		
		anticaptcha.setDefaultRecognizerName(recognizer.getName());
		
		/**
		 * CREO UN IMAGE READER, OBJETO QUE VA A LEER LA IMAGEN UTILIZANDO
		 * UN RECOGNIZER
		 */
		
		//Creo un objeto ImageReader, incializando sus propiedades internas
		//en base a la informaci�n del juego de caracteres del recognizer seleccionado,
		//que se le pasa en el constructor
		Tree characterSetTree = recognizer.getRecognizerInfo().getCode();
		ImageReader imageReader = OCRUtil.makeImageReader(characterSetTree);
		
		//Se sustituye la referencia del ImageReader por la que devuelve
		//�ste m�todo
		imageReader = new WithFratboySegmenter(imageReader);
		imageReader.setCollectDebugInfo(true);
		
		//Creo un objeto InputImagen con la imagen que contiene los caracteres
		//a reconocer
		BWImage bwImage = new BWImage(ImageIO.read(new ByteArrayInputStream(imagen)));
		InputImage inputImage = new InputImage(bwImage);
			
		/**
		 * LECTURA DE CARACTERES
		 */
		
		RecognizedText text = imageReader.extendedReadImage(inputImage);
		
		return text.text;
	}
	
}
