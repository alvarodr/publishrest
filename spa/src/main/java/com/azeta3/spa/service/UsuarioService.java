package com.azeta3.spa.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.azeta3.spa.bom.SpaUsuario;
import com.azeta3.spa.security.Roles;

@Service
public interface UsuarioService {
	
	public SpaUsuario findByEmail(String email);
	
	public void removeUser(String email);
	
	public void createUser(SpaUsuario usuario);
	
	public List<SpaUsuario> findAll();
	
	public List<SpaUsuario> findUsersByRole(Roles rol);

	public void update(SpaUsuario usuario);
}
