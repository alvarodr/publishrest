package com.azeta3.spa.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;


import com.azeta3.spa.bom.SpaProvincia;
import com.azeta3.spa.dao.ProvinciaDAO;
import com.azeta3.spa.dto.SpaCombo;
import com.azeta3.spa.service.ProvinciaService;

public class ProvinciaServiceImpl implements ProvinciaService {

	private Logger log = Logger.getLogger(ProvinciaServiceImpl.class);
	@Autowired
	private ProvinciaDAO provinciaDao;
	
	@Override
	public List<SpaCombo> getProvsByQuery(String query) {
		// TODO Auto-generated method stub
		List<SpaProvincia> provincias = provinciaDao.getAllProvincias();
		log.debug("Se han recuperado " + provincias.size() + " provincias");
		List<SpaCombo> listaCombo = new ArrayList<SpaCombo>();
		for (SpaProvincia prov : provincias){
			SpaCombo combo = new SpaCombo();
			if (query!="" && query!=null){
				if (prov.getNombre().startsWith(query.toUpperCase())){
					combo.setValoNombre(prov.getNombre());
					combo.setValoId(prov.getId());
					listaCombo.add(combo);
				}				
			}else{
				combo.setValoNombre(prov.getNombre());
				combo.setValoId(prov.getId());
				listaCombo.add(combo);			
			}
		}
		return listaCombo;
	}

	public ProvinciaDAO getProvinciaDao() {
		return provinciaDao;
	}

	public void setProvinciaDao(ProvinciaDAO provinciaDao) {
		this.provinciaDao = provinciaDao;
	}

}
