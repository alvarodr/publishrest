package com.azeta3.spa.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public interface LogService {

	public List<Map<String,Object>> getLogJSON(String idEmailUsuario, Integer start, Integer limit);
	public Integer getTotalLog(String usuario, Integer start, Integer limit);
}
