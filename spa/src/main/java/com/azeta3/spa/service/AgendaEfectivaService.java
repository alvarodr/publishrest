package com.azeta3.spa.service;

import java.util.List;

import com.azeta3.spa.bom.SpaPublicacionEfectiva;

public interface AgendaEfectivaService {

	public List<SpaPublicacionEfectiva> generarAgenda();
}
