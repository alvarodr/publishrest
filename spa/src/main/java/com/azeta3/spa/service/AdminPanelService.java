package com.azeta3.spa.service;

import org.springframework.stereotype.Service;

@Service
public interface AdminPanelService {
	
	public Object getUsuariosJSON();
	
}
