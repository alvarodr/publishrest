package com.azeta3.spa.service.impl;

import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.azeta3.spa.bom.SpaPublicacionProgramada;
import com.azeta3.spa.dao.PublicacionEfectivaDAO;
import com.azeta3.spa.dao.PublicacionProgramadaDAO;
import com.azeta3.spa.service.PublicacionesProgramadasService;


@Service
public class PublicacionesProgramadasServiceImpl implements PublicacionesProgramadasService{

	@Autowired
	private PublicacionEfectivaDAO publicacionEfectivaDao;
	
	private Logger log = Logger.getLogger(PublicacionesProgramadasServiceImpl.class);
	
	@Autowired
	private PublicacionProgramadaDAO publicacionProgramadaDao;
	
	@Override
	public Long guardaPublicacion(SpaPublicacionProgramada publi) {
		// TODO Auto-generated method stub
		return publicacionProgramadaDao.createPublicacionProgramada(publi).getId();
	}

	@Override
	public SpaPublicacionProgramada findById(Long id){
		return publicacionProgramadaDao.findById(id);
	}
	
	public PublicacionProgramadaDAO getPublicacionProgramadaDao() {
		return publicacionProgramadaDao;
	}

	public void setPublicacionProgramadaDao(
			PublicacionProgramadaDAO publicacionProgramadaDao) {
		this.publicacionProgramadaDao = publicacionProgramadaDao;
	}

	@Override
	public void delete(Long id) {
		publicacionProgramadaDao.remove(id);
	}

	@Override
	public List<SpaPublicacionProgramada> findPublicacionesExecuteNow(
			Calendar now, String idUsuario) {

		
		return null;
	}


}