package com.azeta3.spa.service.impl;

import static com.gargoylesoftware.htmlunit.BrowserVersion.FIREFOX_3_6;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;

import org.apache.log4j.Logger;
import org.fluttercode.datafactory.impl.DataFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.azeta3.spa.bom.SpaAnuncio;
import com.azeta3.spa.bom.SpaCategoria;
import com.azeta3.spa.bom.SpaImagen;
import com.azeta3.spa.bom.SpaProxy;
import com.azeta3.spa.dao.CategoriaDAO;
import com.azeta3.spa.service.CaptchaFilterService;
import com.azeta3.spa.service.ImagenService;
import com.azeta3.spa.service.OCREngineService;
import com.azeta3.spa.service.ProxyService;
import com.azeta3.spa.service.PublicaService;
import com.azeta3.spa.service.TextService;
import com.gargoylesoftware.htmlunit.ProxyConfig;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.WebResponse;
import com.gargoylesoftware.htmlunit.html.HtmlFileInput;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlOption;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlSelect;
import com.gargoylesoftware.htmlunit.html.HtmlTextArea;

@Service
public class PublicaServiceImpl implements PublicaService {

	private Logger log = Logger.getLogger(PublicaServiceImpl.class);
	
	private final static String CAPTCHAURL = "http://www.mundoanuncio.com/images/captcha_post.jpg";
	
	@Autowired
	private OCREngineService ocrEngineService;
	@Autowired
	private CaptchaFilterService captchaFilterService;
	@Autowired
	private ImagenService imagenService;
	@Autowired
	private CategoriaDAO categoriaDao;
	@Autowired 
	private ProxyService proxyService;
	@Autowired
	private TextService textService;

	/**
	 * Este test realiza una publicacion REAL en MundoAnuncio
	 */
	public Boolean publicarAnuncioMundoAnuncio(SpaAnuncio anuncio) {
		try{
			log.debug("#Entremos a publicar el anuncio " + anuncio.getTitulo());
			WebClient webClient = new WebClient(FIREFOX_3_6);
			webClient.setJavaScriptEnabled(false);
			webClient.setCssEnabled(false);
			webClient.setAppletEnabled(false);
			webClient.setThrowExceptionOnFailingStatusCode(false);
			webClient.setThrowExceptionOnScriptError(false);
			webClient.setPopupBlockerEnabled(true);
			
			//Obtenemos algun proxy que funcione de la lista
			Boolean proxyFunciona = false;
			Integer intentosProxy = 0;
			List<SpaProxy> proxys = proxyService.findAll();
			if(proxys.size() > 0){
				while(!proxyFunciona && intentosProxy < 3){
					log.debug("NUMERO PROXYS --> " + proxys.size());
					DataFactory df = new DataFactory();
					Integer intProxy = df.getNumberBetween(0, proxys.size() - 1);
					log.debug("RANDOM PROXY -> " + intProxy);
					SpaProxy proxy = proxys.get((int) (Math.random() * proxys.size())-1);
					ProxyConfig proxyConfig = new ProxyConfig(proxy.getHost(),proxy.getPort());
					proxyConfig.setSocksProxy(proxy.getSocks());
					webClient.setProxyConfig(proxyConfig);
					
					HtmlPage pagePrueba = webClient.getPage("http://www.mundoanuncio.com");
					if(pagePrueba.getBody().asText().contains("Anuncios gratis en Todo el Mundo")){
						proxyFunciona = true;
					}
					intentosProxy++;
				}
			}
			else{
				log.debug("PUBLICANDO SIN PROXY");
			}
			
			//obtenemos url de la categoria del anuncio que se va a publicar
			SpaCategoria categoria = categoriaDao.findCategoriaById(anuncio.getIdCategoria()); 
			log.debug("vamos a publicar un anuncio de la categoria " + anuncio.getIdCategoria());
			URL urlCategoria = new URL(categoria.getUrlParaPublicar());
			HtmlPage page = webClient.getPage(urlCategoria);
			
			//Obtenemos el codigo html del formulario
			HtmlForm form = (HtmlForm) page.getBody().getHtmlElementsByTagName("form").get(0);
			
			
			//Comprobamos si hay captcha
			try{
				//Captcha
				form.getInputByName("security_code").setValueAttribute(romperCaptcha(webClient));
			}catch(Exception e){
				log.debug("No hay Captcha!");
			}
			//Titulo
			form.getInputByName("titol").setValueAttribute(textService.Randomize(anuncio.getTituloMundoanuncio()));
			log.debug("######## SETEAMOS EL TITULO " + anuncio.getTituloMundoanuncio().toUpperCase());
			
			//Pais: ESPA�A
			HtmlSelect selectPais = (HtmlSelect)form.getElementById("id_pais");
			for(HtmlOption option : selectPais.getOptions()){
				if(option.getValueAttribute().equals("2")){
					option.setAttribute("selected", "selected");
					log.debug("########### SETEAMOS EL PAIS " + option.getValueAttribute());
					break;
				}
			}
			
			log.debug("############ ID PROVINCIA ######" + anuncio.getIdProvincia() + " ###############");
			
			//Provincia
			form.getInputByName("sel_prov").setValueAttribute(anuncio.getIdProvincia().toString());
			
			//Descripcion
			HtmlTextArea descripcion = (HtmlTextArea)form.getElementById("descripcio");
			descripcion.setText(textService.Randomize(anuncio.getDescripcionHTML()));
			
			log.debug("########## SETEAMOS LA DESCRIPCION " + anuncio.getDescripcionHTML());
					
			//EMail
			try{
				form.getInputByName("mail").setValueAttribute(anuncio.getEmail());
			}catch(Exception e){/*No se introduce email*/}
			
			/**
			 * A�adir imagenes
			 */
			List<Long> idsImagenes = anuncio.getIdsImagenes();
			//Indico que las imagenes est�n rellenas y sus descripciones tambien
			form.getInputByName("img_count").setValueAttribute(Integer.toString(idsImagenes.size()));
			form.getInputByName("img_max_id").setValueAttribute(Integer.toString(idsImagenes.size()));
			form.getInputByName("desc_tpl_edited").setValueAttribute("0");
			
			Integer numeroImagen = 1;//Contador
			for(Long idImagen : idsImagenes){
				
				if(numeroImagen > 1){
					form = insertarImagen(numeroImagen,form);
				}
				
				SpaImagen imagen = imagenService.findById(idImagen);
				
				HtmlFileInput htmlFileInput = (HtmlFileInput) form.getElementById("image_" + numeroImagen);
				htmlFileInput.setContentType("image/jpeg");
				
				
				
				ImageInputStream iis = ImageIO.createImageInputStream(new ByteArrayInputStream(imagen.getImagen()));
				File imageFile = newImageOnClasspath();
				ImageIO.write(ImageIO.read(iis),"jpg",imageFile);
				
				log.debug("#### la imagen se guarda en " + imageFile.getPath());
				
				htmlFileInput.setValueAttribute(imageFile.getPath());
				
				if(imagen.getLink() != null && !imagen.getLink().equals("")){
					HtmlInput descripcionInput = (HtmlInput) form.getElementById("desc_" + numeroImagen);
					HtmlInput descripcionEditada = (HtmlInput) form.getElementById("desc_" + numeroImagen + "_edited");
					
					descripcionInput.setValueAttribute(imagen.getLink());
					descripcionEditada.setValueAttribute("1");
				}
				
				numeroImagen++;
			}
			HtmlPage resultado = form.getInputByValue("Inserta anuncio").click();
			log.debug("############## RESULTADO PUBLICACION --> " + resultado.getPage().getBody().asText());
			if(resultado.getPage().asText().indexOf("Tu anuncio ha sido insertado correctamente") != -1){
				log.debug("############## HEMOS PUBLICADO BIEN");
				return true;
			}
			else{
				log.debug("############## HEMOS PUBLICADO MAL");
				return false;
			}
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}
	private HtmlForm insertarImagen(Integer numeroImagen, HtmlForm formulario){
		HtmlFileInput fileInput = (HtmlFileInput) formulario.getElementById("image_1");
		fileInput = (HtmlFileInput) fileInput.cloneNode(true);
		fileInput.setAttribute("name", "image_" + numeroImagen);
		fileInput.setAttribute("id", "image_" + numeroImagen);
		formulario.appendChild(fileInput);
		
		HtmlInput descripcionInput = (HtmlInput) formulario.getElementById("desc_1");
		descripcionInput = (HtmlInput)descripcionInput.cloneNode(true);
		descripcionInput.setAttribute("name", "desc_" + numeroImagen);
		descripcionInput.setAttribute("id", "desc_" + numeroImagen);
		formulario.appendChild(descripcionInput);
		
		HtmlInput descripcionEditada = (HtmlInput) formulario.getElementById("desc_1_edited");
		descripcionEditada = (HtmlInput)descripcionEditada.cloneNode(true);
		descripcionEditada.setAttribute("name", "desc_" + numeroImagen + "_edited");
		descripcionEditada.setAttribute("id", "desc_" + numeroImagen + "_edited");
		formulario.appendChild(descripcionEditada);
		return formulario;
	}
	
	private String romperCaptcha(WebClient webClient) throws Exception {

		List<String> resultados = new ArrayList<String>();
		
		/**
		 * Establezco un maximo de 30 intentos de lectura de la imagen
		 */
		for(int i = 0 ; i < 30; i++){
			WebResponse response = webClient.getPage(CAPTCHAURL).getWebResponse();
			
			BufferedImage bufferedImage = ImageIO.read(response.getContentAsStream());
			byte[] imageByte = imageToByte(bufferedImage);
			
			imageByte = captchaFilterService.mundoAnuncioCaptcha(imageByte);
			
			String r = "";
			try{
				r = ocrEngineService.ocr(imageByte);
			}catch(Exception e){}
			
			r = r.replace("\n", "");
			r = r.replace(" ","");
			if(r.length() == 6) resultados.add(r);
			
			Map<String,Integer> tmr = textoMasRepetido(resultados);
			Map.Entry<String, Integer> c = tmr.entrySet().iterator().next();
			
			/**
			 * Si dos lecturas han coincidido en valor retorno la lectura
			 */
			if(c.getValue() >= 6){
				log.debug("CAPTCHA ENCONTRADO CON " + i + " INTENTOS-> " + c.getKey());
				return c.getKey();
			}
		}
		
		Map<String,Integer> tmr = textoMasRepetido(resultados);
		Map.Entry<String, Integer> c = tmr.entrySet().iterator().next();
		log.debug("CAPTCHA ENCONTRADO-> " + c.getKey() + " CON EL MAXIMO DE INTENTOS"  + "(" + c.getValue() + " resoluciones coinciden)");
		return c.getKey();
	}
	
	/**
	 * Obtiene un mapa con UN UNICO ELEMENTO que representa el texto
	 * que m�s se repite y el numero de repeticiones en el conjunto.
	 * @param textos
	 * @return Map<String,Integer> Texto,Numero de Repeticiones
	 * @throws Exception
	 */
	private Map<String,Integer> textoMasRepetido(List<String> textos) throws Exception{
		
		//Obtengo las repeticiones por texto
		Map<String,Integer> textosPorRepeticion = new HashMap<String, Integer>();
		
		for(String texto : textos){
			if(textosPorRepeticion.get(texto) == null){
				textosPorRepeticion.put(texto, 1);
			}
			else{
				Integer repeticiones = textosPorRepeticion.get(texto);
				repeticiones++;
				textosPorRepeticion.remove(texto);
				textosPorRepeticion.put(texto, repeticiones);
				
			}
		}
		
		//Obtengo el texto que mas se repite
		Integer repeticionesCandidato = 0;
		String textoCandidato = "";
		
		Iterator<Entry<String,Integer>> itr = textosPorRepeticion.entrySet().iterator();
		while(itr.hasNext()){
			Entry<String,Integer> texto = itr.next();
			if(texto.getValue() > repeticionesCandidato){
				textoCandidato = texto.getKey();
				repeticionesCandidato = texto.getValue();
			}
		}
		Map<String,Integer> candidato = new HashMap<String,Integer>();
		candidato.put(textoCandidato, repeticionesCandidato);
		return candidato;
	}
	
	private byte[] imageToByte(BufferedImage img) throws IOException{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(img, "JPG", baos);
		return baos.toByteArray();
	}
	
	private File newImageOnClasspath() throws IOException{
		ClassPathResource r = new ClassPathResource("images/parent/file");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmm");
		Calendar now = Calendar.getInstance();
		now.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
		
		File imageFile = new File(r.getFile().getParent() +
				sdf.format(now.getTime()) +
				"_" + 
				Integer.toString(Calendar.getInstance().get(Calendar.MILLISECOND)) +
				".jpg");
		imageFile.createNewFile();
		return imageFile;
	}
}
