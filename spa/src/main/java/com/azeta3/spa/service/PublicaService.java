package com.azeta3.spa.service;

import com.azeta3.spa.bom.SpaAnuncio;

public interface PublicaService {
	public Boolean publicarAnuncioMundoAnuncio(SpaAnuncio anuncio);
}
