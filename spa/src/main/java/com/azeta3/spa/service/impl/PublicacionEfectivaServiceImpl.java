package com.azeta3.spa.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.azeta3.spa.bom.SpaPublicacionEfectiva;
import com.azeta3.spa.dao.PublicacionEfectivaDAO;
import com.azeta3.spa.service.PublicacionEfectivaService;

@Service
public class PublicacionEfectivaServiceImpl implements PublicacionEfectivaService{

	@Autowired
	private PublicacionEfectivaDAO publicacionEfectivaDao;
	
	@Override
	public SpaPublicacionEfectiva create(SpaPublicacionEfectiva publicacion) {
		
		List<SpaPublicacionEfectiva> publicaciones = publicacionEfectivaDao.findByAnuncio(publicacion.getIdAnuncio());
		
		if(publicaciones != null){
			Integer size = publicacionEfectivaDao.findByAnuncio(publicacion.getIdAnuncio()).size();
			if(size >= 10){
				SpaPublicacionEfectiva ultimaPublicacion = 
					publicacionEfectivaDao.findMasAntigua(publicacion.getIdAnuncio());
				if(ultimaPublicacion != null){
					publicacionEfectivaDao.remove(ultimaPublicacion.getId());
				}
			}
		}
		
		return publicacionEfectivaDao.create(publicacion);
		
	}

	@Override
	public SpaPublicacionEfectiva findById(Long id) {
		return publicacionEfectivaDao.findById(id);
	}

	@Override
	public List<SpaPublicacionEfectiva> findByAnuncio(Long idAnuncio) {
		return publicacionEfectivaDao.findByAnuncio(idAnuncio);
	}

	@Override
	public List<SpaPublicacionEfectiva> findByAnuncio(String usuario,
			Integer start, Integer limit) {
		// TODO Auto-generated method stub
		return publicacionEfectivaDao.findByAnuncio(usuario, start, limit);
	}

	@Override
	public Integer getTotalLogByUsua(String usuario, 
			Integer start, Integer limit){
		return publicacionEfectivaDao.getTotalByUsuario(usuario, start, limit);
	}

	@Override
	public SpaPublicacionEfectiva findMasAntigua(Long idAnuncio) {
		return publicacionEfectivaDao.findMasAntigua(idAnuncio);
	}

	@Override
	public void remove(Long idSpaPublicacionEfectiva) {
		publicacionEfectivaDao.remove(idSpaPublicacionEfectiva);
	}

	@Override
	public Integer getTotalByUsuario(String usuario, Integer start,
			Integer limit) {
		
		return publicacionEfectivaDao.getTotalByUsuario(usuario, start, limit);
	}
}
