package com.azeta3.spa.service;

import org.springframework.stereotype.Service;

@Service
public interface CronService {

	public void publicacion() throws Exception;
	public void borrarImagenes() throws Exception;
}
