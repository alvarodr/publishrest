package com.azeta3.spa.service.impl;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;

import com.azeta3.spa.service.CaptchaFilterService;
import com.jhlabs.image.BlurFilter;
import com.jhlabs.image.MaximumFilter;
import com.jhlabs.image.ThresholdFilter;

public class CaptchaFilterServiceImpl implements CaptchaFilterService{

	private Logger log = Logger.getLogger(CaptchaFilterServiceImpl.class);
	
	@Override
	public byte[] mundoAnuncioCaptcha(byte[] captcha) {
		
		ByteArrayInputStream byteArrayInputStream = null;
		ByteArrayOutputStream byteArrayOutputStream = null;
		try{
			byteArrayInputStream = new ByteArrayInputStream(captcha);
			BufferedImage img = ImageIO.read(byteArrayInputStream);
			
			// Applicamos filtros a la imagen para eliminar el ruido
			// http://www.jhlabs.com/ip/filters/index.html
			
			MaximumFilter maximumFilter = new MaximumFilter();
			img = maximumFilter.filter(img, img);
			
			BlurFilter blurFilter = new BlurFilter();
			img = blurFilter.filter(img, img);
			
			ThresholdFilter thresholdFilter = new ThresholdFilter();
			thresholdFilter.setLowerThreshold(235);
			thresholdFilter.setUpperThreshold(255);
			img = thresholdFilter.filter(img, img);
			

			//Borramos pixels sueltos de la imagen
			for (int j=0; j<10; j++){
				//Borra pixels solos en la horizontal PASO 1
				for(int x=1;x<img.getWidth()-1;x++) {
					for(int y=1;y<img.getHeight()-1;y++) {
						if(img.getRGB(x-1,y)==Color.WHITE.getRGB() &&
							img.getRGB(x+1,y)==Color.WHITE.getRGB()) {
							img.setRGB(x,y,Color.WHITE.getRGB());
						}
					}
				}
			  
				// Borra pixels solos en la vertical PASO 2
				for(int x=1;x<img.getWidth()-1;x++) {
					for(int y=1;y<img.getHeight()-1;y++) {
						if(img.getRGB(x,y-1)==Color.WHITE.getRGB() &&
							img.getRGB(x,y+1)==Color.WHITE.getRGB()) {
							img.setRGB(x,y,Color.WHITE.getRGB());
						}
					}
				}
			}

			byteArrayOutputStream = new ByteArrayOutputStream();
			ImageIO.write(img, "JPG", byteArrayOutputStream);
		}catch (Exception e) {
			// TODO: handle exception
			log.error("Error Producido en la manipulacion de la imagen", e);
		}
		
		return byteArrayOutputStream.toByteArray();
	}

}
