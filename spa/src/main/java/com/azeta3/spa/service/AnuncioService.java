package com.azeta3.spa.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.azeta3.spa.bom.SpaAnuncio;

@Service
public interface AnuncioService {
	public SpaAnuncio nuevoAnuncio(SpaAnuncio anuncio);
	public void eliminaAnuncio(SpaAnuncio anuncio, String idUsuarioEmail);
	public SpaAnuncio findAnuncioById(Long id);
	public List<SpaAnuncio> findAllAnuncios();
	public List<SpaAnuncio> findByPadre(String id, String usuarioEmail);
	public List<SpaAnuncio> findByUsuario(String email);
	public void update(SpaAnuncio anuncio);
}
