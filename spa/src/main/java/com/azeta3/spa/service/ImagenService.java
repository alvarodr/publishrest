package com.azeta3.spa.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.azeta3.spa.bom.SpaImagen;

@Service
public interface ImagenService {
	public SpaImagen create(SpaImagen imagen);
	public SpaImagen findById(Long idImagen);
	public void update(SpaImagen imagen);
	public void delete(Long idImagen);
	public List<SpaImagen> findByIdAnuncio(Long idAnuncio);
	public byte[] ModificarImagen(byte[] imagen);
}
