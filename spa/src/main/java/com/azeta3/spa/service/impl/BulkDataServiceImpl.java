package com.azeta3.spa.service.impl;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.token.Sha512DigestUtils;
import org.springframework.stereotype.Service;

import au.com.bytecode.opencsv.CSVReader;

import com.azeta3.spa.bom.SpaAnuncio;
import com.azeta3.spa.bom.SpaCategoria;
import com.azeta3.spa.bom.SpaLocalidad;
import com.azeta3.spa.bom.SpaProvincia;
import com.azeta3.spa.bom.SpaProxy;
import com.azeta3.spa.bom.SpaUsuario;
import com.azeta3.spa.dao.CategoriaDAO;
import com.azeta3.spa.dao.LocalidadDAO;
import com.azeta3.spa.dao.ProvinciaDAO;
import com.azeta3.spa.dao.UsuarioDAO;
import com.azeta3.spa.security.Roles;
import com.azeta3.spa.service.AnuncioService;
import com.azeta3.spa.service.BulkDataService;
import com.azeta3.spa.service.ProxyService;

@Service
public class BulkDataServiceImpl implements BulkDataService{
	
	private Logger log = Logger.getLogger(BulkDataServiceImpl.class);
	
	@Autowired
	private ProvinciaDAO provinciaDao;
	
	@Autowired
	private LocalidadDAO localidadDao;
	
	@Autowired
	private AnuncioService anuncioService;
	
	@Autowired
	private CategoriaDAO categoriaDao;
	
	@Autowired
	private UsuarioDAO usuarioDao;
	
	@Autowired
	private ProxyService proxyService;
	
	@Override
	public void cargarProvincias(Resource ficheroCSV) throws IOException {
		log.debug("Iniciando carga de provincias");
		

		if (!provinciaDao.isEmpty()){
			log.debug("Las provincias ya estaban registradas");
			provinciaDao.deleteAllProvincias();
			log.debug("Provincias eliminadas");
		}
	
		InputStreamReader ir = new InputStreamReader(ficheroCSV.getInputStream());
		CSVReader reader = new CSVReader(ir,';');
		String[] aux;
		SpaProvincia provincia = null;
		while ((aux = reader.readNext())!=null){
			log.debug("Insertando la provincia: " + aux[1].toUpperCase());
			provincia = new SpaProvincia(Long.parseLong(aux[0]), aux[1].toUpperCase());
			provinciaDao.createProvincia(provincia);
		}
		reader.close();
		log.debug("PROVINCIAS CARGADAS.....OK");
	}

	@Override
	public void cargarUsuarios(Resource ficheroCSV) throws IOException {
		InputStreamReader ir = new InputStreamReader(ficheroCSV.getInputStream());
		CSVReader reader = new CSVReader(ir,';');
		String[] aux;
		
		while ((aux = reader.readNext())!=null){
			System.out.println("NUMERO USUARIO --> " + aux[0].toUpperCase());
			if(usuarioDao.findById(aux[0].toUpperCase()) == null){
			
				log.debug("Insertando el usuario: " + aux[0].toUpperCase());
				
				SpaUsuario usuario = new SpaUsuario();
				usuario.setEnabled(true);
				usuario.setNombre(aux[0].toUpperCase());
				usuario.setEmail(aux[0].toUpperCase());
				usuario.setPassword(Sha512DigestUtils.shaHex(aux[2].toUpperCase()));
				
				String rolString = aux[1].toUpperCase();
				for(Roles rol : Roles.values()){
					if(rol.getAuthority().equals(rolString)){
						
						Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
						authorities.add((GrantedAuthority)rol);
						
						usuario.setAuthorities(authorities);
					}
				}
				
				usuarioDao.create(usuario);
			}
		}
		reader.close();
		log.debug("USUARIOS CARGADOS.....OK");
	}

	@Override
	public void cargarCategorias(Resource ficheroCSV) throws IOException {
		log.debug("Iniciando carga de categorias");

		if (!categoriaDao.isEmpty()){
			log.debug("Las categorias ya estaban registradas");
			categoriaDao.deleteAllCategorias();
			log.debug("Categorias eliminadas");
		}
	
		InputStreamReader ir = new InputStreamReader(ficheroCSV.getInputStream());
		CSVReader reader = new CSVReader(ir,';');
		String[] aux;

		while ((aux = reader.readNext())!=null){
			log.debug("Insertando la Categoria: " + aux[1].toUpperCase());
			SpaCategoria categoria = new SpaCategoria(new Long(aux[0].toUpperCase()), aux[1],aux[2]);
			categoriaDao.createCategoria(categoria);
		}
		reader.close();
		log.debug("CATEGORIAS CARGADAS.....OK");
		
	}

	@Override
	public void cargarLocalidades(Resource ficheroCSV)
			throws IOException {
		log.debug("Iniciando carga de localidades");
		

		if (localidadDao.isEmpty()){
			log.debug("Las localidades ya estaban registradas");
			localidadDao.deleteAllLocalidades();
			log.debug("Localidades eliminadas");
		}
	
		InputStreamReader ir = new InputStreamReader(ficheroCSV.getInputStream());
		CSVReader reader = new CSVReader(ir,';');
		String[] aux;

		while ((aux = reader.readNext())!=null){
			log.debug("Insertando la localidad: " + aux[1].toUpperCase());
			SpaLocalidad localidad = new SpaLocalidad(aux[1].toUpperCase(), new Long(aux[0].toUpperCase()));
			localidadDao.createLocalidad(localidad);
		}
		reader.close();
		log.debug("PROVINCIAS CARGADAS.....OK");
		
	}

	@Override
	public void cargarDirectorioRaiz(Resource ficheroCSV)
			throws IOException {
		
		if(anuncioService.findAnuncioById(-1L) == null){
			SpaAnuncio anuncio = new SpaAnuncio(
					"<b>Anuncios</b>",
					null,//tituloMundoAnuncio
					null,//Provincia
					null,//Descripcionhtml
					null,//idCategoria
					false,//leaf
					null,//nodopadre
					null);  
			anuncio.setAnunId(0L);
	        anuncioService.nuevoAnuncio(anuncio);
		}
	}
	
	@Override
	public void cargarProxys(Resource ficheroCSV) throws IOException {
		
		log.debug("Iniciando carga de proxys");
		
		List<SpaProxy> proxys = proxyService.findAll();
		
		if(proxys == null || proxys.isEmpty()){
			InputStreamReader ir = new InputStreamReader(ficheroCSV.getInputStream());
			CSVReader reader = new CSVReader(ir,';');
			String[] aux;

			while ((aux = reader.readNext())!=null){
				log.debug("Insertando el proxy: " + aux[0] + ":" + aux[1]);
				
				SpaProxy proxy = new SpaProxy();
				proxy.setConexionesFallidas(0);
				proxy.setHost(aux[0]);
				proxy.setPort(Integer.parseInt(aux[1]));
				proxy.setSocks(Boolean.parseBoolean(aux[2]));
				
				proxyService.create(proxy);
			}
			reader.close();
			log.debug("PROXYS CARGADOS.....OK");
		}
	}

	
}
