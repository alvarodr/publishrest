package com.azeta3.spa.security;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.token.Sha512DigestUtils;

import com.azeta3.spa.bom.SpaUsuario;
import com.azeta3.spa.service.BulkDataService;
import com.azeta3.spa.service.UsuarioService;

public class AuthenticationProviderSPA implements AuthenticationProvider{
	
	public static final String PROVINCIASFILE = "bulkloadResources/provincias.csv";
	public static final String LOCALIDADESFILE = "bulkloadResources/localidades.csv";
	public static final String CATEGORIASFILE = "bulkloadResources/categorias.csv";
	public static final String USUARIOSFILE = "bulkloadResources/usuarios.csv";
	public static final String PROXYSFILE = "bulkloadResources/proxys.csv";
	
	@Autowired
	UsuarioService usuarioService;
	
	@Autowired
	private BulkDataService bulkDataService;
	
	private Logger log = Logger.getLogger(this.getClass());
	
	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
	
		List<SpaUsuario> todosUsuarios = usuarioService.findAll();
		if(todosUsuarios == null || todosUsuarios.size() == 0){
			try {
				bulkDataService.cargarCategorias(new ClassPathResource(CATEGORIASFILE));
				bulkDataService.cargarProvincias(new ClassPathResource(PROVINCIASFILE));
				bulkDataService.cargarUsuarios(new ClassPathResource(USUARIOSFILE));
				bulkDataService.cargarDirectorioRaiz(null);
				bulkDataService.cargarProxys(new ClassPathResource(PROXYSFILE));
			} catch (IOException e) {
			  throw new AuthenticationCredentialsNotFoundException("Error de carga masiva");
			}
		}
	   
	    SpaUsuario usuario = usuarioService.findByEmail((String)authentication.getPrincipal());
	 
	    if(usuario == null){
		    throw new AuthenticationCredentialsNotFoundException("Usuario o contrase�a incorrectos");
	    }
		
		String passwordHash = Sha512DigestUtils.shaHex((String)authentication.getCredentials());

		if(!usuario.getPassword().equalsIgnoreCase(passwordHash)){
	    	throw new AuthenticationCredentialsNotFoundException("Usuario o contrase�a incorrectos");
	    }
		
		if(!usuario.isEnabled()){
			throw new AuthenticationCredentialsNotFoundException("El usuario est� desactivado");
		}
		
		log.debug("############ USUARIO AUTENTICADO " + usuario.getEmail());
		
		Authentication auth = new AuthenticationSPA(usuario, authentication.getDetails());
		auth.setAuthenticated(true);
		
		return auth;
	}

	@Override
	public boolean supports(Class<? extends Object> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
}
