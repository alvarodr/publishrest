package com.azeta3.spa.security;

import static com.azeta3.spa.constantes.Constantes.USUARIOENSESION;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

public class LoginResult implements AuthenticationSuccessHandler, AuthenticationFailureHandler{

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication) throws IOException,
			ServletException {
		response.setContentType("application/json");
		
		Map<String, Object> modelMap = new HashMap<String, Object>();		
		Map<String,String> data = new HashMap<String,String>();
		
		data.put("success",	String.valueOf(true));
		
		modelMap.put("data", data);
		
		OutputStream out = response.getOutputStream();
		ObjectMapper objectMapper = new ObjectMapper();
		
		AuthenticationSPA authSpa = (AuthenticationSPA) authentication;
		
		request.getSession().setAttribute(USUARIOENSESION, authSpa.getUsuario());
		
		objectMapper.writeValue(out, modelMap);
		
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException authentication)
			throws IOException, ServletException {
		response.setContentType("application/json");
		
		Map<String, Object> modelMap = new HashMap<String, Object>();		
		Map<String,String> data = new HashMap<String,String>();
		
		data.put("success",	String.valueOf(false));
		
		modelMap.put("data", data);
		
		OutputStream out = response.getOutputStream();
		ObjectMapper objectMapper = new ObjectMapper();
		
		objectMapper.writeValue(out, modelMap);
		
	}

}
