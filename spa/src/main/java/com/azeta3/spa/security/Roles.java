package com.azeta3.spa.security;

import java.io.Serializable;

import org.springframework.security.core.GrantedAuthority;

public enum Roles implements GrantedAuthority, Serializable{
    ROLE_ADMIN (0),
    ROLE_DEVELOPER (1),
    ROLE_USER (2);
    
    @SuppressWarnings("unused")
	private int bit;
 
    Roles(int bit) {
        this.bit = bit;
    }
 
    public String getAuthority() {
        return toString();
    }
}