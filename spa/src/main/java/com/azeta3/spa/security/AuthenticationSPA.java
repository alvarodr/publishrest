package com.azeta3.spa.security;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import com.azeta3.spa.bom.SpaUsuario;

public class AuthenticationSPA implements Authentication {

	private static final long serialVersionUID = -889323159344957225L;

	private SpaUsuario usuario;
	private Object userDetails;
	private boolean authenticated;

	public AuthenticationSPA(SpaUsuario usuario, Object userDetails) {
		this.usuario = usuario;
		this.userDetails = userDetails;
	}

	@Override
	public String getName() {
		return usuario.getNombre();
	}

	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		return usuario.getAuthorities();
	}

	@Override
	public Object getCredentials() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Object getDetails() {
		return userDetails;
	}

	@Override
	public Object getPrincipal() {
		return usuario.getEmail();
	}

	@Override
	public boolean isAuthenticated() {
		return this.authenticated;
	}

	@Override
	public void setAuthenticated(boolean flag) throws IllegalArgumentException {
		this.authenticated = flag;
	}

	public SpaUsuario getUsuario() {
		return this.usuario;
	}

}
