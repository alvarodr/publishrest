package com.azeta3.spa.bom;

import java.io.Serializable;

import javax.jdo.annotations.Extension;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Version;
import javax.jdo.annotations.VersionStrategy;

@PersistenceCapable
@Version(strategy=VersionStrategy.VERSION_NUMBER, column="version")
@Extension(vendorName="datanucleus", key="key", value="version")
public class SpaProvincia implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@PrimaryKey
	@Persistent
	private Long id;

	@Persistent
	private String nombre;

	public SpaProvincia(){
		super();
	}
	
	public SpaProvincia(Long id, String nombre){
		this.id = id;
		this.nombre = nombre;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
