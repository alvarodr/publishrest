package com.azeta3.spa.bom;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class SpaCategoria implements Serializable{

	private static final long serialVersionUID = 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long id;
	
	@Persistent
	private String urlParaPublicar;
	
	@Persistent
	private String nombreCategoria;

	public SpaCategoria(Long id, String urlParaPublicar,String nombreCategoria) {
		this.id = id;
		this.urlParaPublicar = urlParaPublicar;
		this.nombreCategoria = nombreCategoria;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUrlParaPublicar() {
		return urlParaPublicar;
	}

	public void setUrlParaPublicar(String urlParaPublicar) {
		this.urlParaPublicar = urlParaPublicar;
	}

	public String getNombreCategoria() {
		return nombreCategoria;
	}

	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}
	
}
