package com.azeta3.spa.bom;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import org.springframework.web.multipart.MultipartFile;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class SpaImagen implements Serializable{

	private static final long serialVersionUID = 1L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private java.lang.Long id;
	
	@Persistent
	private java.lang.Long idAnuncio;
	
	@Persistent(serialized="true")
	private byte[] imagen;
	
	@Persistent
	private java.lang.String link;
	
	@Persistent
	private java.lang.String filename;
	
	@NotPersistent
	private MultipartFile imagenMultipart;
	
	public SpaImagen(){
		super();
	}
	
	public SpaImagen(java.lang.Long idAnuncio,
			byte[] imagen,
			java.lang.String link,
			java.lang.String filename){
		this.idAnuncio = idAnuncio;
		this.imagen = imagen;
		this.link = link;
		this.filename = filename;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public byte[] getImagen() {
		return imagen;
	}

	public void setImagen(byte[] imagen) {
		this.imagen = imagen;
	}

	public MultipartFile getImagenMultipart() {
		return imagenMultipart;
	}

	public void setImagenMultipart(MultipartFile imagenMultipart) {
		this.imagenMultipart = imagenMultipart;
	}

	public java.lang.Long getIdAnuncio() {
		return idAnuncio;
	}

	public void setIdAnuncio(java.lang.Long idAnuncio) {
		this.idAnuncio = idAnuncio;
	}

	public java.lang.String getLink() {
		return link;
	}

	public void setLink(java.lang.String link) {
		this.link = link;
	}

	public java.lang.String getFilename() {
		return filename;
	}

	public void setFilename(java.lang.String filename) {
		this.filename = filename;
	}
	
}
