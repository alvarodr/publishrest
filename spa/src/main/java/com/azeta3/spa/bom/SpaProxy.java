package com.azeta3.spa.bom;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class SpaProxy {
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long id;
	
	@Persistent
	private String host;
	
	@Persistent 
	private Integer port;
	
	@Persistent
	private Boolean socks;
	
	@Persistent
	private Integer conexionesFallidas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}

	public Integer getConexionesFallidas() {
		return conexionesFallidas;
	}

	public void setConexionesFallidas(Integer conexionesFallidas) {
		this.conexionesFallidas = conexionesFallidas;
	}

	public Boolean getSocks() {
		return socks;
	}

	public void setSocks(Boolean socks) {
		this.socks = socks;
	}
	
}
