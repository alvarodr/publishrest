package com.azeta3.spa.bom;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.annotations.PersistenceCapable;

import org.springframework.security.core.GrantedAuthority;

import com.azeta3.spa.security.Roles;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class SpaUsuario implements Serializable{

	private static final long serialVersionUID = 8886285618700010093L;
	
	@PrimaryKey
	@Persistent
	private  String email;
	@Persistent
	private String password;
	@Persistent
	private  String nombre;
	@Persistent
	private  String apellidos;
	@Persistent		
	private  Set<String> authorities;
	@Persistent
	private  boolean enabled;
	@Persistent
	private List<Long> idAnuncios;
	
	
	public List<Long> getIdAnuncios() {
		return idAnuncios;
	}
	public void setIdAnuncios(List<Long> idAnuncios) {
		this.idAnuncios = idAnuncios;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	
	public void setAuthorities(Set<GrantedAuthority> authorities) {
		Set<String> newAuthorities = new HashSet<String>();
		
		for(GrantedAuthority authority : authorities){
			newAuthorities.add(authority.getAuthority());
		}
		this.authorities = newAuthorities;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public Set<GrantedAuthority> getAuthorities() {
		
		Set<GrantedAuthority> returnAuthorities = new HashSet<GrantedAuthority>();
		
		for(String rolString : this.authorities){
			for(Roles rol : Roles.values()){
				if(rol.getAuthority().equals(rolString)){
					returnAuthorities.add((GrantedAuthority)rol);
					break;
				}
			}
		}
		
		return returnAuthorities;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
