package com.azeta3.spa.bom;

import java.io.Serializable;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.Element;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class SpaAnuncio implements Serializable{

	private static final long serialVersionUID = -3825213524559953526L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private java.lang.Long anunId;

	@Persistent
	private java.lang.String titulo;
	
	@Persistent
	private java.lang.String tituloMundoanuncio;

	@Persistent
	private java.lang.Integer intervalo;

	@Persistent
	private java.lang.String idUsuarioEmail;
	
	@Persistent
	private java.lang.String emailContacto;
	
	@Persistent //La poblacion no es un combo, y es opcional
	private java.lang.String poblacion;
	
	@Persistent //El barrio no es un combo, y es opcional
	private java.lang.String barrio;
	
	@Persistent //Dato opcional
	private java.lang.String email;
	
	@Persistent
	@Column (jdbcType="CLOB")
	private java.lang.String descripcionHTML;
	
	@Persistent
	private java.util.List<java.lang.Long> idPublicacionesProgramadas;

	@Persistent
	private java.lang.Boolean leaf;
	
	@Persistent
	private java.lang.Long nodoPadre;
	
	@Persistent
	private java.lang.Long idCategoria;
	
	@Persistent
	private java.lang.Long idProvincia;
	
	@Persistent
	@Element(types=java.lang.Long.class)
	private java.util.List<java.lang.Long> idsImagenes;
	
	public SpaAnuncio(java.lang.String titulo, java.lang.String tituloMundoanuncio, 
			java.lang.Long idProvincia, java.lang.String descipcionHTML, 
			java.lang.Long idCategoria, java.lang.Boolean leaf,
			java.lang.Long nodoPadre, java.lang.String idUsuarioEmail){
		this.titulo = titulo;
		this.tituloMundoanuncio = tituloMundoanuncio;
		this.idProvincia = idProvincia;
		this.descripcionHTML = descipcionHTML;
		this.idCategoria = idCategoria;
		this.leaf = leaf;
		this.nodoPadre = nodoPadre;
		this.idUsuarioEmail = idUsuarioEmail;
	}
	
	public java.lang.String getEmailContacto() {
		return emailContacto;
	}

	public void setEmailContacto(java.lang.String emailContacto) {
		this.emailContacto = emailContacto;
	}

	public java.lang.Boolean getLeaf() {
		return leaf;
	}
	
	public void setLeaf(java.lang.Boolean leaf) {
		this.leaf = leaf;
	}

	public java.lang.Long getAnunId() {
		return anunId;
	}

	public void setAnunId(java.lang.Long anunId) {
		this.anunId = anunId;
	}

	public java.lang.String getTitulo() {
		return titulo;
	}

	public void setTitulo(java.lang.String titulo) {
		this.titulo = titulo;
	}

	public java.lang.Integer getIntervalo() {
		return intervalo;
	}

	public void setIntervalo(java.lang.Integer intervalo) {
		this.intervalo = intervalo;
	}

	public java.lang.String getPoblacion() {
		return poblacion;
	}

	public void setPoblacion(java.lang.String poblacion) {
		this.poblacion = poblacion;
	}

	public java.lang.String getBarrio() {
		return barrio;
	}

	public void setBarrio(java.lang.String barrio) {
		this.barrio = barrio;
	}

	public java.lang.String getEmail() {
		return email;
	}

	public void setEmail(java.lang.String email) {
		this.email = email;
	}

	public java.lang.String getDescripcionHTML() {
		return descripcionHTML;
	}

	public void setDescripcionHTML(java.lang.String descripcionHTML) {
		this.descripcionHTML = descripcionHTML;
	}

	public java.lang.Long getNodoPadre() {
		return nodoPadre;
	}

	public void setNodoPadre(java.lang.Long nodoPadre) {
		this.nodoPadre = nodoPadre;
	}

	public java.lang.String getIdUsuarioEmail() {
		return idUsuarioEmail;
	}

	public void setIdUsuarioEmail(java.lang.String idUsuarioEmail) {
		this.idUsuarioEmail = idUsuarioEmail;
	}

	public java.util.List<java.lang.Long> getIdPublicacionesProgramadas() {
		return idPublicacionesProgramadas;
	}

	public void setIdPublicacionesProgramadas(java.util.List<java.lang.Long> idPublicacionesProgramadas) {
		this.idPublicacionesProgramadas = idPublicacionesProgramadas;
	}
	public java.lang.String getTituloMundoanuncio() {
		return tituloMundoanuncio;
	}
	public void setTituloMundoanuncio(java.lang.String tituloMundoanuncio) {
		this.tituloMundoanuncio = tituloMundoanuncio;
	}

	public java.lang.Long getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(java.lang.Long idCategoria) {
		this.idCategoria = idCategoria;
	}

	public java.lang.Long getIdProvincia() {
		return idProvincia;
	}

	public void setIdProvincia(java.lang.Long idProvincia) {
		this.idProvincia = idProvincia;
	}

	public java.util.List<java.lang.Long> getIdsImagenes() {
		return idsImagenes;
	}

	public void setIdsImagenes(java.util.List<java.lang.Long> idsImagenes) {
		this.idsImagenes = idsImagenes;
	}
	
}
