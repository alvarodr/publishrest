package com.azeta3.spa.bom;

import java.io.Serializable;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.azeta3.spa.constantes.DiasSemanaEnum;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class SpaPublicacionProgramada implements Serializable{
	
	private static final long serialVersionUID = -5922078159696795735L;

	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private java.lang.Long id;
	
	@Persistent
	private java.lang.String idUsuario;
	
	@Persistent
	private java.util.Date horaMindesde;
	
	@Persistent
	private java.util.Date horaMinHasta;
	
	@Persistent
	private java.lang.Integer intervaloMin;
	
	@Persistent
	private java.lang.String diaSemana; //Los Enum no persisten, ver el metodo get y set
	                          //de este atributo	
	@Persistent
	private java.util.List<java.lang.Long> idsPublicacionesEfectivas;

	/**
	 * 
	 * @param horaMindesde
	 * @param horaMinHasta
	 * @param intervaloMin
	 * @param diaSemana
	 */
	public SpaPublicacionProgramada(java.util.Date horaMindesde, java.util.Date horaMinHasta, 
			java.lang.Integer intervaloMin, com.azeta3.spa.constantes.DiasSemanaEnum diaSemana){
		this.horaMindesde = horaMindesde;
		this.horaMinHasta = horaMinHasta;
		this.intervaloMin = intervaloMin;
		this.diaSemana = diaSemana.getDia();
	}
	
	public java.lang.Long getId() {
		return id;
	}

	public void setId(java.lang.Long id) {
		this.id = id;
	}
	
	public java.util.Date getHoraMindesde() {
		return horaMindesde;
	}

	public void setHoraMindesde(java.util.Date horaMindesde) {
		this.horaMindesde = horaMindesde;
	}

	public java.util.Date getHoraMinHasta() {
		return horaMinHasta;
	}

	public void setHoraMinHasta(java.util.Date horaMinHasta) {
		this.horaMinHasta = horaMinHasta;
	}

	public java.lang.Integer getIntervaloMin() {
		return intervaloMin;
	}

	public void setIntervaloMin(java.lang.Integer intervaloMin) {
		this.intervaloMin = intervaloMin;
	}

	public com.azeta3.spa.constantes.DiasSemanaEnum getDiaSemana() {
		for(DiasSemanaEnum dia : DiasSemanaEnum.values()){
			if(dia.getDia().equals(this.diaSemana)){
				return dia;
			}
		}
		return null;
	}

	public void setDiaSemana(com.azeta3.spa.constantes.DiasSemanaEnum diaSemana) {
		this.diaSemana = diaSemana.getDia();
	}

	public java.util.List<java.lang.Long> getIdsPublicacionesEfectivas() {
		return idsPublicacionesEfectivas;
	}

	public void setIdsPublicacionesEfectivas(java.util.List<java.lang.Long> idsPublicacionesEfectivas) {
		this.idsPublicacionesEfectivas = idsPublicacionesEfectivas;
	}

	public java.lang.String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(java.lang.String idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	
}
