package com.azeta3.spa.tasks;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.azeta3.spa.bom.SpaAnuncio;
import com.azeta3.spa.bom.SpaPublicacionEfectiva;
import com.azeta3.spa.service.PublicaService;
import com.azeta3.spa.service.PublicacionEfectivaService;

public class CrearPublicacionTask implements Runnable{

	private SpaAnuncio anuncioPublicar;
	private SpaPublicacionEfectiva publicacionEfectiva;
	
	private PublicaService publicaService;
	
	private PublicacionEfectivaService publicacionEfectivaService;
	
	public CrearPublicacionTask(PublicaService publicaService, PublicacionEfectivaService publicacionEfectivaService) {
		this.publicaService = publicaService;
		this.publicacionEfectivaService = publicacionEfectivaService;
	}
	
	Logger log = Logger.getLogger(this.getClass());
	
	@Override
	public void run() {
 	   
 	   Boolean publicado = false;
 	   Integer intentos = 0;
 	   while(!publicado && intentos < 5){
     	   try{
     		   publicado = publicaService.publicarAnuncioMundoAnuncio(anuncioPublicar);
     		   intentos ++;
     	   }
     	   catch (Exception e) {
     		   intentos ++;
     		   log.debug("Error al publicar ->" + e.getMessage());
     	   }
 	   }
 	   SpaPublicacionEfectiva pubefec = new SpaPublicacionEfectiva();
 	   pubefec.setIdAnuncio(anuncioPublicar.getAnunId());
 		Date fecha = Calendar.getInstance(TimeZone.getTimeZone("Europe/Madrid")).getTime();
 		pubefec.setFechaPublicacion(fecha);
 		pubefec.setIdPublicacionProgramada(publicacionEfectiva.getId());
 		pubefec.setIdUsuario(anuncioPublicar.getIdUsuarioEmail());
 		 
 	   if(!publicado){
 		   pubefec.setEstado(false);
 	   }
 	   else{
 		   pubefec.setEstado(true);
 	   }
 	   
 	   publicacionEfectivaService.create(pubefec);
 	   
 	   if(publicacionEfectivaService.findByAnuncio(anuncioPublicar.getAnunId()).size() >= 10){
 		   SpaPublicacionEfectiva p = publicacionEfectivaService.findMasAntigua(pubefec.getIdAnuncio());
 	 	   publicacionEfectivaService.remove(p.getId());
 	   }
 	   
    }

	public SpaAnuncio getAnuncioPublicar() {
		return anuncioPublicar;
	}

	public void setAnuncioPublicar(SpaAnuncio anuncioPublicar) {
		this.anuncioPublicar = anuncioPublicar;
	}

	public SpaPublicacionEfectiva getPublicacionEfectiva() {
		return publicacionEfectiva;
	}

	public void setPublicacionEfectiva(
			SpaPublicacionEfectiva publicacionEfectiva) {
		this.publicacionEfectiva = publicacionEfectiva;
	}
}