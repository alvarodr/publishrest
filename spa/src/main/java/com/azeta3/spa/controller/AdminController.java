package com.azeta3.spa.controller;

import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.token.Sha512DigestUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.azeta3.spa.bom.SpaUsuario;
import com.azeta3.spa.security.Roles;
import com.azeta3.spa.service.AdminPanelService;
import com.azeta3.spa.service.UsuarioService;

@Controller
public class AdminController {

	private static final Logger log = Logger.getLogger(AdminController.class);
	public static final String CONTROLLER_MAPPING = "/admin.do";
	public static final String CONTROLLER_GETUSUARIOS = "/admin/getUsuarios.do";
	public static final String CONTROLLER_NUEVOUSUARIO = "/admin/nuevoUsuario.do";
	public static final String CONTROLLER_ELIMINARUSUARIO = "/admin/eliminarUsuario.do";
	public static final String CONTROLLER_CAMBIARPASSWORD = "/admin/cambiarPassword.do";
	public static final String CONTROLLER_CAMBIARACTIVO = "/admin/cambiarActivo.do";
	
	@Autowired
	private AdminPanelService adminPanelService;
	
	@Autowired
	private UsuarioService usuarioService;
	
	@RequestMapping(value=CONTROLLER_MAPPING, method=RequestMethod.GET)
	public ModelAndView admin(HttpServletRequest request, HttpServletResponse response) {
		return new ModelAndView("admin");
	}
	
	@RequestMapping(value=CONTROLLER_GETUSUARIOS, method=RequestMethod.GET)
	public ModelAndView getUsuarios(HttpServletRequest request, HttpServletResponse response) {
		
		ModelAndView mvc = new ModelAndView();
		mvc.setViewName("json");
		
		mvc.addObject("items", adminPanelService.getUsuariosJSON());
		
		return mvc;
	}
	
	@RequestMapping(value=CONTROLLER_ELIMINARUSUARIO, method=RequestMethod.POST)
	public ModelAndView eliminarUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletRequestBindingException {
		
		String email = ServletRequestUtils.getStringParameter(request, "email");
		
		SpaUsuario usuario = new SpaUsuario();
		usuario.setEmail(email);
		
		usuarioService.removeUser(email);
		
		ModelAndView mvc = new ModelAndView("json");
		mvc.addObject("success", "ok");
		
		return mvc;
	}
	
	@RequestMapping(value=CONTROLLER_NUEVOUSUARIO, method=RequestMethod.POST)
	public ModelAndView nuevoUsuario(HttpServletRequest request, HttpServletResponse response) throws ServletRequestBindingException {
		
		String email = ServletRequestUtils.getStringParameter(request, "usuario");
		String password = ServletRequestUtils.getStringParameter(request, "password");
		
		SpaUsuario usuario = new SpaUsuario();
		usuario.setEmail(email);
		HashSet<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		authorities.add((GrantedAuthority)Roles.ROLE_USER);
		usuario.setAuthorities(authorities);
		usuario.setEnabled(true);
		usuario.setPassword(Sha512DigestUtils.shaHex(password));
		
		usuarioService.createUser(usuario);
		
		ModelAndView mvc = new ModelAndView("json");
		mvc.addObject("success", "ok");
		
		return mvc;
	}
	
	@RequestMapping(value=CONTROLLER_CAMBIARPASSWORD, method=RequestMethod.POST)
	public ModelAndView cambiarPassword(HttpServletRequest request, HttpServletResponse response) throws ServletRequestBindingException {
		
		String email = ServletRequestUtils.getStringParameter(request, "email");
		String password = ServletRequestUtils.getStringParameter(request, "password");
		
		SpaUsuario usuario = usuarioService.findByEmail(email);
		
		usuario.setPassword(Sha512DigestUtils.shaHex(password));
		
		usuarioService.update(usuario);
		
		ModelAndView mvc = new ModelAndView("json");
		mvc.addObject("success", "ok");
		
		return mvc;
	}
	
	@RequestMapping(value=CONTROLLER_CAMBIARACTIVO, method=RequestMethod.POST)
	public ModelAndView cambiarActivo(HttpServletRequest request, HttpServletResponse response) throws ServletRequestBindingException {
		
		String email = ServletRequestUtils.getStringParameter(request, "email");
		Boolean activo = Boolean.valueOf(ServletRequestUtils.getStringParameter(request, "activo"));
		
		SpaUsuario usuario = usuarioService.findByEmail(email);
		usuario.setEnabled(activo);
		
		usuarioService.update(usuario);
		
		ModelAndView mvc = new ModelAndView("json");
		mvc.addObject("success", "ok");
		
		return mvc;
	}
}
