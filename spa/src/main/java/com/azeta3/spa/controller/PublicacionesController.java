package com.azeta3.spa.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.azeta3.spa.bom.SpaUsuario;
import com.azeta3.spa.service.AgendaService;

import static com.azeta3.spa.constantes.Constantes.*;

@Controller
public class PublicacionesController {

	private static final Logger log = Logger.getLogger(PublicacionesController.class);
	
	@Autowired
	AgendaService agendaService;
	
	public static final String CONTROLLER_MAPPING = "/publicacionesProgramadas.do";
	
	@RequestMapping(value=CONTROLLER_MAPPING, method=RequestMethod.GET)
	public ModelAndView example(HttpServletRequest request,
	        HttpServletResponse response) throws Exception {
		
		SpaUsuario usuario = (SpaUsuario) request.getSession().getAttribute(USUARIOENSESION);
		
		ModelAndView mvc = new ModelAndView();
		mvc.addObject("text", ".");
		mvc.addObject("children",agendaService.generarAgendaJSON(usuario));
		mvc.setViewName("json");
		return mvc;
	}
}
