package com.azeta3.spa.controller;

import static com.azeta3.spa.constantes.Constantes.USUARIOENSESION;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.azeta3.spa.bom.SpaUsuario;
import com.azeta3.spa.service.LogService;

@Controller
public class LogController {

	public static final String CONTROLLER_MAPPING = "/log.do";
	public static final Logger log = Logger.getLogger(LogController.class);

	@Autowired
	private LogService logService;
	
	@RequestMapping(value=CONTROLLER_MAPPING, method=RequestMethod.GET)
	public ModelAndView log(HttpServletRequest request,
	        HttpServletResponse response) throws Exception {
		
		ModelAndView mvc = new ModelAndView();
		
		//try{
			Integer start = ServletRequestUtils.getIntParameter(request, "start");
			Integer limit = ServletRequestUtils.getIntParameter(request, "limit");
			
			SpaUsuario usuario = (SpaUsuario) request.getSession().getAttribute(USUARIOENSESION);
			
			List<Map<String,Object>> json = logService.getLogJSON(usuario.getEmail(), start, limit);
			mvc.addObject("totalCount", logService.getTotalLog(usuario.getEmail(), start, limit));
			mvc.addObject("items", json);
			
			mvc.setViewName("json");			
		
		return mvc;
	}
}
