package com.azeta3.spa.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.azeta3.spa.bom.SpaAnuncio;
import com.azeta3.spa.bom.SpaImagen;
import com.azeta3.spa.service.AnuncioService;
import com.azeta3.spa.service.ImagenService;

@Controller
public class ImagenController {
	
	@Autowired
	private ImagenService imagenService;
	
	@Autowired
	private AnuncioService anuncioService;
	
	private static final Logger log = Logger.getLogger(ImagenController.class);
	public static final String CONTROLLER_MAPPING = "/uploadImage.do";
	public static final String CONTROLLER_VIEWIMAGE = "/viewImage.do";
	
	@RequestMapping(value = CONTROLLER_MAPPING, method = RequestMethod.POST)
	public ModelAndView uploadImage(SpaImagen imagen,
			HttpServletRequest request, HttpServletResponse response) 
					throws IOException, ServletRequestBindingException{
		
		//ASI SE RECOGEN PARAMETROS
		String campo1 = ServletRequestUtils.getStringParameter(request, "campo1");
		
		//Se crea la imagen
		byte[] imagenByteArray = imagen.getImagenMultipart().getBytes();
		imagen.setImagen(imagenByteArray);
		imagen.setIdAnuncio(ServletRequestUtils.getLongParameter(request, "idAnuncio"));
		imagen = imagenService.create(imagen);
		
		//Cuidado! Aqu� debe venir el idAnuncio para recuperarlo y poder
		//a�adirle la imagen
		String idAnuncio = ServletRequestUtils.getStringParameter(request, "idAnuncio");
		SpaAnuncio anuncio = anuncioService.findAnuncioById(new Long(idAnuncio));
		
		anuncio.getIdsImagenes().add(imagen.getId());
		
		//Actualizo el anuncio
		anuncioService.update(anuncio);
		
		return new ModelAndView("json");
    }
	
	
	@RequestMapping(value = CONTROLLER_VIEWIMAGE, method = RequestMethod.GET)
	public ModelAndView viewImage(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletRequestBindingException {
		
		String idImagen = ServletRequestUtils.getStringParameter(request, "idImagen");
		
		SpaImagen imagen = imagenService.findById(new Long(idImagen));
		
		log.debug(imagen.getImagen());
		
		response.setHeader("Content-Type", "image/jpeg");
		response.setHeader("Content-Disposition", "inline; filename=\"imagen.jpg\"");
		response.setHeader("Content-Transfer-Encoding", "binary");

		response.getOutputStream().write(imagen.getImagen());
		
		return null;
    }
}
