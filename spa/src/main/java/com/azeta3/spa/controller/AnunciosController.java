package com.azeta3.spa.controller;

import static com.azeta3.spa.constantes.Constantes.USUARIOENSESION;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONSerializer;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.azeta3.spa.bom.SpaAnuncio;
import com.azeta3.spa.bom.SpaImagen;
import com.azeta3.spa.bom.SpaPublicacionProgramada;
import com.azeta3.spa.bom.SpaUsuario;
import com.azeta3.spa.constantes.DiasSemanaEnum;
import com.azeta3.spa.dto.SpaCombo;
import com.azeta3.spa.dto.SpaImagenDto;
import com.azeta3.spa.dto.SpaTree;
import com.azeta3.spa.service.AnuncioService;
import com.azeta3.spa.service.CategoriaService;
import com.azeta3.spa.service.ImagenService;
import com.azeta3.spa.service.ProvinciaService;
import com.azeta3.spa.service.PublicacionesProgramadasService;
import com.azeta3.spa.utils.ExtUtils;
import com.azeta3.spa.utils.HttpUtils;

@Controller
public class AnunciosController {
	public static final String CONTROLLER_MAPPING_NUEVO = "/newFolder.do";
	public static final String CONTROLLER_MAPPING_REMOVE_DIR = "/delFolder.do";
	public static final String CONTROLLER_MAPPING_NUEVO_ANUNCIO = "/nuevoAnuncio.do";
	public static final String CONTROLLER_MAPPING_UPDATE_ANUNCIO = "/updateAnuncio.do";
	public static final String CONTROLLER_MAPPING_TREE = "/getArbol.do";
	public static final String CONTROLLER_MAPPING_PROV = "/getProv.do";
	public static final String CONTROLLER_MAPPING_CAT = "/getCategorias.do";
	public static final String CONTROLLER_MAPPING_FORM_AG = "/loadFormAg.do";
	public static final String CONTROLLER_MAPPING_FORM_MA = "/loadFormMa.do";	
	
	private static final Logger log = Logger.getLogger(AnunciosController.class);
	
	private String[] diasSemana = {"Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"};
	
	@Autowired
	private AnuncioService anuncioService;
	@Autowired
	private ProvinciaService provinciaService;
	@Autowired
	private PublicacionesProgramadasService publicacionesProgramadasService;
	@Autowired
	private CategoriaService categoriaService;
	@Autowired
	private ImagenService imagenService;

	@RequestMapping(value = CONTROLLER_MAPPING_CAT, method=RequestMethod.GET)
	public ModelAndView getCategorias(HttpServletRequest request, HttpServletResponse response){
		try{
			String query = "";
			try{
				query = ServletRequestUtils.getStringParameter(request, "query");
			}catch (Exception e) {
				// TODO: handle exception
				query = "";
			}
			List<SpaCombo> comboCategorias = categoriaService.getCategorias(query);
			Map<String, Object> items = new HashMap<String, Object>();
			items.put("items", comboCategorias);
			items.put("totalCount", comboCategorias.size());
			return new ModelAndView("json", items);
		}catch (Exception e) {
			// TODO: handle exception
			log.error("AnunciosController.getCategorias.ERROR", e);
			return null;
		}
	}

	@RequestMapping(value = CONTROLLER_MAPPING_PROV, method = RequestMethod.GET)
	public ModelAndView getProv(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> items = new HashMap<String, Object>();
		List<SpaCombo> lista = null;
		try{
			String query = "";
			try{
				query = ServletRequestUtils.getStringParameter(request, "query");
			}catch (Exception e) {
				// TODO: handle exception
				query = "";
			}
			lista = provinciaService.getProvsByQuery(query);
			items.put("items", lista);
			items.put("totalCount", lista.size());
		}catch (Exception e) {
			// TODO: handle exception
			log.error("Error al devolver las provincias", e);
		}
		return new ModelAndView("json", items);
	}
	
	
	@RequestMapping(value=CONTROLLER_MAPPING_NUEVO, method=RequestMethod.POST)
	public ModelAndView Index(HttpServletRequest request, HttpServletResponse response){
		String dev = "";
		try {
			String titulo = ServletRequestUtils.getStringParameter(request, "titulo");
			Boolean leaf = ServletRequestUtils.getBooleanParameter(request, "leaf");
			Long idPadre = ServletRequestUtils.getLongParameter(request, "padre");
			
			SpaAnuncio anuncio = new SpaAnuncio(
					titulo,
					null,//tituloMundoanuncio
					null,//provincia
					null,//descripcion
					null,//categoria
					leaf,
					idPadre,
					((SpaUsuario) request.getSession().getAttribute(USUARIOENSESION)).getEmail());
			
			anuncio.setIdPublicacionesProgramadas(new ArrayList<Long>());
			anuncio.setIdsImagenes(new ArrayList<Long>());
			
			anuncioService.nuevoAnuncio(anuncio);
			if (!leaf)
				dev = ExtUtils.successJSON(true, "Directorio creado correctamente");
			else
				dev = ExtUtils.successJSON(true, "Anuncio creado correctamente");
		} catch (ServletRequestBindingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			dev = ExtUtils.successJSON(false, "Se ha producido un error interno");
		} 
		return ExtUtils.createJSONMaV(dev);
	}
	
	@JsonSerialize
	@SuppressWarnings("unchecked")
	@RequestMapping(value=CONTROLLER_MAPPING_NUEVO_ANUNCIO, method=RequestMethod.POST)
	public ModelAndView NuevoAnuncio(SpaImagenDto imagenes, HttpServletRequest request, HttpServletResponse response)throws IOException{
		log.debug("AnunciosController.NuevoAnuncio.Iniciando...");
		Map<String, String> datos = HttpUtils.getParamsMap(request);
		List<Long> lPubProg = new ArrayList<Long>(); 
		try{
			//SpaAnuncio anuncio = new SpaAnuncio();
			//Long id = ServletRequestUtils.getLongParameter(request, "id");
			Long idPadre = ServletRequestUtils.getLongParameter(request, "idPadre");
			String titulo = ServletRequestUtils.getStringParameter(request, "titulo");
			String tituloMA = ServletRequestUtils.getStringParameter(request, "titleMunAnun");
			Long categoria = ServletRequestUtils.getLongParameter(request, "categoria");
			Long provincia = ServletRequestUtils.getLongParameter(request, "provincia");
			String emailContacto = ServletRequestUtils.getStringParameter(request, "emailContacto");
			//Long pais = ServletRequestUtils.getLongParameter(request, "pais");
			String description = ServletRequestUtils.getStringParameter(request, "descripcion");
			
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			for (int i = 0; i < diasSemana.length; i++) {
				if (datos.containsKey("c" + diasSemana[i])){
					String dia = "i" + diasSemana[i];
					
					SpaPublicacionProgramada pub = new SpaPublicacionProgramada(
							sdf.parse(datos.get("d" + diasSemana[i])), //desde
							sdf.parse(datos.get("h" + diasSemana[i])), //hasta
							Integer.parseInt((String)(datos.get(dia))),//intervalo min
							DiasSemanaEnum.values()[i]);//dia semana
					
					pub.setIdUsuario(((SpaUsuario) request.getSession().getAttribute(USUARIOENSESION)).getEmail());
					
					lPubProg.add(publicacionesProgramadasService.guardaPublicacion(pub));
				}
			}
			SpaAnuncio anuncio = new SpaAnuncio(
					titulo, //Titulo
					tituloMA, //Titulo MundoAnuncio
					provincia,//idProvincia
					description,//descripcionHTML
					categoria,//idCategoria
					true,//leaf
					idPadre,//NodoPadre
					((SpaUsuario) request.getSession().getAttribute(USUARIOENSESION)).getEmail());
			
			anuncio.setIdPublicacionesProgramadas(lPubProg);
			anuncio.setEmailContacto(emailContacto);
			
			//Recuperando imagenes
			List<SpaImagen> listaImagenes = new ArrayList<SpaImagen>();
			if (imagenes.getImagen1().getSize() > 0){
				byte[] imagenByteArray = imagenes.getImagen1().getBytes();
				listaImagenes.add(new SpaImagen(anuncio.getAnunId(), imagenByteArray, imagenes.getLink1(), imagenes.getImagen1().getOriginalFilename()));
			}
			if (imagenes.getImagen2().getSize() > 0){
				byte[] imagenByteArray = imagenes.getImagen2().getBytes();
				listaImagenes.add(new SpaImagen(anuncio.getAnunId(), imagenByteArray, imagenes.getLink2(), imagenes.getImagen2().getOriginalFilename()));
			}
			if (imagenes.getImagen3().getSize() > 0){
				byte[] imagenByteArray = imagenes.getImagen3().getBytes();
				listaImagenes.add(new SpaImagen(anuncio.getAnunId(), imagenByteArray, imagenes.getLink3(), imagenes.getImagen3().getOriginalFilename()));
			}
			if (imagenes.getImagen4().getSize() > 0){
				byte[] imagenByteArray = imagenes.getImagen4().getBytes();
				listaImagenes.add(new SpaImagen(anuncio.getAnunId(), imagenByteArray, imagenes.getLink4(), imagenes.getImagen4().getOriginalFilename()));
			}
			if (imagenes.getImagen5().getSize() > 0){
				byte[] imagenByteArray = imagenes.getImagen5().getBytes();
				listaImagenes.add(new SpaImagen(anuncio.getAnunId(), imagenByteArray, imagenes.getLink5(), imagenes.getImagen5().getOriginalFilename()));
			}
			if (imagenes.getImagen6().getSize() > 0){
				byte[] imagenByteArray = imagenes.getImagen6().getBytes();
				listaImagenes.add(new SpaImagen(anuncio.getAnunId(), imagenByteArray, imagenes.getLink6(), imagenes.getImagen6().getOriginalFilename()));
			}
			
			SpaImagen imagen = null;
			anuncio.setIdsImagenes(new ArrayList<Long>());
			for(SpaImagen img : listaImagenes){
				imagen = imagenService.create(img);
				anuncio.getIdsImagenes().add(imagen.getId());
			}

			SpaAnuncio anuncioCreated = anuncioService.nuevoAnuncio(anuncio);	
			for(SpaImagen img : listaImagenes){
				SpaImagen imagenCreated = imagenService.findById(img.getId());
				imagenCreated.setIdAnuncio(anuncioCreated.getAnunId());
				imagenService.update(imagenCreated);
			}
			
		}catch (Exception e) {
			// TODO: handle exception
			response.setContentType("text/html");					
			response.getWriter().write(JSONSerializer.toJSON(ExtUtils.composeJSOToMap(false, "Se ha producido un error al crear el anuncio")).toString());
			log.error("Error al almacenar el Anuncio",e);
			return null;
		}
		response.setContentType("text/html");					
		response.getWriter().write(JSONSerializer.toJSON(ExtUtils.composeJSOToMap(true, "El anuncio se ha creado correctamente")).toString());
		return null;
	}
	
	@RequestMapping(value=CONTROLLER_MAPPING_REMOVE_DIR, method=RequestMethod.POST)
	public ModelAndView RemoveFolder(HttpServletRequest request, HttpServletResponse response){
		SpaUsuario usuario = (SpaUsuario) request.getSession().getAttribute(USUARIOENSESION);
		log.debug("AnunciosController.RemoveFolder.Iniciando...");
		String dev = "";
		try{
			Long id = ServletRequestUtils.getLongParameter(request, "id");
			SpaAnuncio anuncio = anuncioService.findAnuncioById(id);
			log.debug("AnunciosController.RemoveFolder.Directorio recuperado: " + anuncio);
			anuncioService.eliminaAnuncio(anuncio,usuario.getEmail());
			log.debug("AnunciosController.RemoveFolder.Directorio eliminado correctamente");
			dev = ExtUtils.successJSON(true, "El directorio se ha guardado correctamente");
		}catch (Exception e) {
			// TODO: handle exception
			log.error("AnunciosController.RemoveFolder.ERROR ", e);
			dev = ExtUtils.successJSON(false, "Se ha producido un error interno");
		}
		return ExtUtils.createJSONMaV(dev);
	}
	
	@RequestMapping(value=CONTROLLER_MAPPING_TREE, method=RequestMethod.GET)
	public ModelAndView Arbol(HttpServletRequest request, HttpServletResponse response) {
		SpaUsuario usuario = (SpaUsuario) request.getSession().getAttribute(USUARIOENSESION);
		
		//Definici�n de la ra�z del arbol
		SpaTree arbol = new SpaTree();
		arbol.setTitulo(".");
		arbol.setLeaf(false);
		
		SpaAnuncio anuncio = anuncioService.findAnuncioById(-1L);
		
		SpaTree tree = new SpaTree();
		
		List<SpaTree> listaTree = new ArrayList<SpaTree>();
		listaTree.add(generaArbol(anuncio, tree, usuario.getEmail()));
		
		arbol.setChildren(listaTree);
		
		ObjectMapper om = new ObjectMapper();
		
		try {
			om.writeValue(response.getOutputStream(), arbol);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			log.error("Error al generar el JSON",e);
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			log.error("Error al mapear el JSON", e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error("Error general", e);
		}
		
		return null;
	}
	
	
	private SpaTree generaArbol(SpaAnuncio anuncio, SpaTree arbol, String idUsuarioEmail){
		List<SpaAnuncio> anuncios = anuncioService.findByPadre(anuncio.getAnunId().toString(), idUsuarioEmail);
		List<SpaTree> aux = new ArrayList<SpaTree>();
		if (anuncio.getNodoPadre() == null){
			arbol.setId(-1L);
			arbol.setLeaf(false);
			arbol.setTitulo(anuncio.getTitulo());
			if (anuncios.size() > 0){
				arbol.setExpanded(true);
				aux = new ArrayList<SpaTree>();
				for (Iterator<SpaAnuncio> iterator = anuncios.iterator(); iterator.hasNext();){
					SpaTree tree = new SpaTree();
					SpaAnuncio spaAnuncio = (SpaAnuncio) iterator.next();
					aux.add(generaArbol(spaAnuncio, tree, idUsuarioEmail));
				}
			}else{
				arbol.setExpanded(false);
			}
		}else if (anuncio.getNodoPadre() != null){
			arbol.setId(anuncio.getAnunId());
			arbol.setLeaf(anuncio.getLeaf());
			if (anuncio.getLeaf()) arbol.setIconCls("file");
			arbol.setTitulo(anuncio.getTitulo());
			if (anuncios.size() > 0){
				arbol.setExpanded(true);
				aux = new ArrayList<SpaTree>();
				for (Iterator<SpaAnuncio> iterator = anuncios.iterator(); iterator.hasNext();){
					SpaTree tree = new SpaTree();
					SpaAnuncio spaAnuncio = (SpaAnuncio) iterator.next();
					aux.add(generaArbol(spaAnuncio, tree, idUsuarioEmail));
				}				
			}else{
				arbol.setExpanded(false);
			}
		}
		
		arbol.setChildren(aux);
		return arbol;
	}
	
	@RequestMapping(value=CONTROLLER_MAPPING_FORM_MA, method=RequestMethod.POST)
	public ModelAndView loadFormMa(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> data = new HashMap<String, Object>();
		try{
			Long id = ServletRequestUtils.getLongParameter(request, "id");
			SpaAnuncio anuncio = anuncioService.findAnuncioById(id);
			data.put("titleMunAnun", anuncio.getTituloMundoanuncio());
			data.put("categoria", anuncio.getIdCategoria());
			data.put("provincia", anuncio.getIdProvincia());
			data.put("descripcion", anuncio.getDescripcionHTML());
			data.put("idPadre", anuncio.getNodoPadre());
			data.put("emailContacto", anuncio.getEmailContacto());
			List<Long> idsImages = anuncio.getIdsImagenes();
			Integer contador = new Integer(1);
			if (idsImages != null && idsImages.size()>0){
				SpaImagen imagen = null;
				for (Long image : idsImages){
					imagen = imagenService.findById(image);
					data.put("link"+contador, imagen.getLink());
					data.put("idImagen"+contador, imagen.getId());
					data.put("imagen"+contador, imagen.getFilename());
					contador++;
				}				
			}
		}catch (Exception e) {
			// TODO: handle exception
			log.error("AnunciosController.loadFormAg.ERROR", e);
			return ExtUtils.successForm(false, "Error al recuperar los datos del anuncio");
		}
		return ExtUtils.composeJSONMaV(true, data);
	}
	
	@RequestMapping(value=CONTROLLER_MAPPING_FORM_AG, method=RequestMethod.POST)
	public ModelAndView loadFormAg(HttpServletRequest request, HttpServletResponse response){
		Map<String, Object> data = new HashMap<String, Object>();
		try{
			Long id = ServletRequestUtils.getLongParameter(request, "id");
			SpaAnuncio anuncio = anuncioService.findAnuncioById(id);
			List<Long> listaPubProg = anuncio.getIdPublicacionesProgramadas();
			SpaPublicacionProgramada pubProg = null;
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			if (listaPubProg != null){
				for(Long publicacion : listaPubProg){
					pubProg = publicacionesProgramadasService.findById(publicacion);
					data.put("c"+diasSemana[pubProg.getDiaSemana().getPositionDia()],new Boolean(true));
					data.put("i"+diasSemana[pubProg.getDiaSemana().getPositionDia()],pubProg.getIntervaloMin());
					data.put("d"+diasSemana[pubProg.getDiaSemana().getPositionDia()],sdf.format(pubProg.getHoraMindesde()));
					data.put("h"+diasSemana[pubProg.getDiaSemana().getPositionDia()],sdf.format(pubProg.getHoraMinHasta()));
				}				
			}
			data.put("titulo", anuncio.getTitulo());
			data.put("idPadre", anuncio.getNodoPadre());
		}catch (Exception e) {
			// TODO: handle exception
			log.error("AnunciosController.loadFormAg.ERROR", e);
			return ExtUtils.successForm(false, "Error al recuperar los datos del anuncio");
		}
		return ExtUtils.composeJSONMaV(true, data);
	}
	
	/**
	 * #####################   MODIFICAR ANUNCIO   ###########
	 * @param imagenes
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws ServletRequestBindingException
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value=CONTROLLER_MAPPING_UPDATE_ANUNCIO, method=RequestMethod.POST)
	public ModelAndView modificarAnuncio(SpaImagenDto imagenes, HttpServletRequest request, HttpServletResponse response)throws IOException, ServletRequestBindingException{
		
		
		/**########################################################################**/
		/**** ################ INICIO DEL TRATAMIENTO DE IMAGENES ################# */
		/**########################################################################**/
		Map<String, String> datos = HttpUtils.getParamsMap(request);
		List<Long> lPubProg = new ArrayList<Long>();
		try{
			//SpaAnuncio anuncio = new SpaAnuncio();
			Long id = ServletRequestUtils.getLongParameter(request, "id");
			SpaAnuncio anuncio = anuncioService.findAnuncioById(id);
			Long idPadre = ServletRequestUtils.getLongParameter(request, "idPadre");
			String titulo = ServletRequestUtils.getStringParameter(request, "titulo");
			String tituloMA = ServletRequestUtils.getStringParameter(request, "titleMunAnun");
			Long categoria = ServletRequestUtils.getLongParameter(request, "categoria");
			Long provincia = ServletRequestUtils.getLongParameter(request, "provincia");
			String emailContacto = ServletRequestUtils.getStringParameter(request, "emailContacto");
			//Long pais = ServletRequestUtils.getLongParameter(request, "pais");
			String description = ServletRequestUtils.getStringParameter(request, "descripcion");
			
			//Eliminamos las publicaciones programadas existentes para el anuncio
			List<Long> publicaciones = anuncio.getIdPublicacionesProgramadas();
			if (publicaciones!=null){
				for (Long idPublicaciones : publicaciones){
					try{
						publicacionesProgramadasService.delete(idPublicaciones);
					}catch (Exception e){
						log.debug("No se ha encontrado la publicacion con este identificador: " + idPublicaciones);
					}
				}				
			}
			
			//Recuperamos las nuevas
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
			for (int i = 0; i < diasSemana.length; i++) {
				if (datos.containsKey("c" + diasSemana[i])){
					String dia = "i" + diasSemana[i];
					
					SpaPublicacionProgramada pub = new SpaPublicacionProgramada(
							sdf.parse(datos.get("d" + diasSemana[i])), //desde
							sdf.parse(datos.get("h" + diasSemana[i])), //hasta
							Integer.parseInt((String)(datos.get(dia))),//intervalo min
							DiasSemanaEnum.values()[i]);//dia semana
					
					lPubProg.add(publicacionesProgramadasService.guardaPublicacion(pub));
				}
			}
			anuncio.setIdPublicacionesProgramadas(lPubProg);
			anuncio.setNodoPadre(idPadre);
			anuncio.setTitulo(titulo);
			anuncio.setTituloMundoanuncio(tituloMA);
			anuncio.setIdCategoria(categoria);
			anuncio.setIdProvincia(provincia);
			anuncio.setDescripcionHTML(description);
			anuncio.setEmailContacto(emailContacto);
			
			/** TRATAMIENTO DE IMAGENES **/
			
			//List<Long> idsImagenesAnuncios = new ArrayList<Long>();
			
			/**#################################################################
				IMAGEN 1
			##################################################################*/
			//Si el anuncio contienen el id de la imagen 1 recuperada 
			//y el flag esta activo entonces modificas
			if (anuncio.getIdsImagenes().contains(imagenes.getIdImagen1()) 
					&& imagenes.getFlag1() == 1){
				// Si la imagen viene vacia esta modificada entonces hay que borrarla
				// y tb se borra como id del anuncio
				if (imagenes.getImagen1().getSize() == 0){
					imagenService.delete(imagenes.getIdImagen1());
					anuncio.getIdsImagenes().remove(imagenes.getIdImagen1());
				}else{
					SpaImagen imagen = imagenService.findById(imagenes.getIdImagen1());
					imagen.setFilename(imagenes.getImagen1().getOriginalFilename());
					imagen.setImagen(imagenes.getImagen1().getBytes());
					imagen.setLink(imagenes.getLink1());
					imagenService.update(imagen);
				}
			// en caso contrario, si la imagen no esta en la lista y viene con el flag
			// a 1, quiere decir que es una nueva imagen
			}else if (imagenes.getIdImagen1()==-1 && imagenes.getFlag1()==1){
				SpaImagen imagen = new SpaImagen();
				imagen.setFilename(imagenes.getImagen1().getOriginalFilename());
				imagen.setIdAnuncio(anuncio.getAnunId());
				imagen.setLink(imagenes.getLink1());
				imagen.setImagen(imagenes.getImagen1().getBytes());
				anuncio.getIdsImagenes().add(imagenService.create(imagen).getId());
			}

			/**#################################################################
			IMAGEN 2
			##################################################################*/
			//Si el anuncio contienen el id de la imagen 1 recuperada 
			//y el flag esta activo entonces modificas
			if (anuncio.getIdsImagenes().contains(imagenes.getIdImagen2()) 
					&& imagenes.getFlag2() == 1){
				// Si la imagen viene vacia esta modificada entonces hay que borrarla
				// y tb se borra como id del anuncio
				if (imagenes.getImagen2().getSize() == 0){
					imagenService.delete(imagenes.getIdImagen2());
					anuncio.getIdsImagenes().remove(imagenes.getIdImagen2());
				}else{
					SpaImagen imagen = imagenService.findById(imagenes.getIdImagen2());
					imagen.setFilename(imagenes.getImagen2().getOriginalFilename());
					imagen.setImagen(imagenes.getImagen2().getBytes());
					imagen.setLink(imagenes.getLink2());
					imagenService.update(imagen);
				}
			// en caso contrario, si la imagen no esta en la lista y viene con el flag
			// a 1, quiere decir que es una nueva imagen
			}else if (imagenes.getIdImagen2()==-1 && imagenes.getFlag2()==1){
				SpaImagen imagen = new SpaImagen();
				imagen.setFilename(imagenes.getImagen2().getOriginalFilename());
				imagen.setIdAnuncio(anuncio.getAnunId());
				imagen.setLink(imagenes.getLink2());
				imagen.setImagen(imagenes.getImagen2().getBytes());
				anuncio.getIdsImagenes().add(imagenService.create(imagen).getId());
			}
			
			/**#################################################################
			IMAGEN 3
			##################################################################*/
			//Si el anuncio contienen el id de la imagen 1 recuperada 
			//y el flag esta activo entonces modificas
			if (anuncio.getIdsImagenes().contains(imagenes.getIdImagen3()) 
					&& imagenes.getFlag3() == 1){
				// Si la imagen viene vacia esta modificada entonces hay que borrarla
				// y tb se borra como id del anuncio
				if (imagenes.getImagen3().getSize() == 0){
					imagenService.delete(imagenes.getIdImagen3());
					anuncio.getIdsImagenes().remove(imagenes.getIdImagen3());
				}else{
					SpaImagen imagen = imagenService.findById(imagenes.getIdImagen3());
					imagen.setFilename(imagenes.getImagen3().getOriginalFilename());
					imagen.setImagen(imagenes.getImagen3().getBytes());
					imagen.setLink(imagenes.getLink3());
					imagenService.update(imagen);
				}
			// en caso contrario, si la imagen no esta en la lista y viene con el flag
			// a 1, quiere decir que es una nueva imagen
			}else if (imagenes.getIdImagen3()==-1 && imagenes.getFlag3()==1){
				SpaImagen imagen = new SpaImagen();
				imagen.setFilename(imagenes.getImagen3().getOriginalFilename());
				imagen.setIdAnuncio(anuncio.getAnunId());
				imagen.setLink(imagenes.getLink3());
				imagen.setImagen(imagenes.getImagen3().getBytes());
				anuncio.getIdsImagenes().add(imagenService.create(imagen).getId());
			}			

			/**#################################################################
			IMAGEN 4
			##################################################################*/
			//Si el anuncio contienen el id de la imagen 1 recuperada 
			//y el flag esta activo entonces modificas
			if (anuncio.getIdsImagenes().contains(imagenes.getIdImagen4()) 
					&& imagenes.getFlag4() == 1){
				// Si la imagen viene vacia esta modificada entonces hay que borrarla
				// y tb se borra como id del anuncio
				if (imagenes.getImagen4().getSize() == 0){
					imagenService.delete(imagenes.getIdImagen4());
					anuncio.getIdsImagenes().remove(imagenes.getIdImagen4());
				}else{
					SpaImagen imagen = imagenService.findById(imagenes.getIdImagen4());
					imagen.setFilename(imagenes.getImagen4().getOriginalFilename());
					imagen.setImagen(imagenes.getImagen4().getBytes());
					imagen.setLink(imagenes.getLink4());
					imagenService.update(imagen);
				}
			// en caso contrario, si la imagen no esta en la lista y viene con el flag
			// a 1, quiere decir que es una nueva imagen
			}else if (imagenes.getIdImagen4()==-1 && imagenes.getFlag4()==1){
				SpaImagen imagen = new SpaImagen();
				imagen.setFilename(imagenes.getImagen4().getOriginalFilename());
				imagen.setIdAnuncio(anuncio.getAnunId());
				imagen.setLink(imagenes.getLink4());
				imagen.setImagen(imagenes.getImagen4().getBytes());
				anuncio.getIdsImagenes().add(imagenService.create(imagen).getId());
			}
			
			/**#################################################################
			IMAGEN 5
			##################################################################*/
			//Si el anuncio contienen el id de la imagen 1 recuperada 
			//y el flag esta activo entonces modificas
			if (anuncio.getIdsImagenes().contains(imagenes.getIdImagen5()) 
					&& imagenes.getFlag5() == 1){
				// Si la imagen viene vacia esta modificada entonces hay que borrarla
				// y tb se borra como id del anuncio
				if (imagenes.getImagen5().getSize() == 0){
					imagenService.delete(imagenes.getIdImagen5());
					anuncio.getIdsImagenes().remove(imagenes.getIdImagen5());
				}else{
					SpaImagen imagen = imagenService.findById(imagenes.getIdImagen5());
					imagen.setFilename(imagenes.getImagen5().getOriginalFilename());
					imagen.setImagen(imagenes.getImagen5().getBytes());
					imagen.setLink(imagenes.getLink5());
					imagenService.update(imagen);
				}
			// en caso contrario, si la imagen no esta en la lista y viene con el flag
			// a 1, quiere decir que es una nueva imagen
			}else if (imagenes.getIdImagen5()==-1 && imagenes.getFlag5()==1){
				SpaImagen imagen = new SpaImagen();
				imagen.setFilename(imagenes.getImagen5().getOriginalFilename());
				imagen.setIdAnuncio(anuncio.getAnunId());
				imagen.setLink(imagenes.getLink5());
				imagen.setImagen(imagenes.getImagen5().getBytes());
				anuncio.getIdsImagenes().add(imagenService.create(imagen).getId());
			}
			
			/**#################################################################
			IMAGEN 6
			##################################################################*/
			//Si el anuncio contienen el id de la imagen 1 recuperada 
			//y el flag esta activo entonces modificas
			if (anuncio.getIdsImagenes().contains(imagenes.getIdImagen6()) 
					&& imagenes.getFlag6() == 1){
				// Si la imagen viene vacia esta modificada entonces hay que borrarla
				// y tb se borra como id del anuncio
				if (imagenes.getImagen6().getSize() == 0){
					imagenService.delete(imagenes.getIdImagen6());
					anuncio.getIdsImagenes().remove(imagenes.getIdImagen6());
				}else{
					SpaImagen imagen = imagenService.findById(imagenes.getIdImagen6());
					imagen.setFilename(imagenes.getImagen6().getOriginalFilename());
					imagen.setImagen(imagenes.getImagen6().getBytes());
					imagen.setLink(imagenes.getLink6());
					imagenService.update(imagen);
				}
			// en caso contrario, si la imagen no esta en la lista y viene con el flag
			// a 1, quiere decir que es una nueva imagen
			}else if (imagenes.getIdImagen6()==-1 && imagenes.getFlag6()==1){
				SpaImagen imagen = new SpaImagen();
				imagen.setFilename(imagenes.getImagen6().getOriginalFilename());
				imagen.setIdAnuncio(anuncio.getAnunId());
				imagen.setLink(imagenes.getLink6());
				imagen.setImagen(imagenes.getImagen6().getBytes());
				anuncio.getIdsImagenes().add(imagenService.create(imagen).getId());
			}
			anuncioService.update(anuncio);
			
			/**########################################################################**/
			/**** ################  FIN DEL TRATAMIENTO DE IMAGENES   ################# */
			/**########################################################################**/
		}catch (Exception e) {
			// TODO: handle exception
			response.setContentType("text/html");					
			response.getWriter().write(JSONSerializer.toJSON(ExtUtils.composeJSOToMap(false, "Se ha producido un error al actualizar el anuncio")).toString());
			log.error("AnunciosController.ModificarAnuncio.ERROR", e);
			return null;
		}
		response.setContentType("text/html");					
		response.getWriter().write(JSONSerializer.toJSON(ExtUtils.composeJSOToMap(true, "El anuncio se ha actualizaco correctamente")).toString());
		return null;
	}

}
