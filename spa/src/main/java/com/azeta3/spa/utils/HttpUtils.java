// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   HttpUtils.java

package com.azeta3.spa.utils;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.web.bind.ServletRequestUtils;

// Referenced classes of package com.joc.spring.core.helpers:
//            FileUtils

public abstract class HttpUtils
{

    public HttpUtils()
    {
    }


    @SuppressWarnings({ "rawtypes", "unchecked" })
	public static Map getParamsMap(HttpServletRequest request)
    {
        Hashtable dev = new Hashtable();
        String auxName;
        String auxValue;
        for(Enumeration enu = request.getParameterNames(); enu.hasMoreElements(); dev.put(auxName, auxValue))
        {
            auxName = (String)enu.nextElement();
            String auxMultValue[] = ServletRequestUtils.getStringParameters(request, auxName);
            auxValue = "";
            if(auxMultValue.length == 0)
                log.warn("Clave sin dato: " + auxName + "-" + auxMultValue);
            else
            if(auxMultValue.length == 1)
                auxValue = auxMultValue[0];
            else
                auxValue = StringUtils.join(auxMultValue, ",");
        }

        return dev;
    }

    private static Logger log;

    static 
    {
        log = Logger.getLogger(com.azeta3.spa.utils.HttpUtils.class);
    }
}
