// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   ExtObject.java

package com.azeta3.spa.utils;

import java.util.*;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

/**
 * 
 * @author ADORU3N
 *
 */
public class ExtObject {

    private static Logger log;
    public static final String SEPARATOR = "|";
    private Map<String, Object> prop;

    static 
    {
        log = Logger.getLogger(com.azeta3.spa.utils.ExtObject.class);
    }
	
    public ExtObject()
    {
        prop = new TreeMap<String, Object>();
    }

    /**
     * 
     * @param data
     */
    public void putAll(Map<String, Object> data)
    {
        if(data != null)
        {
            for(Iterator<String> ite = data.keySet().iterator(); ite.hasNext();)
                try
                {
                    String auxKey = (String)ite.next();
                    Object value = data.get(auxKey);
                    put(auxKey, value);
                }
                catch(Exception ex)
                {
                    log.debug("putAll: " + ex.getMessage());
                }

        }
    }

    /**
     * 
     * @param key
     * @param value
     */
    public void put(String key, Object value)
    {
        if(key != null && value != null)
        {
            log.debug("put: " + key + "-" + value);
            if(value instanceof String)
                value = StringEscapeUtils.escapeHtml((String)value);
            prop.put(key, value);
        } else
        {
            log.debug("Intentando inyectar key/value null. " + key + "=" + value);
        }
    }

    /**
     * 
     * @param key
     * @return
     */
    protected Object get(String key)
    {
        return prop.get(key);
    }

    /**
     * 
     * @return
     */
    public Map<String, Object> getProp()
    {
        return prop;
    }

    /**
     * 
     * @return
     */
    public JSONObject toJSON()
    {
        JsonConfig config = new JsonConfig();
        config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
        return JSONObject.fromObject(getProp(), config);
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        return toJSON().toString();
    }

    /**
     * 
     * @param obj
     * @return
     */
    public static String toJSON(Object obj)
    {
        ExtObject dev = new ExtObject();
        dev.put("key", obj);
        String aux = dev.toString();
        aux = aux.substring(aux.indexOf(":") + 1, aux.length() - 1);
        return aux;
    }

    /**
     * 
     * @param treeNodes
     * @return
     */
    public static String toTreeNode(List<TreeNode> treeNodes)
    {
        return toTreeNode(treeNodes, null);
    }

    /**
     * 
     * @param treeNodes
     * @param defaultProp
     * @return
     */
    public static String toTreeNode(List<TreeNode> treeNodes, Map<String, Object> defaultProp)
    {
        return toTreeNode(treeNodes, defaultProp, false);
    }

    /**
     * 
     * @param treeNodes
     * @param defaultProp
     * @param withoutArray
     * @return
     */
    public static String toTreeNode(List<TreeNode> treeNodes, Map<String, Object> defaultProp, boolean withoutArray)
    {
        ArrayList<String> dev = new ArrayList<String>();
        for(Iterator<TreeNode> ite = treeNodes.iterator(); ite.hasNext();)
            try
            {
                TreeNode treeNode = (TreeNode)ite.next();
                dev.add(treeNode.toTreeNode(defaultProp));
            }
            catch(Exception ex)
            {
                log.warn(ex.getMessage());
            }

        return (withoutArray ? "" : "[") + StringUtils.join(dev.toArray(new Object[dev.size()]), ",") + (withoutArray ? "" : "]");
    }

    /**
     * 
     * @param data
     * @param prop
     * @param defaultProp
     * @return
     */
    public static String toTreeNode(Object data[], String prop[], Map<String, Object> defaultProp)
    {
        return toTreeNode(data, prop, defaultProp, false);
    }

    /**
     * 
     * @param data
     * @param prop
     * @param defaultProp
     * @param withoutArray
     * @return
     */
    public static String toTreeNode(Object data[], String prop[], Map<String, Object> defaultProp, boolean withoutArray)
    {
        ArrayList<String> dev = new ArrayList<String>();
        Map<String, Object> propMap = new TreeMap<String, Object>(defaultProp);
        for(int i = 0; i < data.length; i++)
        {
            String auxData = String.valueOf(data[i]);
            for(int c = 0; c < prop.length; c++)
            {
                String auxProp = prop[c];
                propMap.put(auxProp, auxData);
            }

            ExtObject extObject = new ExtObject();
            extObject.putAll(propMap);
            dev.add(extObject.toString());
        }

        return (withoutArray ? "" : "[") + StringUtils.join(dev.toArray(new Object[dev.size()]), ",") + (withoutArray ? "" : "]");
    }
    
}
