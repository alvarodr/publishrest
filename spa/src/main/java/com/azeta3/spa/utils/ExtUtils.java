package com.azeta3.spa.utils;

import java.util.*;

import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

/**
 * 
 * @author ADORU3N
 *
 */
public abstract class ExtUtils {

	private static Logger log;

	static {
		log = Logger.getLogger(com.azeta3.spa.utils.ExtUtils.class);
	}
	
	public ExtUtils() { }

	/**
	 * 
	 * @param success
	 * @param msg
	 * @return
	 */
	public static String successJSON(boolean success, String msg) {
		return successJSON(success, msg, null, null);
	}

	/**
	 * 
	 * @param success
	 * @param msg
	 * @param errMsg
	 * @param exceptionError
	 * @return
	 */
	public static String successJSON(boolean success, String msg,
			String errMsg, String exceptionError) {
		log.debug(success + "-" + msg);
		Map<String, Object> dev = new HashMap<String, Object>();
		dev.put("success", new Boolean(success));
		dev.put("msg", msg);
		dev.put("errMsg", errMsg);
		dev.put("exception", exceptionError);
		return dev.toString();
	}

	/**
	 * 
	 * @param success
	 * @param msg
	 * @return
	 */
	public static ModelAndView successForm(boolean success, String msg) {
		Map<String, Object> mapa = new HashMap<String, Object>();
		mapa.put("success", success);
		mapa.put("msg", msg);
		return new ModelAndView("json", mapa);
	}

	/**
	 * 
	 * @param data
	 * @return
	 */
	public static String composeJSON(Map data) {
		// Map<String, Object> dev = new HashMap<String, Object>();
		// dev.putAll(data);
		return data.toString();
	}

	/**
	 * 
	 * @param data
	 * @return
	 */
	public static JSONObject composeJSONObject(Map data) {
		ExtObject dev = new ExtObject();
		dev.putAll(data);
		return dev.toJSON();
	}

	/**
	 * 
	 * @param key
	 * @param data
	 * @return
	 */
	public static Map<String, Object> composeJSONonMap(String key, Map data) {
		Map<String, Object> dev = new HashMap<String, Object>();
		dev.put(key, composeJSON(data));
		return dev;
	}

	/**
	 * 
	 * @param success
	 * @param msg
	 * @return
	 */
	public static Map<String, Object> composeJSOToMap(Boolean success,
			String msg) {
		Map<String, Object> dev = new HashMap<String, Object>();
		dev.put("success", success);
		dev.put("msg", msg);
		return dev;
	}

	/**
	 * 
	 * @param json
	 * @return
	 */
	public static ModelAndView createJSONMaV(String json) {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("results", json);
		data.put("data", json);
		return new ModelAndView("json", data);
	}

	/**
	 * 
	 * @param success
	 * @param lista
	 * @return
	 */
	public static ModelAndView composeJSONMaV(boolean success, Object lista) {
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("success", new Boolean(success));
		data.put("data", lista);
		return new ModelAndView("json", data);
	}

}
