package com.azeta3.spa.utils;

import java.io.Serializable;
import java.util.Map;

/**
 * 
 * @author ADORU3N
 *
 */
public interface TreeNode extends Serializable {

	/**
	 * 
	 * @param map
	 * @return
	 */
	public abstract String toTreeNode(Map<String, Object> map);
}
