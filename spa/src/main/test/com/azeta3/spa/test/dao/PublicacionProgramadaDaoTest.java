package com.azeta3.spa.test.dao;

import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.jdo.annotations.NotPersistent;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.azeta3.spa.bom.SpaPublicacionProgramada;
import com.azeta3.spa.constantes.DiasSemanaEnum;
import com.azeta3.spa.dao.PublicacionProgramadaDao;
import com.azeta3.spa.test.BaseSpringTest;


public class PublicacionProgramadaDaoTest extends BaseSpringTest{
	
	@Autowired
	private PublicacionProgramadaDao publicacionProgramadaDao;

	public PublicacionProgramadaDaoTest(){}
	
	@Test
	@NotPersistent
	public void findPublicacionesExecuteNowTest(){
		findPublicacionesExecuteNowTestMock(publicacionProgramadaDao);
	}
	
	
	@Test
	@NotPersistent
	public void findByIdTest(){
		findByIdTestMock(publicacionProgramadaDao);
	}
	
	@Test
	@NotPersistent
	public void findByDayOfWeekTest(){
		findByDayOfWeekTestMock(publicacionProgramadaDao);
	}
	
	@Test
	@NotPersistent
	public void createPublicacionProgramadaTest(){
		createPublicacionProgramadaTestMock(publicacionProgramadaDao);
	}
	
	@Test
	@NotPersistent
	public void removeTest(){
		removeTestMock(publicacionProgramadaDao);
	}
	
	@Test
	@NotPersistent
	public void findAllTest(){
		findAllTestMock(publicacionProgramadaDao);
	}
	
	public void findAllTestMock(PublicacionProgramadaDao publicacionProgramadaDao){
		String idUsuario = "usuario1@dominio.com";
		
		/**
		 * Creo 3 publicaciones para el Lunes
		 * 
		 *  - De 18:15 a 19:15 cada minuto
		 *  - De 18:30 a 19:30 cada minuto
		 *  - De 18:45 a 19:45 cada minuto
		 */
		Calendar horaDesde = new GregorianCalendar();
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 15);
		horaDesde.set(Calendar.MONTH,Calendar.JANUARY);
		horaDesde.set(Calendar.YEAR, 2007);
		horaDesde.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		horaDesde.set(Calendar.DAY_OF_MONTH, 1);
		
		
		Calendar horaHasta = new GregorianCalendar();
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 15);
		horaHasta.set(Calendar.MONTH,Calendar.JANUARY);
		horaHasta.set(Calendar.YEAR, 2007);
		horaHasta.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		horaHasta.set(Calendar.DAY_OF_MONTH, 1);
		
		SpaPublicacionProgramada publicacionProgramada1 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		publicacionProgramada1.setIntervaloMin(1);
		publicacionProgramada1.setIdUsuario(idUsuario);
		
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 30);
	
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 30);
		
		SpaPublicacionProgramada publicacionProgramada2 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		publicacionProgramada2.setIntervaloMin(2);
		publicacionProgramada2.setIdUsuario(idUsuario);
		
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 45);
	
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 45);
		
		SpaPublicacionProgramada publicacionProgramada3 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		
		publicacionProgramada3.setIntervaloMin(3);
		publicacionProgramada3.setIdUsuario(idUsuario);
		
		publicacionProgramada1 = publicacionProgramadaDao.createPublicacionProgramada(publicacionProgramada1);
		publicacionProgramada2 = publicacionProgramadaDao.createPublicacionProgramada(publicacionProgramada2);
		publicacionProgramada3 = publicacionProgramadaDao.createPublicacionProgramada(publicacionProgramada3);

		Assert.assertTrue(publicacionProgramadaDao.findAll().size() == 3);
	}
	
	public void removeTestMock(PublicacionProgramadaDao publicacionProgramadaDao){
		String idUsuario = "usuario1@dominio.com";
		Calendar horaDesde = new GregorianCalendar();
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 15);
		horaDesde.set(Calendar.MONTH,Calendar.JANUARY);
		horaDesde.set(Calendar.YEAR, 2007);
		horaDesde.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		horaDesde.set(Calendar.DAY_OF_MONTH, 1);
		
		
		Calendar horaHasta = new GregorianCalendar();
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 15);
		horaHasta.set(Calendar.MONTH,Calendar.JANUARY);
		horaHasta.set(Calendar.YEAR, 2007);
		horaHasta.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		horaHasta.set(Calendar.DAY_OF_MONTH, 1);
		
		SpaPublicacionProgramada publicacionProgramada1 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		publicacionProgramada1.setIntervaloMin(1);
		publicacionProgramada1.setIdUsuario(idUsuario);
		
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 30);
	
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 30);
		
		publicacionProgramada1 = publicacionProgramadaDao.createPublicacionProgramada(publicacionProgramada1);
		Assert.assertTrue(publicacionProgramada1 != null);
				
		publicacionProgramadaDao.remove(publicacionProgramada1.getId());
		Assert.assertTrue(publicacionProgramadaDao.findById(publicacionProgramada1.getId()) == null);
	}
	
	public void createPublicacionProgramadaTestMock(PublicacionProgramadaDao publicacionProgramadaDao){
		String idUsuario = "usuario1@dominio.com";
		Calendar horaDesde = new GregorianCalendar();
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 15);
		horaDesde.set(Calendar.MONTH,Calendar.JANUARY);
		horaDesde.set(Calendar.YEAR, 2007);
		horaDesde.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		horaDesde.set(Calendar.DAY_OF_MONTH, 1);
		
		
		Calendar horaHasta = new GregorianCalendar();
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 15);
		horaHasta.set(Calendar.MONTH,Calendar.JANUARY);
		horaHasta.set(Calendar.YEAR, 2007);
		horaHasta.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		horaHasta.set(Calendar.DAY_OF_MONTH, 1);
		
		SpaPublicacionProgramada publicacionProgramada1 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		publicacionProgramada1.setIntervaloMin(1);
		publicacionProgramada1.setIdUsuario(idUsuario);
		
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 30);
	
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 30);
		
		Assert.assertTrue(publicacionProgramadaDao.createPublicacionProgramada(publicacionProgramada1) != null);
		
	}
	
	public void findByDayOfWeekTestMock(PublicacionProgramadaDao publicacionProgramadaDao){
String idUsuario = "usuario1@dominio.com";
		
		/**
		 * Creo 3 publicaciones para el Lunes
		 * 
		 *  - De 18:15 a 19:15 cada minuto
		 *  - De 18:30 a 19:30 cada minuto
		 *  - De 18:45 a 19:45 cada minuto
		 */
		Calendar horaDesde = new GregorianCalendar();
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 15);
		horaDesde.set(Calendar.MONTH,Calendar.JANUARY);
		horaDesde.set(Calendar.YEAR, 2007);
		horaDesde.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		horaDesde.set(Calendar.DAY_OF_MONTH, 1);
		
		
		Calendar horaHasta = new GregorianCalendar();
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 15);
		horaHasta.set(Calendar.MONTH,Calendar.JANUARY);
		horaHasta.set(Calendar.YEAR, 2007);
		horaHasta.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		horaHasta.set(Calendar.DAY_OF_MONTH, 1);
		
		SpaPublicacionProgramada publicacionProgramada1 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		publicacionProgramada1.setIntervaloMin(1);
		publicacionProgramada1.setIdUsuario(idUsuario);
		
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 30);
	
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 30);
		
		SpaPublicacionProgramada publicacionProgramada2 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		publicacionProgramada2.setIntervaloMin(2);
		publicacionProgramada2.setIdUsuario(idUsuario);
		
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 45);
	
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 45);
		
		SpaPublicacionProgramada publicacionProgramada3 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		
		publicacionProgramada3.setIntervaloMin(3);
		publicacionProgramada3.setIdUsuario(idUsuario);
		
		publicacionProgramada1 = publicacionProgramadaDao.createPublicacionProgramada(publicacionProgramada1);
		publicacionProgramada2 = publicacionProgramadaDao.createPublicacionProgramada(publicacionProgramada2);
		publicacionProgramada3 = publicacionProgramadaDao.createPublicacionProgramada(publicacionProgramada3);
		
		
		Assert.assertTrue(publicacionProgramadaDao.findByDayOfWeek(DiasSemanaEnum.LUNES).size() == 3);
	}
	
	public void findByIdTestMock(PublicacionProgramadaDao publicacionProgramadaDao){
		String idUsuario = "usuario1@dominio.com";
		Calendar horaDesde = new GregorianCalendar();
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 15);
		horaDesde.set(Calendar.MONTH,Calendar.JANUARY);
		horaDesde.set(Calendar.YEAR, 2007);
		horaDesde.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		horaDesde.set(Calendar.DAY_OF_MONTH, 1);
		
		
		Calendar horaHasta = new GregorianCalendar();
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 15);
		horaHasta.set(Calendar.MONTH,Calendar.JANUARY);
		horaHasta.set(Calendar.YEAR, 2007);
		horaHasta.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		horaHasta.set(Calendar.DAY_OF_MONTH, 1);
		
		SpaPublicacionProgramada publicacionProgramada1 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		publicacionProgramada1.setIntervaloMin(1);
		publicacionProgramada1.setIdUsuario(idUsuario);
		
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 30);
	
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 30);
		
		publicacionProgramada1 = publicacionProgramadaDao.createPublicacionProgramada(publicacionProgramada1);
		Assert.assertTrue(publicacionProgramadaDao.findById(publicacionProgramada1.getId()) != null);
		
	}
	
	public void findPublicacionesExecuteNowTestMock(PublicacionProgramadaDao publicacionProgramadaDao){
		String idUsuario = "usuario1@dominio.com";
		
		/**
		 * Creo 3 publicaciones para el Lunes
		 * 
		 *  - De 18:15 a 19:15 cada minuto
		 *  - De 18:30 a 19:30 cada minuto
		 *  - De 18:45 a 19:45 cada minuto
		 */
		Calendar horaDesde = new GregorianCalendar();
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 15);
		horaDesde.set(Calendar.MONTH,Calendar.JANUARY);
		horaDesde.set(Calendar.YEAR, 2007);
		horaDesde.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		horaDesde.set(Calendar.DAY_OF_MONTH, 1);
		
		
		Calendar horaHasta = new GregorianCalendar();
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 15);
		horaHasta.set(Calendar.MONTH,Calendar.JANUARY);
		horaHasta.set(Calendar.YEAR, 2007);
		horaHasta.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		horaHasta.set(Calendar.DAY_OF_MONTH, 1);
		
		SpaPublicacionProgramada publicacionProgramada1 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		publicacionProgramada1.setIntervaloMin(1);
		publicacionProgramada1.setIdUsuario(idUsuario);
		
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 30);
	
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 30);
		
		SpaPublicacionProgramada publicacionProgramada2 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		publicacionProgramada2.setIntervaloMin(2);
		publicacionProgramada2.setIdUsuario(idUsuario);
		
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 45);
	
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 45);
		
		SpaPublicacionProgramada publicacionProgramada3 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		
		publicacionProgramada3.setIntervaloMin(3);
		publicacionProgramada3.setIdUsuario(idUsuario);
		
		publicacionProgramada1 = publicacionProgramadaDao.createPublicacionProgramada(publicacionProgramada1);
		publicacionProgramada2 = publicacionProgramadaDao.createPublicacionProgramada(publicacionProgramada2);
		publicacionProgramada3 = publicacionProgramadaDao.createPublicacionProgramada(publicacionProgramada3);
		
		/**
		 * Busco publicaciones para las 18:00, debo obtener 0
		 */
		Calendar now = new GregorianCalendar();
		now.set(Calendar.MONTH,Calendar.JANUARY);
		now.set(Calendar.YEAR, 2007);
		now.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		now.set(Calendar.DAY_OF_MONTH, 1);
		
		now.set(Calendar.HOUR_OF_DAY, 18);
		now.set(Calendar.MINUTE,0);
		
		Assert.assertTrue(publicacionProgramadaDao.findPublicacionesExecuteNow(now,idUsuario) == null ||
				publicacionProgramadaDao.findPublicacionesExecuteNow(now,idUsuario).size() == 0);
		
		/**
		 * Busco publicaciones para las 18:15, debo obtener 1
		 */
		
		now.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		now.set(Calendar.HOUR_OF_DAY, 18);
		now.set(Calendar.MINUTE,15);
		
		Assert.assertTrue(publicacionProgramadaDao.findPublicacionesExecuteNow(now,idUsuario).size() == 1);
		
		/**
		 * Busco publicaciones para las 19:00, debo obtener 3
		 */
		
		now.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		now.set(Calendar.HOUR_OF_DAY, 19);
		now.set(Calendar.MINUTE,00);
		
		Assert.assertTrue(publicacionProgramadaDao.findPublicacionesExecuteNow(now,idUsuario).size() == 3);
	}
}
