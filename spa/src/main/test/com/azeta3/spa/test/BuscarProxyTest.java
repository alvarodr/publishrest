package com.azeta3.spa.test;

import static com.gargoylesoftware.htmlunit.BrowserVersion.FIREFOX_3_6;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.datanucleus.query.evaluator.memory.TrimFunctionEvaluator;
import org.junit.Test;

import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.Page;
import com.gargoylesoftware.htmlunit.ProxyConfig;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlTable;
import com.gargoylesoftware.htmlunit.html.HtmlTableRow;

public class BuscarProxyTest {
	
	Logger log = Logger.getLogger(this.getClass());
	
	@Test
	public void buscarProxy() throws FailingHttpStatusCodeException, MalformedURLException, IOException{
		
		WebClient webClient = new WebClient(FIREFOX_3_6);
		//webClient.setJavaScriptEnabled(false);
		webClient.setCssEnabled(false);
		webClient.setAppletEnabled(false);
		webClient.setThrowExceptionOnFailingStatusCode(false);
		webClient.setThrowExceptionOnScriptError(false);
		webClient.setPopupBlockerEnabled(true);
		
		Integer count = 3;
		while(count< 11){
			try{
				count++;
				
				HtmlPage page = webClient.getPage("http://www.cnproxy.com/proxy" + count + ".html");
				
				List<HtmlElement> tablas = page.getBody().getHtmlElementsByTagName("table");
				HtmlTable tabla = (HtmlTable) tablas.get(2);
				List<HtmlTableRow> filas = tabla.getRows();
	
				
				for(HtmlTableRow fila : filas){
					
					String ip;
					String puerto;
					String content = fila.getCell(0).asXml();
					
					int posFinalIP = content.lastIndexOf("<script") - 4;
					if(posFinalIP >= 11){
						
						ip = content.substring(6, posFinalIP);
						
						int posInicioPuerto = content.lastIndexOf("</script>") + 14;
						int posFinalPuerto = content.lastIndexOf("</td>") - 1;
						puerto = content.substring(posInicioPuerto,posFinalPuerto);
						
						WebClient webClient2 = new WebClient(FIREFOX_3_6);
						webClient2.setJavaScriptEnabled(false);
						webClient2.setCssEnabled(false);
						webClient2.setAppletEnabled(false);
						webClient2.setThrowExceptionOnFailingStatusCode(false);
						webClient2.setThrowExceptionOnScriptError(false);
						webClient2.setPopupBlockerEnabled(true);
						
						ip = ip.trim();
						puerto = puerto.trim().replaceAll("\"", "");
						
						try{
							Boolean socks = false;
							
							ProxyConfig proxy = new ProxyConfig(ip, Integer.parseInt(puerto));
							if(fila.getCell(1).asXml().contains("HTTP")) socks = false;
							else socks = true;
							
							proxy.setSocksProxy(socks);
							
							webClient2.setProxyConfig(proxy);
							
							HtmlPage mundoAnuncio = webClient2.getPage("http://www.mundoanuncio.com");
							//log.debug(mundoAnuncio.asText());
							if(mundoAnuncio.getBody().asText().contains("Anuncios gratis en Todo el Mundo")){
								System.out.println(ip + ";" + puerto + ";" + socks);
							}
						}catch (Exception e) {}
						
					}
				}
			}catch(Exception e){log.debug("error en pagina " + count + e.getMessage());}
			
		}
	}
}
