package com.azeta3.spa.test.dao;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.jdo.annotations.NotPersistent;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.azeta3.spa.bom.SpaAnuncio;
import com.azeta3.spa.dao.AnuncioDao;
import com.azeta3.spa.test.BaseSpringTest;

public class AnuncioDaoTest extends BaseSpringTest{

	@Autowired
	AnuncioDao anuncioDao;
	
	@Test
	@NotPersistent
	public void creaAnuncioTest() {
		creaAnuncioTestMock(anuncioDao);
	}
	
	@Test
	@NotPersistent
	public void eliminaAnuncioTest() {
		eliminaAnuncioTestMock(anuncioDao);
	}
	
	@Test
	@NotPersistent
	public void findAnuncioByIdTest() {
		findAnuncioByIdTestMock(anuncioDao);
	}
	
	@Test
	@NotPersistent
	public void findAllAnunciosTest() {
		findAllAnunciosTestMock(anuncioDao);
	}
	
	@Test
	@NotPersistent
	public void findByPadreTest(){
		findByPadreTestMock(anuncioDao);
	}
	
	@Test
	@NotPersistent
	public void findByUserEmailTest(){
		findByUserEmailTestMock(anuncioDao);
	}
	
	@Test
	@NotPersistent
	public void updateAnuncioTest(){
		updateAnuncioTestMock(anuncioDao);
	}
	
	@Test
	@NotPersistent
	public void getIdsImagesTest(){
		getImagesTestMock(anuncioDao);
	}
	
	@Test
	@NotPersistent
	public void getIdsPublicacionesTest(){
		getPublicacionesTestMock(anuncioDao);
	}
	
	public void updateAnuncioTestMock(AnuncioDao anuncioDao){
		SpaAnuncio anuncio = new SpaAnuncio(
				"titulo",
				"tituloMundoanuncio",
				1L,
				"DescripcionHTML",
				1L,
				true,
				1L, "usuario1@dominio.com");
		anuncio = anuncioDao.creaAnuncio(anuncio);
		
		anuncio.setTitulo("updated");
		
		anuncioDao.updateAnuncio(anuncio);
		assertTrue(anuncioDao.findAnuncioById(anuncio.getAnunId()).getTitulo().equals("updated"));
	}
	
	public void findByUserEmailTestMock(AnuncioDao anuncioDao){
		SpaAnuncio directorio = new SpaAnuncio(null,null,null,null,null,false,0L, "usuario1@dominio.com");
		
		SpaAnuncio anuncio = new SpaAnuncio(
				"titulo",
				"tituloMundoanuncio",
				1L,
				"DescripcionHTML",
				1L,
				true,
				1L, "usuario1@dominio.com");
		
		directorio = anuncioDao.creaAnuncio(directorio);
		anuncio = anuncioDao.creaAnuncio(anuncio);
		
		assertTrue(anuncioDao.findByUserEmail("usuario1@dominio.com") != null);
	}
	
	public void findByPadreTestMock(AnuncioDao anuncioDao){
		SpaAnuncio raiz = new SpaAnuncio("Raiz",null,null,null,null,false,-1L,"usuario1@dominio.com");
		
		SpaAnuncio directorio = new SpaAnuncio(null,null,null,null,null,false,0L, "usuario1@dominio.com");
		
		SpaAnuncio anuncio = new SpaAnuncio(
				"titulo",
				"tituloMundoanuncio",
				1L,
				"DescripcionHTML",
				1L,
				true,
				1L, "usuario1@dominio.com");
		
		raiz = anuncioDao.creaAnuncio(raiz);
		directorio = anuncioDao.creaAnuncio(directorio);
		anuncio = anuncioDao.creaAnuncio(anuncio);
		
		assertTrue(anuncioDao.findByPadre(anuncio.getNodoPadre().toString(), "usuario1@dominio.com").size() == 1);
	}
	
	public void findAllAnunciosTestMock(AnuncioDao anuncioDao){
		SpaAnuncio raiz = new SpaAnuncio("Raiz",null,null,null,null,false,-1L,"usuario1@dominio.com");
		
		SpaAnuncio directorio = new SpaAnuncio(null,null,null,null,null,false,0L, "usuario1@dominio.com");
		
		SpaAnuncio anuncio = new SpaAnuncio(
				"titulo",
				"tituloMundoanuncio",
				1L,
				"DescripcionHTML",
				1L,
				true,
				1L, "usuario1@dominio.com");
		
		anuncioDao.creaAnuncio(raiz);
		anuncioDao.creaAnuncio(directorio);
		anuncioDao.creaAnuncio(anuncio);
		
		assertTrue(anuncioDao.findAllAnuncios().size() == 3);
	}
	
	public void findAnuncioByIdTestMock(AnuncioDao anuncioDao){
		SpaAnuncio raiz = new SpaAnuncio("Raiz",null,null,null,null,false,-1L,"usuario1@dominio.com");
		raiz = anuncioDao.creaAnuncio(raiz);
		assertTrue(anuncioDao.findAnuncioById(raiz.getAnunId()) != null);
	}
	
	public void eliminaAnuncioTestMock(AnuncioDao anuncioDao){
		SpaAnuncio raiz = new SpaAnuncio("Raiz",null,null,null,null,false,-1L,"usuario1@dominio.com");
		raiz = anuncioDao.creaAnuncio(raiz);
		anuncioDao.eliminaAnuncio(raiz);
		assertTrue(anuncioDao.findAnuncioById(raiz.getAnunId()) == null);
	}
	
	public void creaAnuncioTestMock(AnuncioDao anuncioDao){
		SpaAnuncio raiz = new SpaAnuncio("Raiz",null,null,null,null,false,-1L,"usuario1@dominio.com");
		assertTrue(anuncioDao.creaAnuncio(raiz) != null);
	}
	
	public void getImagesTestMock(AnuncioDao anuncioDao){
		SpaAnuncio raiz = new SpaAnuncio("Raiz",null,null,null,null,false,-1L,"usuario1@dominio.com");
		SpaAnuncio directorio = new SpaAnuncio(null,null,null,null,null,false,0L, "usuario1@dominio.com");
		SpaAnuncio anuncio = new SpaAnuncio(
				"titulo",
				"tituloMundoanuncio",
				1L,
				"DescripcionHTML",
				1L,
				true,
				1L, "usuario1@dominio.com");
		
		//anuncio.setIdsImagenes(new ArrayList<Long>());
		anuncio.setIdsImagenes(new ArrayList<Long>());
		anuncio.getIdsImagenes().add(1L);
		
		anuncioDao.creaAnuncio(raiz);
		anuncioDao.creaAnuncio(directorio);
		SpaAnuncio aux = anuncioDao.creaAnuncio(anuncio);
		
		assertNotNull(anuncioDao.findAnuncioById(aux.getAnunId()).getIdsImagenes());
	}
	
	public void getPublicacionesTestMock(AnuncioDao anuncioDao){
		SpaAnuncio raiz = new SpaAnuncio("Raiz",null,null,null,null,false,-1L,"usuario1@dominio.com");
		SpaAnuncio directorio = new SpaAnuncio(null,null,null,null,null,false,0L, "usuario1@dominio.com");
		SpaAnuncio anuncio = new SpaAnuncio(
				"titulo",
				"tituloMundoanuncio",
				1L,
				"DescripcionHTML",
				1L,
				true,
				1L, "usuario1@dominio.com");
		
		anuncio.setIdPublicacionesProgramadas(new ArrayList<Long>());
		anuncio.getIdPublicacionesProgramadas().add(1L);
		
		anuncioDao.creaAnuncio(raiz);
		anuncioDao.creaAnuncio(directorio);
		SpaAnuncio aux = anuncioDao.creaAnuncio(anuncio);
		
		assertNotNull(anuncioDao.findAnuncioById(aux.getAnunId()).getIdPublicacionesProgramadas());
	}
}
