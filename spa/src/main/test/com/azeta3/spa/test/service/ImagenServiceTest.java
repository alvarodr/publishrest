package com.azeta3.spa.test.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.azeta3.spa.service.ImagenService;
import com.azeta3.spa.test.BaseSpringTest;

public class ImagenServiceTest extends BaseSpringTest {

	@Autowired
	private ImagenService imagenService;
	
	@Test
	public void editImageTest() throws IOException{
		
		BufferedImage bi = ImageIO.read(new File("src/main/test/resources/captcha/test.jpg"));
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bi, "JPG", baos);
		byte[] imageByteArray = baos.toByteArray();
		byte[] imageByteArrayNew = imagenService.ModificarImagen(imageByteArray);

		ByteArrayInputStream bais = new ByteArrayInputStream(imageByteArrayNew);
		BufferedImage imgNew = ImageIO.read(bais);
		
		Assert.assertFalse(bi.equals(imgNew));
	}
}
