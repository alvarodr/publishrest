package com.azeta3.spa.test.service;

import java.io.File;
import java.io.IOException;

import junit.framework.Assert;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import com.azeta3.spa.service.AdminPanelService;
import com.azeta3.spa.service.impl.AdminPanelServiceImpl;
import com.azeta3.spa.test.BaseMockTest;

public class AdminPanelServiceTest extends BaseMockTest{
	
	private AdminPanelService adminPanelService;
	
	@Before
    public void init() throws IOException {
		
		adminPanelService = new AdminPanelServiceImpl(usuarioDaoMock);
		
    }
	
	@Test
	public void getUsuariosJSON() throws JsonGenerationException, JsonMappingException, IOException{
		
	    ObjectMapper objectMapper = new ObjectMapper();
		String json = objectMapper.writeValueAsString(adminPanelService.getUsuariosJSON());
		
		Assert.assertTrue(json.equals(leerFichero(new File("src/main/test/resources/usuarios.json"))));
	}
}
