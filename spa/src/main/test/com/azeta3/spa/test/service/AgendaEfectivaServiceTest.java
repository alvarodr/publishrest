package com.azeta3.spa.test.service;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.azeta3.spa.bom.SpaAnuncio;
import com.azeta3.spa.bom.SpaPublicacionEfectiva;
import com.azeta3.spa.bom.SpaPublicacionProgramada;
import com.azeta3.spa.constantes.DiasSemanaEnum;
import com.azeta3.spa.dao.AnuncioDao;
import com.azeta3.spa.dao.PublicacionProgramadaDao;
import com.azeta3.spa.service.AgendaEfectivaService;
import com.azeta3.spa.test.BaseSpringTest;

public class AgendaEfectivaServiceTest extends BaseSpringTest {

	@Autowired
	private AgendaEfectivaService agendaEfectivaService;
	
	@Autowired
	private PublicacionProgramadaDao publicacionProgramadaDao;
	
	@Autowired
	private AnuncioDao anuncioDao;
	
	@Before
	public void prepareGenerarAgendaTest(){
		String idUsuario = "usuario1@dominio.com";
		
		/**
		 * Creo 3 publicaciones para el Lunes
		 * 
		 *  - De 18:15 a 19:15 cada minuto
		 *  - De 18:30 a 19:30 cada minuto
		 *  - De 18:45 a 19:45 cada minuto
		 */
		Calendar horaDesde = new GregorianCalendar();
		horaDesde.set(Calendar.HOUR_OF_DAY, 17);
		horaDesde.set(Calendar.MINUTE, 50);
		horaDesde.set(Calendar.MONTH,Calendar.JANUARY);
		horaDesde.set(Calendar.YEAR, 2007);
		horaDesde.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		horaDesde.set(Calendar.DAY_OF_MONTH, 1);
		
		
		Calendar horaHasta = new GregorianCalendar();
		horaHasta.set(Calendar.HOUR_OF_DAY, 18);
		horaHasta.set(Calendar.MINUTE, 10);
		horaHasta.set(Calendar.MONTH,Calendar.JANUARY);
		horaHasta.set(Calendar.YEAR, 2007);
		horaHasta.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		horaHasta.set(Calendar.DAY_OF_MONTH, 1);
		
		SpaPublicacionProgramada publicacionProgramada1 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		publicacionProgramada1.setIntervaloMin(1);
		publicacionProgramada1.setIdUsuario(idUsuario);
		
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 50);
	
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 10);
		
		SpaPublicacionProgramada publicacionProgramada2 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		publicacionProgramada2.setIntervaloMin(1);
		publicacionProgramada2.setIdUsuario(idUsuario);
		
		horaDesde.set(Calendar.HOUR_OF_DAY, 19);
		horaDesde.set(Calendar.MINUTE, 50);
	
		horaHasta.set(Calendar.HOUR_OF_DAY, 20);
		horaHasta.set(Calendar.MINUTE, 10);
		
		SpaPublicacionProgramada publicacionProgramada3 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		
		publicacionProgramada3.setIntervaloMin(1);
		publicacionProgramada3.setIdUsuario(idUsuario);
		
		publicacionProgramada1 = publicacionProgramadaDao.createPublicacionProgramada(publicacionProgramada1);
		publicacionProgramada2 = publicacionProgramadaDao.createPublicacionProgramada(publicacionProgramada2);
		publicacionProgramada3 = publicacionProgramadaDao.createPublicacionProgramada(publicacionProgramada3);

		SpaAnuncio anuncio = new SpaAnuncio(null,null,null,null,null,false,null,null);
		List<Long> pubs = new ArrayList<Long>();
		pubs.add(publicacionProgramada1.getId());
		pubs.add(publicacionProgramada2.getId());
		pubs.add(publicacionProgramada3.getId());
		anuncio.setIdPublicacionesProgramadas(pubs);
		anuncioDao.creaAnuncio(anuncio);
	}
	
	@Test
	public void generarAgendaTest() {
		
		List<SpaPublicacionEfectiva> publicaciones = agendaEfectivaService.generarAgenda();
		
		assertTrue(publicaciones.size()==60);
	}

}
