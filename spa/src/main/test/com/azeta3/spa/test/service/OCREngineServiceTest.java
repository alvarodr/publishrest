package com.azeta3.spa.test.service;

import static org.junit.Assert.*;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.azeta3.spa.service.OCREngineService;
import com.azeta3.spa.test.BaseSpringTest;

public class OCREngineServiceTest extends BaseSpringTest{

	private Logger log = Logger.getLogger(this.getClass());
	
	@Autowired
	private OCREngineService ocr;
	
	@Test
	public void reconocerTexto() throws IOException{

		File image = new File("src/main/test/resources/captcha/test.jpg");

		BufferedImage bi = ImageIO.read(image);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bi, "JPG", baos);
		byte[] imageByteArray = baos.toByteArray();
		
		String texto = ocr.ocr(imageByteArray);
		texto = texto.toUpperCase();
		texto = texto.replace("\n", "");
		texto = texto.replace(" ","");
		
		log.debug("Texto OCR -> " + texto); 
		assertTrue(texto.equals("TEST"));
	}
	
}
