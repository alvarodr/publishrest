package com.azeta3.spa.test;

import static org.mockito.Mockito.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import junit.framework.Assert;

import org.springframework.core.io.FileSystemResource;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.token.Sha512DigestUtils;

import au.com.bytecode.opencsv.CSVReader;

import com.azeta3.spa.bom.SpaAnuncio;
import com.azeta3.spa.bom.SpaCategoria;
import com.azeta3.spa.bom.SpaImagen;
import com.azeta3.spa.bom.SpaProvincia;
import com.azeta3.spa.bom.SpaPublicacionProgramada;
import com.azeta3.spa.bom.SpaUsuario;
import com.azeta3.spa.constantes.DiasSemanaEnum;
import com.azeta3.spa.dao.AnuncioDao;
import com.azeta3.spa.dao.CategoriaDao;
import com.azeta3.spa.dao.ImagenDao;
import com.azeta3.spa.dao.ProvinciaDao;
import com.azeta3.spa.dao.PublicacionProgramadaDao;
import com.azeta3.spa.dao.UsuarioDao;
import com.azeta3.spa.security.Roles;
import com.azeta3.spa.test.dao.AnuncioDaoTest;
import com.azeta3.spa.test.dao.PublicacionProgramadaDaoTest;
import com.azeta3.spa.test.dao.UsuarioDaoTest;

@RunWith(MockitoJUnitRunner.class)
public class BaseMockTest {

	protected Logger log = Logger.getLogger(this.getClass());
	
	@Mock
	protected UsuarioDao usuarioDaoMock;
	@Mock
	protected AnuncioDao anuncioDaoMock;
	@Mock
	protected CategoriaDao categoriaDaoMock;
	@Mock
	protected ImagenDao imagenDaoMock;
	@Mock
	protected ProvinciaDao provinciaDaoMock;
	@Mock
	protected PublicacionProgramadaDao publicacionProgramadaDaoMock;
	
	@Before
	public void setupMocks() throws NumberFormatException, IOException{
		
		usuarioDaoMockSetup();
		anuncioDaoMockSetup();
		categoriaDaoMockSetup();
		imagenDaoMockSetup();
		publicacionProgramadaDaoMockSetup();
		provinciaDaoMockSetup();
		
	}
	
	@Test 
	public void usuarioDaoMockTest(){
		//Testeo que usuarioDaoMock pasa los mismos test que usuarioDao
		
		UsuarioDaoTest test = new UsuarioDaoTest();
		test.createTestMock(usuarioDaoMock);
		test.findAllTestMock(usuarioDaoMock);
		test.findByIdTestMock(usuarioDaoMock);
		test.findUsersByRoleTestMock(usuarioDaoMock);
		
		//Estos metodos del Mock no devuelven nada, no es necesario testearlos
//		test.updateTestMock(usuarioDaoMock);
//		test.removeTestMock(usuarioDaoMock);

		
	}
	
	@Test 
	public void anuncioDaoMockTest(){
		//Testeo que anuncioDaoMock pasa los mismos test que anuncioDao
		
		AnuncioDaoTest test = new AnuncioDaoTest();
		test.creaAnuncioTestMock(anuncioDaoMock);
		test.findAllAnunciosTestMock(anuncioDaoMock);
		test.findAnuncioByIdTestMock(anuncioDaoMock);
		test.findByPadreTestMock(anuncioDaoMock);

		// Este metodo no est� mockeado, no es necesario testearlo, no devuelve nada
//		test.eliminaAnuncioTestMock(anuncioDaoMock);
		
	}
	
	@Test 
	public void categoriaDaoMockTest(){
		
	}
	
	@Test 
	public void imagenDaoMockTest(){
		
	}
	
	
	
	@Test 
	public void provinciaDaoMockTest(){
		
	}
	
	@Test 
	public void publicacionProgramadaDaoMockTest(){
		//Testeo que publicacionProgramadaDaoMock pasa los mismos test que publicacionProgramadaDao
		
		PublicacionProgramadaDaoTest test = new PublicacionProgramadaDaoTest();
		
		test.createPublicacionProgramadaTestMock(publicacionProgramadaDaoMock);
		test.findAllTestMock(publicacionProgramadaDaoMock);
		test.findByDayOfWeekTestMock(publicacionProgramadaDaoMock);
		test.findByIdTestMock(publicacionProgramadaDaoMock);
		
		/**
		 * El metodo findPublicacionesExecuteNowTestMock testea la funcionalidad buscando publicaciones
		 * en distintos rangos horarios.
		 * Como es complicado de implementar una l�gica mock que funcione de esa forma, testeo simplemente que el
		 * Mock funciona como he definido que funciona: devolviendo las 3 publicaciones programadas para cualquier fecha
		 */
		//PublicacionProgramadaDaoTest.findPublicacionesExecuteNowTestMock(publicacionProgramadaDaoMock);
		//Lo testeo a mano
		Assert.assertTrue(publicacionProgramadaDaoMock.findPublicacionesExecuteNow(new GregorianCalendar(), "usuario1@dominio.com").size() == 3);
	}
	
	private void usuarioDaoMockSetup(){
		SpaUsuario usuario1 = new SpaUsuario();
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		authorities.add(Roles.ROLE_USER);
		usuario1.setAuthorities(authorities);
		usuario1.setNombre("Usuario1");
		usuario1.setEmail("usuario1@dominio.com");
		usuario1.setNombre("Usuario");
		usuario1.setPassword(Sha512DigestUtils.shaHex("1234"));
		usuario1.setEnabled(true);
		usuario1.setApellidos("asdf asdf");
		
		List<Long> idAnuncios = new ArrayList<Long>();
		idAnuncios.add(0L);
		usuario1.setIdAnuncios(idAnuncios);
		
		List<SpaUsuario> usuarios = new ArrayList<SpaUsuario>();
		usuarios.add(usuario1);
		
		when(usuarioDaoMock.findAll()).thenReturn(usuarios);
		when(usuarioDaoMock.findById(any(String.class))).thenReturn(usuario1);
		when(usuarioDaoMock.findUsersByRole(same(Roles.ROLE_USER))).thenReturn(usuarios);
	}
	
	private void anuncioDaoMockSetup(){
		SpaAnuncio raiz = new SpaAnuncio("Raiz",null,null,null,null,false,-1L,null);
		raiz.setAnunId(1L);
		
		SpaAnuncio directorio = new SpaAnuncio(null,null,null,null,null,false,0L, "usuario1@dominio.com");
		directorio.setAnunId(2L);
		
		SpaAnuncio anuncio = new SpaAnuncio(
				"titulo",
				"tituloMundoanuncio",
				1L,
				"DescripcionHTML",
				1L,
				true,
				1L, "usuario1@dominio.com");
		anuncio.setAnunId(3L);
		
		List<Long> idsPublicacionesProgramadas = new ArrayList<Long>();
		idsPublicacionesProgramadas.add(1L);
		idsPublicacionesProgramadas.add(2L);
		idsPublicacionesProgramadas.add(3L);
		
		anuncio.setIdPublicacionesProgramadas(idsPublicacionesProgramadas);
		
		List<SpaAnuncio> anuncios = new ArrayList<SpaAnuncio>();
		anuncios.add(raiz);
		anuncios.add(directorio);
		anuncios.add(anuncio);
		
		List<SpaAnuncio> anunciosByUser = new ArrayList<SpaAnuncio>();
		anunciosByUser.add(directorio);
		anunciosByUser.add(anuncio);
		
		when(anuncioDaoMock.creaAnuncio(any(SpaAnuncio.class))).thenReturn(anuncio);
		
		//findAllAnuncios, creaAnuncio, findByUserEmail
		when(anuncioDaoMock.findAllAnuncios()).thenReturn(anuncios);
		when(anuncioDaoMock.creaAnuncio(any(SpaAnuncio.class))).thenReturn(anuncio);
		when(anuncioDaoMock.findByUserEmail(any(String.class))).thenReturn(anunciosByUser);
		
		//findAnuncioById
		when(anuncioDaoMock.findAnuncioById(same(1L))).thenReturn(raiz);
		when(anuncioDaoMock.findAnuncioById(same(2L))).thenReturn(directorio);
		when(anuncioDaoMock.findAnuncioById(same(3L))).thenReturn(anuncio);
		
		//findByPadre
		List<SpaAnuncio> returnList = new ArrayList<SpaAnuncio>();
		returnList.add(raiz);
		when(anuncioDaoMock.findByPadre(eq("-1"), any(String.class))).thenReturn(returnList);
		
		returnList = new ArrayList<SpaAnuncio>();
		returnList.add(directorio);
		when(anuncioDaoMock.findByPadre(eq("1"), any(String.class))).thenReturn(returnList);
		
		returnList = new ArrayList<SpaAnuncio>();
		returnList.add(anuncio);
		returnList.add(directorio);
		when(anuncioDaoMock.findByPadre(eq("2"), any(String.class))).thenReturn(returnList);
	}
	
	private void categoriaDaoMockSetup() throws NumberFormatException, IOException{
		
		List<SpaCategoria> categorias = new ArrayList<SpaCategoria>();
		
		FileSystemResource ficheroCSV = new FileSystemResource(new File("src/main/resources/bulkloadResources/categorias.csv"));
		InputStreamReader ir = new InputStreamReader(ficheroCSV.getInputStream());
		CSVReader reader = new CSVReader(ir,';');
		String[] aux;
		while ((aux = reader.readNext())!=null){
			SpaCategoria categoria = new SpaCategoria(new Long(aux[0].toUpperCase()), aux[1].toUpperCase(),aux[2].toUpperCase());
			categorias.add(categoria);
		}
		
		when(categoriaDaoMock.findCategoriaById(any(Long.class))).thenReturn(categorias.get(0));
		when(categoriaDaoMock.getAllCategorias()).thenReturn(categorias);
		
	}
	
	private void imagenDaoMockSetup(){
		SpaImagen imagen = new SpaImagen();
		imagen.setId(1L);
		imagen.setIdAnuncio(3L);
		imagen.setImagen(new byte[1]);
		imagen.setLink("");
		
		when(imagenDaoMock.create(any(SpaImagen.class))).thenReturn(imagen);
		when(imagenDaoMock.findById(same(1L))).thenReturn(imagen);
		
		List<SpaImagen> imagenes = new ArrayList<SpaImagen>();
		imagenes.add(imagen);
		when(imagenDaoMock.findByIdAnuncio(same(3L))).thenReturn(imagenes);
		
	}
	
	private void publicacionProgramadaDaoMockSetup(){
		
		String idUsuario = "usuario1@dominio.com";
		
		/**
		 * Creo 3 publicaciones para el Lunes
		 * 
		 *  - De 18:15 a 19:15 cada minuto
		 *  - De 18:30 a 19:30 cada minuto
		 *  - De 18:45 a 19:45 cada minuto
		 */
		Calendar horaDesde = new GregorianCalendar();
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 15);
		horaDesde.set(Calendar.MONTH,Calendar.JANUARY);
		horaDesde.set(Calendar.YEAR, 2007);
		horaDesde.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		horaDesde.set(Calendar.DAY_OF_MONTH, 1);
		
		
		Calendar horaHasta = new GregorianCalendar();
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 15);
		horaHasta.set(Calendar.MONTH,Calendar.JANUARY);
		horaHasta.set(Calendar.YEAR, 2007);
		horaHasta.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		horaHasta.set(Calendar.DAY_OF_MONTH, 1);
		
		SpaPublicacionProgramada publicacionProgramada1 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		publicacionProgramada1.setIntervaloMin(1);
		publicacionProgramada1.setIdUsuario(idUsuario);
		publicacionProgramada1.setId(1L);
		
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 30);
	
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 30);
		
		SpaPublicacionProgramada publicacionProgramada2 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		publicacionProgramada2.setIntervaloMin(2);
		publicacionProgramada2.setIdUsuario(idUsuario);
		publicacionProgramada2.setId(2L);
		
		horaDesde.set(Calendar.HOUR_OF_DAY, 18);
		horaDesde.set(Calendar.MINUTE, 45);
	
		horaHasta.set(Calendar.HOUR_OF_DAY, 19);
		horaHasta.set(Calendar.MINUTE, 45);
		
		SpaPublicacionProgramada publicacionProgramada3 = 
			new SpaPublicacionProgramada(
					horaDesde.getTime(),
					horaHasta.getTime(),
					5,
					DiasSemanaEnum.LUNES);
		
		
		publicacionProgramada3.setIntervaloMin(3);
		publicacionProgramada3.setIdUsuario(idUsuario);
		publicacionProgramada3.setId(3L);
		
		List<SpaPublicacionProgramada> publicacionesProgramadas = new ArrayList<SpaPublicacionProgramada>();
		publicacionesProgramadas.add(publicacionProgramada1);
		publicacionesProgramadas.add(publicacionProgramada2);
		publicacionesProgramadas.add(publicacionProgramada3);
		
		/**
		 * 
		 */
		
		when(publicacionProgramadaDaoMock.createPublicacionProgramada(any(SpaPublicacionProgramada.class))).thenReturn(publicacionProgramada1);
		when(publicacionProgramadaDaoMock.findAll()).thenReturn(publicacionesProgramadas);
		when(publicacionProgramadaDaoMock.findByDayOfWeek(same(DiasSemanaEnum.LUNES))).thenReturn(publicacionesProgramadas);
		
		when(publicacionProgramadaDaoMock.findById(same(1L))).thenReturn(publicacionProgramada1);
		when(publicacionProgramadaDaoMock.findById(same(2L))).thenReturn(publicacionProgramada2);
		when(publicacionProgramadaDaoMock.findById(same(3L))).thenReturn(publicacionProgramada3);
		
		when(publicacionProgramadaDaoMock.findPublicacionesExecuteNow(any(Calendar.class), same(idUsuario))).thenReturn(publicacionesProgramadas);
	}
	
	private void provinciaDaoMockSetup() throws IOException{
		
		List<SpaProvincia> provincias = new ArrayList<SpaProvincia>();
		
		FileSystemResource ficheroCSV = new FileSystemResource(new File("src/main/resources/bulkloadResources/provincias.csv"));
		InputStreamReader ir = new InputStreamReader(ficheroCSV.getInputStream());
		CSVReader reader = new CSVReader(ir,';');
		String[] aux;
		SpaProvincia provincia = null;
		Long id = 1L;
		while ((aux = reader.readNext())!=null){
			provincia = new SpaProvincia(id, aux[1].toUpperCase());
			provincias.add(provincia);
			id++;
			
		}
		
		when(provinciaDaoMock.findProvById(any(Long.class))).thenReturn(provincias.get(0));
		when(provinciaDaoMock.getAllProvincias()).thenReturn(provincias);
		
	}
	
	protected String leerFichero(File fichero) throws IOException{
		StringBuffer fileContains = new StringBuffer();
		BufferedReader br = new BufferedReader(new FileReader(fichero));
		fileContains = new StringBuffer();
	    String str;
	    while ((str = br.readLine()) != null) {fileContains.append(str);}
	    br.close();
		return fileContains.toString();
	}
	
}
