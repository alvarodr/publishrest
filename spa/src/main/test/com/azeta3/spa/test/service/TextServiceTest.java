package com.azeta3.spa.test.service;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.azeta3.spa.service.TextService;
import com.azeta3.spa.test.BaseSpringTest;

public class TextServiceTest extends BaseSpringTest{
	
	@Autowired
	TextService textService;
	
	@Test
	public void ranzomizeTest(){
		String text = new String();
		
		text = 
			"Es importante darse cuenta de que las pruebas unitarias " +
			"no descubrir�n todos los errores del c�digo. Por definici�n, " +
			"s�lo prueban las unidades por s� solas. Por lo tanto, no descubrir�n " +
			"errores de integraci�n, problemas de rendimiento y otros problemas" +
			" que afectan a todo el sistema en su conjunto. Adem�s, puede no ser" +
			" trivial anticipar todos los casos especiales de entradas que puede " +
			"recibir en realidad la unidad de programa bajo estudio. Las pruebas " +
			"unitarias s�lo son efectivas si se usan en conjunto con otras pruebas " +
			"de software.";
		
		System.out.println(textService.Randomize(text));
	}
}
