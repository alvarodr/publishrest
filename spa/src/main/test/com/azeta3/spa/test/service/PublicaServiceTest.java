package com.azeta3.spa.test.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.junit.Assert;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

import com.azeta3.spa.bom.SpaAnuncio;
import com.azeta3.spa.bom.SpaCategoria;
import com.azeta3.spa.bom.SpaImagen;
import com.azeta3.spa.dao.AnuncioDao;
import com.azeta3.spa.dao.CategoriaDao;
import com.azeta3.spa.service.ImagenService;
import com.azeta3.spa.service.PublicaService;
import com.azeta3.spa.service.TextService;
import com.azeta3.spa.test.BaseSpringTest;
import com.gargoylesoftware.htmlunit.ElementNotFoundException;


public class PublicaServiceTest extends BaseSpringTest{

	@Autowired
	private CategoriaDao categoriaDao;
	@Autowired
	private AnuncioDao anuncioDao;
	@Autowired
	private ImagenService imagenService;
	@Autowired
	private PublicaService publicaService;
	@Autowired
	private TextService textService;
	
	@Before
	public void prepare() throws ElementNotFoundException, Exception{
		SpaCategoria categoria = new SpaCategoria(1L,
				"http://www.mundoanuncio.com/publicar/categoria/otros_servicios_51.html",
				"Empleo - Otros");
		categoria = categoriaDao.createCategoria(categoria);
		
		SpaAnuncio anuncio = new SpaAnuncio(
				"REPARO TODO TIPO DE PC",
				"REPARO TODO TIPO DE PC",
				4L,
				"Reparo todo tipo de PC's" +
				" Estoy sin trabajo y necesito alimentar a mi familia." +
				"Trabajo a cambio de comida. Interesados llamen al 645 839 222."
				, 1L
				, false,
				-1L,
				"sadffas_asd@sadf.com");
		
		anuncio.setDescripcionHTML(textService.Randomize(anuncio.getDescripcionHTML()));
		anuncio = anuncioDao.creaAnuncio(anuncio);
		anuncio.setEmail("gotoalberto@gmail.com");

		SpaImagen imagen = new SpaImagen();
		imagen.setFilename("imagen.jpg");
		imagen.setIdAnuncio(anuncio.getAnunId());
		
		BufferedImage bi = ImageIO.read(new File("src/main/test/resources/testImages/piso.jpg"));
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(bi, "JPG", baos);
		byte[] imageByteArray = baos.toByteArray();
		
		imagen.setImagen(imageByteArray);
		
		imagen = imagenService.create(imagen);
		
		List<Long> idsImagenes = new ArrayList<Long>();
		idsImagenes.add(imagen.getId());
		anuncio.setIdsImagenes(idsImagenes);
		anuncioDao.updateAnuncio(anuncio);
		
		Assert.assertTrue(publicaService.publicarAnuncioMundoAnuncio(anuncio));
	}
}
