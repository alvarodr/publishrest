package com.azeta3.spa.test.dao;

import java.util.HashSet;
import java.util.Set;

import javax.jdo.annotations.NotPersistent;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.token.Sha512DigestUtils;

import com.azeta3.spa.bom.SpaUsuario;
import com.azeta3.spa.dao.UsuarioDao;
import com.azeta3.spa.security.Roles;
import com.azeta3.spa.test.BaseSpringTest;

public class UsuarioDaoTest extends BaseSpringTest{

	@Autowired
	UsuarioDao usuarioDao;
	
	@Test
	@NotPersistent
	public void findByIdTest() {
		findByIdTestMock(usuarioDao);
	}
	
	@Test
	@NotPersistent
	public void createTest() {
		createTestMock(usuarioDao);
	}
	
	@Test
	@NotPersistent
	public void removeTest(){
		removeTestMock(usuarioDao);
	}
	
	@Test
	@NotPersistent
	public void findUsersByRoleTest(){
		findUsersByRoleTestMock(usuarioDao);
	}
	
	@Test
	@NotPersistent
	public void findAllTest(){
		findAllTestMock(usuarioDao);
	}
	
	@Test
	@NotPersistent
	public void updateTest(){
		updateTestMock(usuarioDao);
	}
	
	public void updateTestMock(UsuarioDao usuarioDao){
		SpaUsuario usuario = new SpaUsuario();
		usuario.setApellidos("asdf asdf");
		
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		authorities.add(Roles.ROLE_USER);
		usuario.setAuthorities(authorities);
		usuario.setEmail("usuario1@dominio.com");
		usuario.setEnabled(true);
		usuario.setNombre("nombre");
		usuario.setPassword(Sha512DigestUtils.shaHex("1234"));
		
		usuarioDao.create(usuario);
		
		usuario.setNombre("updated");
		usuarioDao.update(usuario);
		
		Assert.assertTrue(usuarioDao.findById("usuario1@dominio.com").getNombre().equals("updated"));
	}
	
	public void findAllTestMock(UsuarioDao usuarioDao){
		SpaUsuario usuario = new SpaUsuario();
		usuario.setApellidos("asdf asdf");
		
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		authorities.add(Roles.ROLE_USER);
		usuario.setAuthorities(authorities);
		usuario.setEmail("usuario2@dominio.com");
		usuario.setEnabled(true);
		usuario.setNombre("nombre");
		usuario.setPassword(Sha512DigestUtils.shaHex("1234"));
		
		usuarioDao.create(usuario);
		
		Assert.assertTrue(usuarioDao.findAll().size() == 1);
	}
	
	public void findUsersByRoleTestMock(UsuarioDao usuarioDao){
		SpaUsuario usuario = new SpaUsuario();
		usuario.setApellidos("asdf asdf");
		
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		authorities.add(Roles.ROLE_USER);
		usuario.setAuthorities(authorities);
		usuario.setEmail("usuario2@dominio.com");
		usuario.setEnabled(true);
		usuario.setNombre("nombre");
		usuario.setPassword(Sha512DigestUtils.shaHex("1234"));
		
		usuarioDao.create(usuario);
		
		Assert.assertTrue(usuarioDao.findUsersByRole(Roles.ROLE_USER).size() == 1);
	}
	
	public void removeTestMock(UsuarioDao usuarioDao){
		SpaUsuario usuario = new SpaUsuario();
		usuario.setApellidos("asdf asdf");
		
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		authorities.add(Roles.ROLE_USER);
		usuario.setAuthorities(authorities);
		usuario.setEmail("usuario2@dominio.com");
		usuario.setEnabled(true);
		usuario.setNombre("nombre");
		usuario.setPassword(Sha512DigestUtils.shaHex("1234"));
		
		usuarioDao.create(usuario);
		usuario = usuarioDao.findById("usuario2@dominio.com");
		
		usuarioDao.remove(usuario);
		Assert.assertTrue(usuarioDao.findById("usuario2@dominio.com") == null);
	}
	
	public void createTestMock(UsuarioDao usuarioDao){
		SpaUsuario usuario = new SpaUsuario();
		usuario.setApellidos("asdf asdf");
		
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		authorities.add(Roles.ROLE_USER);
		usuario.setAuthorities(authorities);
		usuario.setEmail("usuario1@dominio.com");
		usuario.setEnabled(true);
		usuario.setNombre("nombre");
		usuario.setPassword(Sha512DigestUtils.shaHex("1234"));
		
		usuarioDao.create(usuario);
		Assert.assertTrue(usuarioDao.findById("usuario1@dominio.com") != null);
	}
	
	public void findByIdTestMock(UsuarioDao usuarioDao){
		SpaUsuario usuario = new SpaUsuario();
		usuario.setApellidos("asdf asdf");
		
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		authorities.add(Roles.ROLE_USER);
		usuario.setAuthorities(authorities);
		usuario.setEmail("usuario1@dominio.com");
		usuario.setEnabled(true);
		usuario.setNombre("nombre");
		usuario.setPassword(Sha512DigestUtils.shaHex("1234"));
		
		usuarioDao.create(usuario);
		Assert.assertTrue(usuarioDao.findById("usuario1@dominio.com") != null);
	}

}
