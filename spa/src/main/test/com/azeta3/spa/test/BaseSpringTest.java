package com.azeta3.spa.test;


import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/spring/applicationContext-test.xml",
									"classpath:/spring/applicationContext-dao-test.xml",
									"classpath:/spring/applicationContext-security-test.xml",
									"classpath:/spring/applicationContext-service-test.xml"})
public class BaseSpringTest{
	
	protected Logger log = Logger.getLogger(this.getClass());
	
	
	@Test
	public void test(){}

}
